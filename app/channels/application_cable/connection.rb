module ApplicationCable
  class Connection < ActionCable::Connection::Base
    HITL_EMAIL = "tech+hitl@dirtylemon.com"

    identified_by :current_user

    def connect
      self.current_user = authenticate_user
    end


    private

    # Authenticates the current user or rejects the connection
    # @raise [UnauthorizedError]
    # @return [User]
    def authenticate_user
      if user = User.find_by(id: auth_token.try(:[], :user_id))
        user
      else
        reject_unauthorized_connection
      end
    end

    # Decodes and verifies a token
    # @return [Hash, Nil]
    def auth_token
      token = decode_generic_token || decode_hitl_token
      if token && token[:exp] && (Time.at(token[:exp]) > Time.current)
        token
      else
        nil
      end
    end

    # @return [Hash, Nil]
    def decode_generic_token
      decode_token(request.params[:token])
    end

    # Special HITL JWT secret that can only be used to sign the HITL user.
    # @return [Hash, Nil]
    def decode_hitl_token
      decode_token(request.params[:token],
                   secret: ENV.fetch('HITL_JWT_SECRET'),
                   constraints: { user_id: User.find_by!(email: HITL_EMAIL).id })
    end

    # Decodes a token for a given secret and verifies that it matches of set of constraints
    # @return [Hash, Nil]
    def decode_token(token, secret: nil, constraints: {})
      token = DirtyLemon::JsonWebToken.decode(token, secret: secret)
      if token.slice(*constraints.keys).stringify_keys == constraints.stringify_keys
        token
      else
        nil
      end
    rescue JWT::VerificationError, JWT::DecodeError
      nil
    end
  end
end
