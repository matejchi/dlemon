# https://robots.thoughtbot.com/catching-json-parse-errors-with-custom-middleware
class RescueJsonParseErrors
  def initialize(app)
    @app = app
  end

  def call(env)
    begin
      @app.call(env)
    rescue ActionDispatch::Http::Parameters::ParseError => error
      if env['CONTENT_TYPE'] =~ /application\/json/
        error_output = "There was a problem in the JSON you submitted: #{error}"
        return [
          400, { "Content-Type" => "application/json" },
          [ { code: 'BadRequest', error: error_output }.to_json ]
        ]
      else
        raise error
      end
    end
  end
end
