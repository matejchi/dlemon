require 'mandrill'

class ApplicationMailer < ActionMailer::Base
  include ActionView::Helpers::NumberHelper

  default(
    from: DirtyLemon::Settings.email,
    reply_to: DirtyLemon::Settings.email
  )


  private

  def mandrill_mail(template_name, options, variables)
    mandrill = Mandrill::API.new(ENV.fetch('MANDRILL_API_TOKEN'))
    options[:global_merge_vars] = variables.map do |key, value|
      { 'name' => key.upcase, 'content' => value }
    end
    options[:merge] = true
    mandrill.messages.send_template(template_name, [], options)
  end
end
