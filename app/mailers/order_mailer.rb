class OrderMailer < ApplicationMailer

  # @param id [String] Order id
  # @param order_updated [Boolean] True if is update, false if new order
  def confirmation(id, order_updated = false)
    @order = Order.find(id)

    options = {
      to: [{
        type: 'to',
        email: @order.shipping.email, # A paid order is garanteed to have an email
        name: @order.customer.full_name
      }],
      subject: "Order ##{@order.reference} - #{order_updated ? "Updated" : "Confirmed"}",
      from_email: default_params[:from],
      from_name: "Dirty Lemon"
    }

    variables = {
      localized_phone_number: DirtyLemon::Settings.localized_phone_number(@order.customer.country_code),
      customer_first_name: @order.customer.first_name,
      order_reference: @order.reference,
      order_total: number_with_precision(@order.total.round(2), precision: 2),
      order_shipping_name: @order.shipping.name,
      order_shipping_company: @order.shipping.company,
      order_shipping_phone_number: @order.shipping.phone_number,
      order_shipping_street1: @order.shipping.street1,
      order_shipping_street2: @order.shipping.street2,
      order_shipping_city: @order.shipping.city,
      order_shipping_state: @order.shipping.state,
      order_shipping_country: @order.shipping.country,
      order_shipping_zip: @order.shipping.zip,
      line_items: @order.line_items.map do |li|
        line_item_price = @order.customer.vip? ? li.sku.vip_price : li.sku.price
        {
          quantity: li.quantity,
          product_name: li.sku.product.name,
          sku_price: number_with_precision(line_item_price.round(2), precision: 2),
        }
      end
    }

    mandrill_mail('order-confirmation', options, variables)
  end

  # @param id [String] Order id
  # @param shipment_id [String] Shipment id
  def shipping_confirmation(id, shipment_id)
    @order = Order.find(id)
    @shipment = Shipment.find(shipment_id)

    options = {
      to: [{
        type: 'to',
        email: @order.shipping.email || @order.customer.email, # A fulfilled order is not garanteed to have an email
        name: @order.shipping.name
      }],
      subject: "Order ##{@order.reference} - Shipped",
      from_email: default_params[:from],
      from_name: "Dirty Lemon"
    }

    variables = {
      localized_phone_number: DirtyLemon::Settings.localized_phone_number(@order.customer.country_code),
      customer_first_name: @order.customer.first_name,
      order_reference: @order.reference,
      shipment_tracking_number: @shipment.tracking_number,
      shipment_tracking_url: @shipment.tracking_url
    }

    mandrill_mail('shipment-in-transit', options, variables)
  end

  # @param batch_id [String] Batch id
  # @param fulfillment_address_id [String] FulfillmentAddress id
  def fulfillment(batch_id, fulfillment_address_id)
    @batch = Batch.find(batch_id)
    @fulfillment_address = FulfillmentAddress.find(fulfillment_address_id)

    # A batch has fulfilled and queued orders
    @orders = @batch.orders.where(status: 'fulfilled')
    @shipments = @batch.shipments.where(order_id: @orders.select(:id))

    # Generate a CSV document of fulfilled orders and shipments
    @export_orders = DirtyLemon::ExportOrder.new.perform(@orders)
    @export_shipments = DirtyLemon::ExportShipment.new.perform(@shipments)

    # Generate Fulfillment Address specific documents
    if @fulfillment_address.carrier_facility == 'spwg'
      @ontrac_shipments = @shipments.where(carrier: 'OnTrac')
      @export_shipments_ontrac = DirtyLemon::ExportShipmentOntrac.new.perform(@ontrac_shipments)
    end

    # Build metrics
    @skus = Sku.find(LineItem.where(order: @orders).pluck(:sku_id).uniq)
    @products = Product.find(@skus.pluck(:product_id).uniq)
    @skus_count = LineItem.where(order: @orders).group(:sku_id).sum(:quantity)

    @metrics = @products.map do |product|
      sku_ids = product.skus.pluck(:id)

      [
        product,
        @skus_count.slice(*sku_ids).map { |id, count| [@skus.find { |s| s.id == id }, count] }
      ]
    end

    attachments.inline["orders-#{@batch.created_at.to_i}.csv"] = {
      mine_type: 'text/csv',
      content: @export_orders
    }

    attachments.inline["shipments-#{@batch.created_at.to_i}.csv"] = {
      mine_type: 'text/csv',
      content: @export_shipments
    }

    if @export_shipments_ontrac
      attachments.inline["shipments-ontrac-#{@batch.created_at.to_i}.csv"] = {
        mine_type: 'text/csv',
        content: @export_shipments_ontrac
      }
    end

    mail(
      to: @fulfillment_address.email,
      subject: "Orders - #{Time.current.to_s(:db)}"
    )
  end

end
