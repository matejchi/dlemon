require 'concurrent'

module DirtyLemon
  class CRM
    include Concurrent::Async

    # @param customer [Customer]
    def initialize(customer)
      super() # Required for concurrent async proper instantiation
      @customer = customer
    end

    def create
    end

    def update
    end
  end
end
