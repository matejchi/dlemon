module DirtyLemon
  class Settings
    Phone = Struct.new(:number, :country_code)

    # @return [Array]
    def self.phone_numbers
      @@phone_numbers ||= [
        Phone.new(ENV['TWILIO_PHONE_US'], "US"),
        Phone.new(ENV['TWILIO_PHONE_CA'], "CA"),
        Phone.new(ENV['TWILIO_PHONE_GB'], "GB"),
        Phone.new(ENV['TWILIO_PHONE_AU'], "AU")
      ]
    end

    # @return [String]
    def self.email
      "hi@dirtylemon.com"
    end

    # @return [String]
    def self.phone_number_voice
      "+18778977784"
    end

    # @return [String]
    def self.fulfillment_email
      "tech@dirtylemon.com"
    end

    # Selects the most appropriate phone number given a country code
    # @param cc [String] Country code
    def self.localized_phone_number(cc)
      phone = phone_numbers.find { |phone| phone.country_code == cc }
      phone ? phone.number : phone_numbers.find { |phone| phone.country_code == 'US' }.number
    end

    # @return [Array]
    def self.carrier_accounts
      [
        ENV.fetch('UPS_CARRIER_ACCOUNT'),
        ENV.fetch('ONTRAC_CARRIER_ACCOUNT')
      ]
    end

    # @return [Hash]
    def self.parcel
      {
        length: 10,   # inches (in)
        width: 10,    # inches (in)
        height: 10,   # inches (in)
        weight: 176   # ounces (oz)
      }
    end

    # Computes the shipping rate for a given country
    #
    # @param country [String] A ISO 3166 country code
    # @return [Float]
    # @note Price is per shipment
    def self.shipping_rate(country)
      {
        'GB' => 12.0,
        'AU' => 24.0
      }[country] || 0.0
    end

    # @return [Array]
    def self.available_countries
      []
    end

    # @return [Integer] Hour at which the day starts for customers (do not text before)
    def self.customer_day_start
      8
    end

    # @return [Integer] Hour at which the day ends for customers (do not text after)
    def self.customer_day_end
      22
    end
  end
end
