require 'csv'

module DirtyLemon
  class ExportOrder

    # Generates a CSV document
    # @param orders [ActiveRecord::Relation]
    # @return [String]
    def perform(orders)
      CSV.generate do |csv|
        csv << headers

        orders.each do |order|
          order.line_items.each do |line_item|
            row = [
              order_fields(order),
              line_item_fields(line_item),
              shipping_fields(order.shipping)
            ].flatten

            csv << row
          end
        end
      end
    end


    private

    # @return [Array]
    def headers
      [
        'Order #',
        'Date',
        'Status',

        'Quantity',
        'SKU',

        'Name',
        'Company',
        'Phone Number',
        'Email',
        'Street 1',
        'Street 2',
        'City',
        'State',
        'Country',
        'Zip',
      ]
    end

    # @param order [Order]
    # @return [Array]
    def order_fields(order)
      [
        order.reference,
        order.created_at.to_date.to_s(:db),
        order.status
      ]
    end

    # @param line_item [LineItem]
    # @return [Array]
    def line_item_fields(line_item)
      [
        line_item.quantity,
        line_item.sku.stripe_id
      ]
    end

    # @param shipping [Shipping]
    # @return [Array]
    def shipping_fields(shipping)
      [
        shipping.name,
        shipping.company,
        shipping.phone_number,
        shipping.email,
        shipping.street1,
        shipping.street2,
        shipping.city,
        shipping.state,
        shipping.country,
        shipping.zip
      ]
    end
  end
end
