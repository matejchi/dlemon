module DirtyLemon
  module Taxjar
    # Very simple Taxjar fake client for our usecase (dev, test):
    #  - Has harcoded global rate for some States only
    #  - Always taxes shipping
    class FakeClient
      RATES = {
        "US" => {
          "NY" => 0.08875,
          "NJ" => 0.06875,
        }
      }

      def initialize(_)
      end

      def tax_for_order(to_country:, to_state:, amount:, shipping:, **_)
        rate = RATES.dig((to_country || '').upcase, (to_state || '').upcase)
        pre_tax_amount = amount + shipping

        ::Taxjar::Tax.new(
          order_total_amount: pre_tax_amount,
          taxable_amount: pre_tax_amount,
          shipping: shipping,
          amount_to_collect: ((rate || 0.0) * pre_tax_amount).round(2),
          rate: rate || 0.0,
          has_nexus: rate.present?,
          freight_taxable: true,
          tax_source: "destination"
        )
      end

      def create_order(params)
        ::Taxjar::Order.new(params)
      end

      def create_refund(params)
        ::Taxjar::Refund.new(params)
      end
    end
  end
end
