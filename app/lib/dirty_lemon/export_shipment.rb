require 'csv'

module DirtyLemon
  class ExportShipment

    # Generates a CSV document
    # @param shipments [ActiveRecord::Relation]
    # @return [String]
    def perform(shipments)
      CSV.generate do |csv|
        csv << headers

        shipments.each do |shipment|
          row = [
            shipment_fields(shipment),
            order_fields(shipment.order),
            sku_fields(shipment.sku),
            shipping_fields(shipment.order.shipping),
          ].flatten

          csv << row
        end
      end
    end


    private

    # @return [Array]
    def headers
      [
        'Tracking Number',
        'Tracking Status',
        'Carrier',
        'Service',

        'Order #',

        'SKU',
        'Product',

        'Name',
        'Company',
        'Phone Number',
        'Email',
        'Street 1',
        'Street 2',
        'City',
        'State',
        'Country',
        'Zip',
      ]
    end

    # @param shipment [Shipment]
    # @return [Array]
    def shipment_fields(shipment)
      [
        shipment.tracking_number,
        shipment.tracking_status,
        shipment.carrier,
        shipment.service
      ]
    end

    # @param sku [Sku]
    # @return [Array]
    def sku_fields(sku)
      [
        sku.stripe_id,
        sku.product.name
      ]
    end

    # @param order [Order]
    # @return [Array]
    def order_fields(order)
      [
        order.reference
      ]
    end

    # @param shipping [Shipping]
    # @return [Array]
    def shipping_fields(shipping)
      [
        shipping.name,
        shipping.company,
        shipping.phone_number,
        shipping.email,
        shipping.street1,
        shipping.street2,
        shipping.city,
        shipping.state,
        shipping.country,
        shipping.zip
      ]
    end
  end
end
