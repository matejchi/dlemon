module DirtyLemon
  class ShipmentRouter
    attr_reader :service, :carriers, :fulfillment_addresses

    # @param service [Easypost:Service]
    # @param carriers [Array] A collection of carrier accounts
    # @param fulfillment_addresses [Array] A collection of FulfillmentAddress objects
    def initialize(service, carriers, fulfillment_addresses)
      @service = service
      @carriers = carriers
      @fulfillment_addresses = fulfillment_addresses
    end

    # Selects the lowest rate respecting constraints for a given shipment
    # @see Rate services values provided by EasyPost https://www.easypost.com/docs/api/ruby.html#rates
    # @param shipment [Shipment]
    # @param purchase_at [TimeWithZone] Time at which the shipping label is being purchased
    # @return [EasyPost::Shipment, EasyPost::Rate, FulfillmentAddress]
    def route(shipment, purchase_at)
      to_address = shipment.order.shipping

      @all_routes = Array.new     # Collect all possible routes
      @all_rates = Array.new      # Collect all possible rates
      @filtered_rates = Array.new # Collect filtered rates

      # Generate all possible routes
      fulfillment_addresses.map do |fulfillment_address|
        label_date = fulfillable_at(purchase_at.in_time_zone(fulfillment_address.time_zone))

        options = {
          label_date: label_date.utc,
          description: shipment.sku.product.name,
          sku: shipment.sku.stripe_id,
          print1: shipment.order.reference,
          print2: shipment.sku.product.name
        }

        # Create shipment with saturday rates
        #options.merge!(saturday_delivery: true) if label_date.thursday?

        # Create shipment with regular rates
        ep_shipment = service.create_shipment(to_address, fulfillment_address, carriers, options)
        ep_rates = ep_shipment.get_rates.rates

        @all_routes << ep_shipment
        @all_rates << ep_rates
        @filtered_rates << apply_constraints(ep_rates, to_address, options)
      end

      # Get lowest rate
      @selected_rate = @filtered_rates.flatten.min_by { |r| r.rate.to_f }

      raise StandardError, 'No rates found' unless @selected_rate

      # Get selected route
      @ep_shipment = @all_routes.flatten.find { |s| s.id == @selected_rate.shipment_id }

      # Get selected fulfillment address
      @fulfillment_address = fulfillment_addresses.find { |a| a.street1 == @ep_shipment.from_address.street1 }

      return @ep_shipment, @selected_rate, @fulfillment_address
    end

    # Assuming it's purchased at a given time, represents the time at wich the shipment would be fulfilled
    # according to DirtyLemon's shipping policy:
    #
    # # Monday
    #   1. Order from last week’s Thursday 7AM to Monday 7AM
    #   2. Fulfilled on Monday
    #   3. Will arrive before EOD Wednesday
    #
    # # Tuesday
    #   1. Order from Monday 7AM to Tuesday 7AM
    #   2. Fulfilled on Tuesday
    #   3. Will arrive before EOD Thursday
    #
    # # Wednesday
    #   1. Order from Tuesday 7AM to Wednesday 7AM
    #   2. Fulfilled on Wednesday
    #   3. Will arrive on before EOD Friday
    #
    # # Thursday
    #   1. Order from Wednesday 7AM to Thursday 7AM
    #   2. Fulfilled on Thursday
    #   3. Will arrive:
    #     - on Friday if "1 day ground" is available
    #     - on Saturday if "2 days saturday" is available
    #     - on Friday if "next day air saver" is available
    #
    # # Friday, Saturday and Sunday
    #   1. Order from Thursday 7AM to next week's Monday 7AM
    #
    # @param time [TimeWithZone] Time at which the shipment is purchased in the warehouse's timezone
    # @param fulfills_at [Integer] Hour of the day
    def fulfillable_at(time, fulfills_at=7)
      if time.monday? || time.tuesday? || time.wednesday? || time.thursday?
        # Time between 12PM and 7AM
        if time.hour < fulfills_at
          time.at_beginning_of_day + fulfills_at.hours

        # Time between 7AM and 12PM
        else
          time.at_beginning_of_day.next_day + fulfills_at.hours
        end
      elsif time.friday? || time.saturday? || time.sunday?
        time.next_week.monday + fulfills_at.hours
      end
    end

    # @param ep_rates [Array]
    # @param to_address [Hash]
    # @param options [Hash]
    # @return [Array]
    def apply_constraints(ep_rates, to_address, options)
      ep_rates.select do |ep_rate|
        rate_constraints(to_address, options).all? { |c| c.call(ep_rate) }
      end
    end

    # Returns a collection of constraints returning a boolean on wether or not a
    # rate can be used to ship a shipment
    # @param to_address [Shipping]
    # @return [Array] A collection of Procs
    def rate_constraints(to_address, options)
      # UPS
      # - In the US, on thursday only allow rates garanteed to ship in 1 day or less, otherwise 2 days
      # - Internationaly, allow all rates
      ups_constraint = ->(rate) do
        return true unless rate.carrier == 'UPS'
        return true unless to_address.country == 'US'
        # Some UPS shipments do not have `delivery_days` – let's ignore those
        return false unless rate.delivery_days

        # FIXME: this should be less about days, and more about time till
        # weekend...

        if options[:label_date].thursday?
          max_allowed_delivery_days = 1
        else
          max_allowed_delivery_days = 2
        end

        rate.delivery_days <= max_allowed_delivery_days
      end

      # OnTrac
      # - In the US, on thursday only allow rates garanteed to ship in 1 day or less, otherwise 2 days
      # - Internationaly, allow all rates
      ontrac_constraint = ->(rate) do
        return true unless rate.carrier == 'OnTrac'
        return true unless to_address.country == 'US'

        # FIXME: this should be less about days, and more about time till
        # weekend...

        if options[:label_date].thursday?
          max_allowed_delivery_days = 1
        else
          max_allowed_delivery_days = 2
        end

        rate.delivery_days <= max_allowed_delivery_days
      end

      [ups_constraint, ontrac_constraint]
    end


    private

    # Formats a zip like 90292-6794 into 90292
    # @param zip [String]
    # @return [String]
    def format_zip(zip)
      zip.match(/(\d+)(-(\d+))?/)[1]
    end
  end
end
