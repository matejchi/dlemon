module DirtyLemon
  class ShipmentHandler
    class Error < StandardError; end

    FORMATS = [:png, :zpl].freeze

    attr_reader :s3_service, :labelary_service

    # @param s3_service [S3Service, Nil]
    # @param labelary_service [LabelaryService, Nil]
    def initialize(s3_service=nil, labelary_service=nil)
      @s3_service = s3_service || S3Service.new
      @labelary_service = labelary_service || LabelaryService.new
    end

    # Generate and cache PNG label from ZPL label using Labelary
    #
    # @param shipment [Shipment]
    # @param zpl_url [String]
    # @raise [S3Service::Error, LabelaryService::Error]
    # @return [True]
    def generate_shipping_labels(shipment, zpl_url)
      zpl_content = get_file_content!(zpl_url)
      png_content = labelary_service.generate(zpl_content)

      png_label_key = "shipping_labels/#{shipment.id}.png"
      zpl_label_key = "shipping_labels/#{shipment.id}.zpl"

      s3_service.upload_object(bucket: bucket, key: png_label_key, body: png_content, content_type: 'image/png', content_encoding: 'utf-8')
      s3_service.upload_object(bucket: bucket, key: zpl_label_key, body: zpl_content, content_type: 'text/plain', content_encoding: 'utf-8')

      shipment.update_attributes!(label_png_s3_key: png_label_key, label_zpl_s3_key: zpl_label_key)
    end

    # Copy a shipment's shipping label files to the warehouse's AWS S3 bucket
    # from the main bucket
    #
    # @param shipment [Shipment]
    # @raise [S3Service::Error]
    # @return [True]
    def copy_to_warehouse(shipment)
      target_bucket = warehouse_bucket_mapping.fetch(shipment.fulfillment_address.carrier_facility)

      format_key_hash = FORMATS.map { |ext| [ext, shipment.label_s3_key(ext)] }.to_h
      format_key_hash.each do |ext, source_key|
        s3_service.copy_object({
          copy_source: File.join(bucket, source_key),
          bucket: target_bucket,
          key: filepath(shipment, ext)
        })
      end

      true
    end


    private

    # @param url [String]
    # @return [String]
    def get_file_content!(url)
      response = HTTP.get(url)

      # Pass on errors when HTTP status included in 300 to 599
      if (300..599).include?(response.code)
        raise Error.new([response.reason, "CODE: #{response.code}", "URL: #{response.uri}"].join(' : '))
      end

      response.to_s
    end

    # @return [String, Nil]
    def warehouse_bucket_mapping
      {
        'njfs' => ENV.fetch('AWS_NJFS_BUCKET'),
        'spwg' => ENV.fetch('AWS_SPWG_BUCKET')
      }
    end

    # @param shipment [Shipment]
    # @param ext [String, Symbol] File extension
    # @return [String]
    def filename(shipment, ext)
      [
        shipment.service.downcase,
        shipment.carrier.downcase,
        shipment.order.reference,
        shipment.tracking_number
      ].join('-') + ".#{ext}"
    end

    # @param shipment [Shipment]
    # @param ext [String, Symbol] File extension
    # @return [String]
    def filepath(shipment, ext)
      File.join(*[
        shipment.batch.name,
        ext.to_s,
        shipment.sku.stripe_id,
        filename(shipment, ext)
      ])
    end

    # @return [String]
    def bucket
      ENV.fetch('AWS_BUCKET')
    end
  end
end
