require 'csv'

module DirtyLemon
  class ExportShipmentOntrac

    # Generates a CSV document
    # @param shipments [ActiveRecord::Relation]
    # @return [String]
    def perform(shipments)
      CSV.generate do |csv|
        csv << headers

        shipments.each do |shipment|
          row = [
            shipping_fields(shipment.order.shipping),
            order_fields(shipment.order),
            sku_fields(shipment.sku),
            shipment_fields(shipment)
          ].flatten

          csv << row
        end
      end
    end


    private

    # @return [Array]
    def headers
      [
        'Name',
        'Address',
        'Address 2',
        'City',
        'State',
        'Zip',

        'Reference',

        'Reference 2',

        'Service',
        'Lbs',
        'Residential',
        'Satdelivery',
      ]
    end

    # @param sku [Sku]
    # @return [Array]
    def sku_fields(sku)
      [
        sku.product.name
      ]
    end

    # @param order [Order]
    # @return [Array]
    def order_fields(order)
      [
        order.reference
      ]
    end

    # @param shipping [Shipping]
    # @return [Array]
    def shipping_fields(shipping)
      [
        shipping.name,
        shipping.street1,
        shipping.street2,
        shipping.city,
        shipping.state,
        shipping.zip,
      ]
    end

    # @param shipment [Shipment]
    # @return [Array]
    def shipment_fields(shipment)
      [
        'C',    # Service level
        '11',   # Weight in lbs
        '1',    # True
        '0'     # False
      ]
    end
  end
end
