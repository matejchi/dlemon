# Collection of constants to handle recurring orders for vip customers
module DirtyLemon
  module Recurring
    RENEWAL_DELAY = 30.days

    # Delay between the renwal notice text and when the order will process
    TEXT_PAYMENT_DELAY = 2.days

    # First time, kickoff the process after 28 days
    INITAL_RENEWAL_DELAY = RENEWAL_DELAY - TEXT_PAYMENT_DELAY

    # Do not recur if the last order was placed in that interval
    MIN_RENEWAL_INTERVAL= 7.days
  end
end
