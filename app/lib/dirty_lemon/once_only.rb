module DirtyLemon
  class OnceOnly

    # Performs a block only once using Redis as a store
    # @param key [String, Array]
    # @param member [String]
    # @return [Boolean]
    def self.with(key, member, &block)
      if REDIS.sadd(['once', Array.wrap(key)].join(':'), member)
        yield
      end
    end
  end
end
