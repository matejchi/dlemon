require_relative '../../services/botpress_service'

module DirtyLemon
  class BotpressDispatcher
    attr_reader :service

    def initialize
      @service = BotpressService.new
    end

    # @param event [Event]
    # @return [True]
    def emit(event)
      return true unless ENV.member?('BOTPRESS_EVENTS') && ENV['BOTPRESS_EVENTS'].to_bool

      Rails.logger.tagged("BotpressDispatcher") do
        service.create_event(event.serialize)
        Rails.logger.info(event.attributes.slice(*%w(id name eventable_id eventable_type)).to_json.to_s)
      end

      true
    end
  end
end
