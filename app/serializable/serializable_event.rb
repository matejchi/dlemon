class SerializableEvent < JSONAPI::Serializable::Resource
  type :event
  attributes :id, :name, :data, :created_at

  attribute :created_at do
    @object.created_at.utc
  end
end
