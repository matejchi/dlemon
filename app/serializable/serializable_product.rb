class SerializableProduct < JSONAPI::Serializable::Resource
  type :product
  attribute :id { @object.id }
  attributes :name, :description
end
