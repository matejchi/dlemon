class SerializableSku < JSONAPI::Serializable::Resource
  type :sku
  attribute :id { @object.stripe_id }
  attributes :currency

  attribute :price do
    (@object.price * 100).to_i
  end

  attribute :vip_price do
    (@object.vip_price * 100).to_i
  end

  include_relation :product
end
