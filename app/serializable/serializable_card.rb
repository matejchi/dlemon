class SerializableCard < JSONAPI::Serializable::Resource
  type :cards
  attributes :customer_id, :exp_month, :exp_year, :last4
  attribute :url do
    @object.short_url
  end
end
