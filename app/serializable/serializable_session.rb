class SerializableSession < JSONAPI::Serializable::Resource
  type :session
  attributes :id, :customer_id, :tags, :topics, :created_at, :updated_at, :interactive

  include_relation :messages
end
