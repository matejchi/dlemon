class SerializableShipping < JSONAPI::Serializable::Resource
  type :shipping
  attributes :name, :company, :phone_number, :email, :street1, :street2, :city, :state, :country, :zip
end