class SerializableMessage < JSONAPI::Serializable::Resource
  type :message
  attributes :id, :session_id, :content, :content_type, :sender_role, :sent_at, :processed_at, :created_at

  attribute :customer_id do
    @object.session.customer_id if @object.session
  end
end
