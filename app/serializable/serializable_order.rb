class SerializableOrder < JSONAPI::Serializable::Resource
  type :order
  attributes :id, :customer_id, :shipping, :currency, :reference, :status, :recur

  attribute :coupon_codes do
    @object.coupons.map(&:code)
  end

  attribute :shipping_fee do
    (@object.shipping_fee * 100).to_i
  end

  attribute :total do
    (@object.total * 100).to_i
  end

  attribute :tax do
    (@object.tax * 100).to_i
  end

  attribute :created_at do
    @object.created_at.utc
  end

  attribute :updated_at do
    @object.updated_at.utc
  end

  include_relation :shipping
  include_relation :line_items
  include_relation :shipments
end
