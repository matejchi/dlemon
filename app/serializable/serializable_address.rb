class SerializableAddress < JSONAPI::Serializable::Resource
  type :addresses

  attributes :id, :street1, :street2, :city, :state, :country, :zip, :type, :lat,
             :lng, :created_at, :updated_at

  attribute :url do
    @object.short_url
  end
end
