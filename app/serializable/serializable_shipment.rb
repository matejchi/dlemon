class SerializableShipment < JSONAPI::Serializable::Resource
  type :shipment
  attributes :id, :order_id, :tracking_number, :carrier, :eta,
             :service, :tracking_url, :tracking_status, :tracking_status_at,
             :refunded_at

  attribute :created_at do
    @object.created_at.utc
  end

  attribute :updated_at do
    @object.updated_at.utc
  end

  include_relation :sku
end
