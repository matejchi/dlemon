class SerializableLineItem < JSONAPI::Serializable::Resource
  type :line_items
  attributes :id, :description, :quantity, :created_at, :updated_at

  attribute :amount do
    (@object.amount * 100).to_i
  end

  include_relation :sku
end
