class SerializableCustomer < JSONAPI::Serializable::Resource
  type :customer
  attributes :id, :first_name, :last_name, :reference, :country_code,
             :email, :first_name, :last_name, :needs_attention, :time_zone, :tags

  attribute :phone_number do
    if @object.confirmed?
      @object.phone_number
    else
      nil
    end
  end

  attribute :unconfirmed_phone_number do
    if @object.confirmed?
      nil
    else
      @object.unconfirmed_phone_number
    end
  end

  attribute :confirmed_at do
    @object.confirmed_at.try(:utc)
  end

  attribute :renew_recur_order_on do
    if @object.vip?
      @object.renew_recur_order_on.try(:utc)
    else
      nil
    end
  end

  attribute :created_at do
    @object.created_at.try(:utc)
  end

  attribute :updated_at do
    @object.updated_at.try(:utc)
  end

  attribute :unsubscribed do
    @object.unsubscribed?
  end

  attribute :shipping_rate do
    if @object.shipping_address
      (DirtyLemon::Settings.shipping_rate(@object.shipping_address[:country]) * 100).to_i
    end
  end

  include_relation :billing_address
  include_relation :shipping_address
  include_relation :card
end
