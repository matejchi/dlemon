class SerializableCoupon < JSONAPI::Serializable::Resource
  type :coupon
  attribute :id { @object.code }
end
