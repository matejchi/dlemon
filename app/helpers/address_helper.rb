module AddressHelper

  def formatted_address(address)
    [:street1, :street2, :city, :state, :country, :zip].map { |m| address.send(m) }.reject(&:blank?).join(", ")
  end
end
