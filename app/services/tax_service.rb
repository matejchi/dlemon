class TaxService < Service

  class Error < Service::ServiceError; end

  attr_accessor :client, :order, :amount

  def self.client
    @taxjar_client ||= if ENV.fetch('TAXJAR_FAKE', 'false').to_bool
                         DirtyLemon::Taxjar::FakeClient.new({})
                       else
                         Taxjar::Client.new(api_key: ENV['TAXJAR_API_KEY'])
                       end
  end

  def calculate(order)
    # Shipping will always be free to US – the only tax region we support
    tax_for_order_params = taxjar_params(order, amount: order.subtotal, shipping: 0.00)
    client.tax_for_order(tax_for_order_params)
  rescue Taxjar::Error => e
    Raven.capture_exception(e)
    raise Error, e.message
  end

  def create_order(order, charge_transaction)
    return nil if order.tax_rate.zero?

    order_params = taxjar_params(order,
                                 transaction_id: charge_transaction.id,
                                 transaction_date: charge_transaction.created_at,
                                 sales_tax: order.tax,
                                 amount: order.subtotal)
    client.create_order(order_params)
  end

  def refund_order(order, charge_transaction, refund_transaction)
    return nil if order.tax_rate.zero?

    # Recalculate if it doesn't exist for orders made through Stripe Order
    tax_rate = order.tax_rate || calculate(order).rate
    tax_refund = (tax_rate * refund_transaction.amount).round(2)

    refund_params = taxjar_params(order,
                                  transaction_id: refund_transaction.id,
                                  transaction_reference_id: charge_transaction.id,
                                  transaction_date: refund_transaction.created_at,
                                  amount: refund_transaction.amount - tax_refund,
                                  sales_tax: tax_refund)
    client.create_refund(refund_params)
  end


  private

  def taxjar_params(order, extra={})
    {
      to_country: order.shipping.country,
      to_zip: order.shipping.zip,
      to_city: order.shipping.city,
      to_state: order.shipping.state,
      shipping: order.shipping_fee,
    }.merge(extra)
  end

  def client
    self.class.client
  end
end
