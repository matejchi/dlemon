class S3Service < Service
  class Error < Service::ServiceError; end

  # Deletes an object from an AWS S3 bucket
  #
  # @param options [Hash]
  # @raise [Error]
  # @return [True]
  # @see http://docs.aws.amazon.com/sdk-for-ruby/v3/api/Aws/S3/Object.html#delete-instance_method
  def delete_object(options)
    report_exception do
      client.delete_object(options)
    end

    true
  end

  # Uploads an object to an AWS S3 bucket
  #
  # @param options [Hash]
  # @raise [Error]
  # @return [True]
  # @see http://docs.aws.amazon.com/sdk-for-ruby/v3/api/Aws/S3/Object.html#put-instance_method
  def upload_object(options)
    report_exception do
      client.put_object(options)
    end

    true
  end

  # Copies an object from an AWS S3 bucket to another S3 bucket
  #
  # @param options [Hash]
  # @raise [Error]
  # @return [True]
  # @see http://docs.aws.amazon.com/sdkforruby/api/Aws/S3/Client.html#copy_object-instance_method
  def copy_object(options)
    report_exception do
      client.copy_object(options)
    end

    true
  end


  private

  # @note Credentials are automatically set from env variables
  # @see https://github.com/aws/aws-sdk-ruby#configuration
  def client
    @client ||= Aws::S3::Client.new
  end

  def report_exception(&block)
    begin
      yield
    rescue Aws::S3::Errors::ServiceError => e
      logger.error(e)
      Raven.capture_exception(e)

      raise Error, e.message
    end
  end
end
