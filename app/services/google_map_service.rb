class GoogleMapService < Service
  class Error < Service::ServiceError; end

  attr_reader :geocode_endpoint
  attr_reader :timezone_endpoint

  def initialize
    @geocode_endpoint  = "https://maps.googleapis.com/maps/api/geocode/json"
    @timezone_endpoint = "https://maps.googleapis.com/maps/api/timezone/json"
  end

  # @param address [Address]
  # @raise [Error]
  # @return [[String, String], Nil]
  # @see https://developers.google.com/maps/documentation/geocoding
  def get_coordinates(address)
    format_address = ->(address) do
      [:street1, :street2, :city, :state, :country, :zip]
        .map { |m| address.try(m) }
        .reject(&:blank?).join(', ')
    end

    params = {
      address: format_address.call(address)
    }

    @response = request(:get, geocode_endpoint, params: params)

    if @response.results[0]
      location = @response.results[0].geometry.location
      return location.lat, location.lng
    end
  end

  # @param address [Address]
  # @raise [Error, ArgumentError]
  # @return [ActiveSupport::TimeZone, Nil]
  # @see https://developers.google.com/maps/documentation/timezone
  def get_time_zone(address)
    params = {
      location: [address.lat.to_f, address.lng.to_f].join(','),
      timestamp: Time.current.to_i
    }

    @response = request(:get, timezone_endpoint, params: params)

    if @response.status == 'OK'
      return Time.find_zone!(@response.timeZoneId).name
    end
  end


  private

  def client
    @client ||= HTTP::Client.new(headers: {
      'Accept'       => "application/json",
      'Content-Type' => "application/json",
    })
  end

  # @param method [String, Symbol]
  # @param path [String]
  # @raise [Error]
  # @return [OpenStruct]
  def request(method, path, params: {})
    response = client.send(method, path, params: params.merge(key: ENV['GOOGLE_API_TOKEN']))

    # Pass on errors when HTTP status included in 400 to 599
    if (400..599).include?(response.code)
      e = Error.new(response.reason)

      logger.error(e)
      Raven.capture_exception(e)

      raise e
    end

    wrap_response(response.parse)
  end

  # @param response [Hash]
  def wrap_response(response)
    JSON.parse(response.to_json, object_class: OpenStruct)
  end
end
