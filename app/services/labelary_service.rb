class LabelaryService < Service
  class Error < Service::ServiceError; end

  attr_reader :endpoint

  def initialize
    @endpoint = ENV.fetch('LABELARY_API_URL')
  end

  # Generates a PNG or PDF from ZPL
  #
  # @see http://labelary.com/service.html
  # @param zpl [String]
  # @param format [String, Symbol] One of 'png' or 'pdf'. Default to 'png'
  # @param options [Hash]
  # @param option :width The label width, in inches. Default to 4
  # @param option :height The label height, in inches. Default to 6
  # @param option :index The label index (base 0). Default to 0
  # @param option :dpmm One of '6dpmm', '8dpmm', '12dpmm', or '24dpmm'. Default to '8dpmm'
  # @param option :rotation One of 0, 90, 180 or 270. Default to 0
  # @return [String]
  def generate(zpl, format=:png, options={})
    options = {
      width: 4,  # inches
      height: 6, # inches
      index: 0,
      dpmm: '8dpmm',
      rotation: 0
    }.merge(options)

    formats = {
      'pdf' => 'application/pdf',
      'png' => 'image/png'
    }

    headers = {
      'Accept' => formats[format.to_s.downcase],
      'X-Rotation' => options[:rotation]
    }

    url = URI.join(endpoint, "/v1/printers/#{options[:dpmm]}/labels/#{options[:width]}x#{options[:height]}/#{options[:index]}/")

    response = nil
    report_exception do
      fight_throttle do
        response = client.post(url, body: zpl, headers: headers)
      end
    end

    response.body.to_s
  end


  private

  # @note The Labelary API is throttled at a maximum of 5 requests per second per client.
  # @see http://labelary.com/service.html#limits
  def fight_throttle
    attempts = 0
    response = nil

    while attempts < 10 do
      attempts += 1
      response = yield
      break unless response.code == 429
      sleep(Random.new.rand(0.2...1.2))
    end

    response
  end

  def client
    @client ||= HTTP::Client.new
  end

  def report_exception(&block)
    begin
      response = yield

      # Pass on errors when HTTP status included in 400 to 599
      if (400..599).include?(response.code)
        raise Error.new(response.parse)
      end

    rescue HTTP::Error
      raise Error.new(response.reason)
    rescue Error => e
      logger.error(e)
      Raven.capture_exception(e)

      raise e
    end
  end
end
