class BitlyService < Service
  class Error < Service::ServiceError; end

  # @param url [String]
  # @return [String] Bitly URL ID, also named hash
  def self.shorten(url)
    Bitly.client.shorten(url).global_hash
  end

  # @param id [String] Bitly URL ID, also named hash
  # @return [String] Bitly short URL
  def self.short_url(id)
    URI.join('https://bit.ly/', id)
  end
end
