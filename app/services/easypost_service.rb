class EasypostService < Service
  class Error < Service::ServiceError; end

  # Creates a shipment
  #
  # @see https://www.easypost.com/docs/api/ruby.html#create-a-shipment
  # @param to_address [Shipping]
  # @param fulfillment_address [FulfillmentAddress]
  # @param carrier_accounts [Array]
  # @param [Hash] options
  # @option options [Symbol] :label_date. Default to Time.current
  # @option options [Symbol] :description Shipment description
  # @option options [Symbol] :sku Shipment SKU
  # @option options [Symbol] :saturday_delivery Fetch rates for saturday delivery. Default to false
  # @option options [Symbol] :print1 Custom text to be printed on label
  # @option options [Symbol] :print2 Custom text to be printed on label
  # @return [EasyPost::Shipment]
  def create_shipment(to_address, fulfillment_address, carrier_accounts, options={})
    options = {
      label_date: Time.current,
      saturday_delivery: false
    }.merge(options)

    from_address = {
      company: "Dirty Lemon",
      name: "Dirty Lemon",
      phone: DirtyLemon::Settings.phone_number_voice.delete('+')
    }.merge(fulfillment_address.attributes.with_indifferent_access.slice(:email, :street1, :street2, :city, :state, :zip, :country))

    to_address = {
      company: to_address.name,
      phone: to_address.phone_number
    }.merge(to_address.attributes.slice(:name, :company, :street1, :street2, :city, :state, :zip, :country))

    parcel = DirtyLemon::Settings.parcel

    if to_address[:country] != 'US'
      customs_item = EasyPost::CustomsItem.create({
        description: options[:description],         # Description of item being shipped
        quantity: 6,                                # 6 items per case
        value: 10.83,                               # Average line item price ($)
        weight: parcel[:weight],                    # Total shipment weight (oz)
        code: options[:sku],                        # Shipment SKU
        hs_tariff_number: '2202.10.00',             # Harmonized Tariff Schedule
        origin_country: 'US',                       # Origin country
        currency: 'USD'                             # Currency code
      })

      customs_info = EasyPost::CustomsInfo.create({
        eel_pfc: 'NOEEI 30.37(a)',                  # Assumes shipment value < 2500$
        contents_type: 'merchandise',               # Type of content
        contents_explanation: 'Bottled beverage',   # Human readable description
        customs_certify: true,                      # Electronically certify the information provided
        customs_signer: 'Sommer Carroll',           # Signer for customs certify
        restriction_type: 'none',                   # Restriction type
        non_delivery_option: 'return',              # Non delivery option
        customs_items: [customs_item]
      })
    end

    payload = {
      from_address: from_address,
      to_address: to_address,
      parcel: parcel,
      carrier_accounts: carrier_accounts,
      customs_info: customs_info || nil,
      options: {
        incoterm: 'DDP',                                # Pass the cost and responsibility of duties on to DL
        delivery_confirmation: 'NO_SIGNATURE',          # Leave package at the door
        label_date: options[:label_date].utc.iso8601,   # Time at which the label is considered being purchased
        saturday_delivery: options[:saturday_delivery], # Enable rates for saturday delivery
        label_format: 'ZPL',                            # Use the Zebra Programming Language format
        print_custom_1: options[:print1],
        print_custom_2: options[:print2]
      }
    }

    ep_shipment = nil
    report_exception do
      ep_shipment = EasyPost::Shipment.create(payload)
    end

    if ep_shipment.messages.any?
      error = ep_shipment.messages.select { |m| m.type != 'rate_error' }.first
      raise Error.new(error.message) if error
    end

    ep_shipment
  end

  # Purchases a shipping label and returns its URL
  #
  # @param shipment [Shipment]
  # @param ep_shipment [EasyPost::Shipment]
  # @param ep_rate [EasyPost::Rate]
  # @raise [Error]
  # @return [EasyPost::Shipment]
  def purchase_shipping_label(shipment, ep_shipment, ep_rate)
    report_exception do
      ep_shipment = ep_shipment.buy(rate: ep_rate)
    end

    ep_shipment
  end

  # Refunds a shipping label
  #
  # @see https://www.easypost.com/docs/api/ruby.html#refunds
  # @param shipment [Shipment]
  # @raise [Error]
  # @return [EasyPost::Shipment]
  def refund_shipping_label(shipment)
    ep_shipment = nil
    report_exception do
      ep_shipment = EasyPost::Shipment.retrieve(shipment.easypost_shipment_id)
      ep_shipment.refund
    end

    ep_shipment
  end

  # Validates an address using the EasyPost address validation API
  #
  # @see https://www.easypost.com/docs/api/ruby.html#create-and-verify-addresses
  # @param address [Api::Address] A ActiveModel compatible object
  # @return [Address] Returns a modified address object
  def self.validate_address(address)
    response = nil
    begin
      response = EasyPost::Address.create(address.attributes.merge(verify: ['delivery']))
    rescue EasyPost::Error => e
      Raven.capture_exception(e)
      address.errors.add(:base, :invalid, message: "Address is invalid")
      return address
    end

    # Use standardized data from EasyPost
    address.street1 = response.street1
    address.street2 = response.street2
    address.city    = response.city
    address.state   = response.state
    address.country = response.country
    address.zip     = response.zip

    unless response.verifications.delivery.success
      address.errors.add(:base, :invalid, message: "Address is invalid")

      response.verifications.delivery.errors.each do |error|
        address.errors.add(error.field, :invalid, message: error.message) if address.respond_to?(error.field)
      end
    end

    address
  end


  private

  def report_exception(&block)
    begin
      yield
    rescue EasyPost::Error => e
      logger.error(e)
      Raven.capture_exception(e)

      raise Error, e.message
    end
  end
end
