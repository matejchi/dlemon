class TwilioService < Service
  class Error < Service::ServiceError; end

  # Validates a phone number using the Twilio phone number lookup API
  #
  # @param phone_number [String]
  # @return [OpenStruct, false] Valid and normalized phone number object
  def validate(phone_number)
    begin
      @response = client.lookups.phone_numbers(phone_number.delete(' ')).fetch
      OpenStruct.new(phone_number: @response.phone_number, country_code: @response.country_code)
    rescue Twilio::REST::RestError => e
      false
    end
  end


  private

  def client
    @client ||= Twilio::REST::Client.new
  end

  def report_exception(&block)
    begin
      yield
    rescue Twilio::REST::RestError => e
      logger.error(e)
      Raven.capture_exception(e)

      raise Error, e.message
    end
  end
end
