class BotpressService < Service
  class Error < Service::ServiceError; end

  attr_reader :endpoint

  def initialize
    @endpoint = ENV.fetch('BOTPRESS_API_URL')
  end

  # Sends an event
  #
  # @param data [Hash] Event payload
  # @raise [Error]
  # @return [Boolean]
  def create_event(data)
    request(:post, endpoint, 'botpress-dirtylemon/webhook', body: data)
  end

  # Sends a trigger
  # @param name [String] Trigger name
  # @param customer [Customer]
  # @param data [Hash]
  def trigger(name, customer, data)
    payload = {
      name: name,
      customer_id: customer.id,
      data: data
    }

    request(:post, endpoint, 'botpress-dirtylemon/trigger', body: payload)
  end


  private

  def client
    @client ||= HTTP::Client.new(headers: {
      'Accept'        => "application/json",
      'Authorization' => "Bearer #{ENV['BOTPRESS_API_TOKEN']}",
      'Content-Type'  => "application/json",
    })
  end

  # @param verb [String, Symbol]
  # @param base [String]
  # @param path [String]
  # @raise [Error]
  # @return [True]
  def request(verb, base, path, params: {}, body: {})
    uri = URI.join(base, path)
    response = client.request(verb, uri, { params: params, json: body })

    # Pass on errors when HTTP status included in 400 to 599
    if (400..599).include?(response.code)
      e = nil
      begin
        e = Error.new(response.parse)
      rescue HTTP::Error
        e = Error.new([response.reason, "URI: #{uri}"].join(' : '))
      end

      logger.error(e)
      Raven.capture_exception(e)

      raise e
    end

    true
  end
end
