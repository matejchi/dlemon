class UpdateOrdersAddressAction

  def self.call(customer)

    orders = Order.unshipped.where(customer_id: customer.id)

    new_shipping = Shipping.from_customer(customer)

    orders.each do |order|
      order.shipping = new_shipping
      order.save!
      Event.create(name: 'order.updated', eventable: order, data: order.serialize)

      if order.status == 'queued'
        order.shipments.where(refunded_at: nil).ids.each do |id|
          RefundShippingLabelJob.perform_async(id)
        end

        order.create_shipments_by_line_item!

        order.shipments.where(purchased_at: nil).ids.each do |shipment_id|
          PurchaseShippingLabelJob.perform_async(shipment_id, order.state_machine.last_transition.id)
        end

        OrderMailer.confirmation(order.id, true).deliver_later
      end
    end

  end

end
