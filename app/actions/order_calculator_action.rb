class OrderCalculatorAction

  attr_accessor :order

  def initialize(order)
    @order = order
  end

  def prepare
    @order.item_total = @order.line_items.sum(&:amount)
    @order.item_discount = [calculate_item_discount(@order.item_total), @order.item_total].min
    @order.subtotal = @order.item_total - @order.item_discount
    @order.shipping_fee = @order.shipping.rate * @order.line_items.sum(&:quantity)

    tax = TaxService.new.calculate(@order)
    @order.tax = tax.amount_to_collect
    @order.tax_rate = tax.rate

    @order.total = @order.subtotal + @order.shipping_fee + @order.tax
  end


  private

  def calculate_item_discount(subtotal)
    @order.coupons.map { |coupon| coupon.compute(subtotal) }.sum
  end
end
