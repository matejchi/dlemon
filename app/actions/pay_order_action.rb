class PayOrderAction
  attr_accessor :order

  def self.call(*args)
    new(*args).call
  end

  def initialize(order)
    @order = order
  end

  def call
    return true if order.total.zero?

    order.customer.tags -= [Customer::VIP_FAILED_CARD_TAG]

    if order.customer.vip?
      order.customer.touch_renew_recur_order_on
      order.customer.save!
    end

    stripe_charge = charge!

    begin
      # Could be abstracted elsewhere so we can retry if this fails
      # (ideas: add it to a queue or use Stripe webhooks?)
      order.transactions.create!(type: ChargeTransaction,
                                 customer: order.customer,
                                 amount: stripe_charge.amount.fdiv(100),
                                 status: stripe_charge.status,
                                 stripe_id: stripe_charge.id,
                                 # `.try` to to work around ruby-stripe-mock not
                                 # having the outcome field yet. Remove and update once
                                 # this is merged: https://github.com/rebelidealist/stripe-ruby-mock/pull/493
                                 outcome: stripe_charge.try(:outcome))
    rescue StandardError => error
      Raven.capture_message("[ACTION REQUIRED] Failed recording payment data for order #{order.id}", level: 'fatal')
      Raven.capture_exception(error)
    end

  rescue Stripe::CardError => e
    BotpressTriggerJob.perform_async('order.charge-failed', order.customer_id, order.serialize.as_json)
    order.errors.add(:base, :card_error, message: e.message)
    raise ActiveRecord::RecordInvalid.new(order)
  end


  private

  def charge!
    # Only pay once per order per card
    idempotency_key = order.id + order.customer.card.attributes.hash.to_s

    Stripe::Charge.create(
      {
        customer: order.customer.stripe_id,
        amount: (order.total * 100).to_i,
        currency: order.currency,
        description: "Payment for order ##{order.reference}",
        metadata: {
          order_id: order.id,
          order_reference: order.reference
        }},
        { idempotency_key: idempotency_key }
    )
  end
end
