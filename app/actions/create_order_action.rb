class CreateOrderAction

  attr_accessor :customer, :order, :destination, :line_items, :coupons

  def initialize(customer:, destination:, line_items:, coupons:, recur: false)
    @customer = customer
    @line_items = line_items
    @coupons = Array.wrap(coupons)

    # Order shipping attributes are set according to this priority:
    # 1. Shipping data passed in request params
    # 2. Customer shipping address data
    @destination = Shipping.from_customer(@customer, destination || {})

    @order = @customer.orders.build(shipping: @destination, vip: @customer.vip?, recur: recur)
  end

  def valid?
    # trigger validations
    (@order.valid? && @order.shipping.valid?).tap do
      if @order.shipping.errors.any?
        @order.shipping.errors.full_messages.each do |message|
          @order.errors.add(:shipping, :invalid, message: message)
        end
      end
    end
  end

  def prepare
    @line_items.each do |payload|
      # TODO: change sku reference to something good
      @order.line_items.build(sku: Sku.find_by!(stripe_id: payload[:sku]), quantity: payload[:quantity])
    end


    # add coupons declared on the order
    @coupons.each { |c| @order.coupons << Coupon.find_by!(code: c) }
  end

  def place!
    @calculator = OrderCalculatorAction.new(@order)
    @calculator.prepare
    @order.save!
  end

end
