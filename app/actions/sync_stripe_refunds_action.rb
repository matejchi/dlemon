class SyncStripeRefundsAction

  def self.call(stripe_charge)
    new(stripe_charge).call
  end

  def initialize(stripe_charge)
    @stripe_charge = stripe_charge
  end

  def call
    if charge_transaction.nil?
      Rails.loggger.info("[#{self.class.name}] skipping refund for #{@stripe_charge.id} – charge not found")
      return []
    end

    existing_refund_refs = order.transactions.type(RefundTransaction).map(&:stripe_id)
    new_refunds = @stripe_charge.refunds.reject do |refund|
      existing_refund_refs.include?(refund.id)
    end

    new_transactions = Order.transaction do
      new_refunds.map do |refund|
        order.transactions.create!(
          type: RefundTransaction,
          customer: order.customer,
          stripe_id: refund.id,
          status: refund.status,
          amount: -refund.amount.fdiv(100),
        )
      end
    end

    new_transactions.each do |transaction|
      RefundTaxJob.perform_async(transaction.id)
    end

    new_transactions
  end


  private

  def charge_transaction
    @charge_transaction ||= Transaction.find_by(stripe_id: @stripe_charge.id)
  end

  def order
    @order ||= charge_transaction&.order
  end

end
