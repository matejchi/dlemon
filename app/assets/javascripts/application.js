//= require jquery
//= require rails-ujs
//= require_tree .

$(document).ready(function() {
  var creditCardForm = $('#js-credit-card');

  if (creditCardForm.length !== 0) {
    creditCard(creditCardForm);
  };
});
