function creditCard(form) {
  var stripe = Stripe($('meta[name="stripe-key"]').attr('content'));
  var elements = stripe.elements();

  // Get card API endpoint
  var cardEndpoint = $('meta[name="card-url"]').attr('content')

  // Create an instance of the card Element
  var card = elements.create('card');

  // Add an instance of the card Element into the `card-element` <div>
  card.mount('#js-card-element');

  var formElement = $('#js-credit-card');
  var errorElement = $('#js-error');
  var successElement = $('#js-success');
  var submitElement = $('#js-submit');

  function setMessage(message) {
    if (!message) {
      errorElement.hide();
    }
    else {
      errorElement.text(message);
      errorElement.show();
    }
  }

  function setOutcome(result) {
    form.find(':submit').prop('disabled', false);

    // Send token to API
    if (result.token) {
      form.find(':submit').prop('disabled', true);

      $.post(cardEndpoint, { stripeToken: result.token.id }, function(data, status, response) {
        successElement.show();
        submitElement.hide();
      }).fail(function(response, status, error) {
        form.find(':submit').prop('disabled', false);
        setMessage(response.responseJSON.error.base.join(', '));
      });

    // Handle real-time validation errors from the card Element.
    } else if (result.error) {
      setMessage(result.error.message);
      form.find(':submit').prop('disabled', true);
    } else {
      setMessage(null);
    }
  }

  card.on('change', function(event) {
    setOutcome(event);
  });

  // Handle form submission
  form.on('submit', function(e) {
    e.preventDefault();
    var extraDetails = {
      name: form.find('input[name=cardholder-name]').value,
    };

    stripe.createToken(card, extraDetails).then(setOutcome);
  });
};
