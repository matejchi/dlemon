class RefundShippingLabelJob
  include Sidekiq::Worker

  # @param id [String] Shipment id
  def perform(id)
    @shipment = Shipment.find(id)
    return if @shipment.refunded_at.present?
    return if @shipment.easypost_shipment_id.nil?

    ActiveRecord::Base.transaction do
      @shipment.update!(refunded_at: Time.current)
      EasypostService.new.refund_shipping_label(@shipment)
    end
  end
end
