class RefundTaxJob
  include Sidekiq::Worker

  def perform(transaction_id)
    refund_transaction = Transaction.find(transaction_id)
    stripe_refund = Stripe::Refund.retrieve(refund_transaction.stripe_id)
    charge_transaction = Transaction.find_by(stripe_id: stripe_refund.charge)

    TaxService.new.refund_order(refund_transaction.order, charge_transaction, refund_transaction)
  end
end
