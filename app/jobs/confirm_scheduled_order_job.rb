class ConfirmScheduledOrderJob
  include Sidekiq::Worker

  def perform(id)
    Rails.logger.tagged(self.class.name, "order=#{id}") do |logger|
      @order = Order.find(id)
      @customer = @order.customer

      return if halt_not_scheduled
      return if halt_recur_not_vip
      return if halt_recur_recent_order

      begin
        pay_order!
      rescue ActiveRecord::RecordInvalid => e
        is_card_error = @order.errors.details[:base].any? { |details| details[:error] == :card_error }

        if is_card_error
          @customer.reload
          @order.reload.state_machine.transition_to!(:created)
          customer_mark_failed_card! if @customer.vip?
          return logger.info("Charge failed, bot will be notified")
        else
          raise e
        end
      end

      queue_order!

      if @order.status != 'queued'
        raise "Order not queued – job ended unexpectedly"
      end
    end
  end


  private

  def halt_not_scheduled
    return false if @order.status == 'scheduled'

    logger.info("Order no longer scheduled, status=#{@order.status}")
    true
  end

  def halt_recur_not_vip
    return false unless @order.recur? && !@customer.vip?

    logger.info("Clearing vip and transitioning order to `created`, recur=true and customer isn't VIP anymore")
    @order.state_machine.transition_to!(:created)
    clear_customer_vip_status!
    true
  end

  def halt_recur_recent_order
    return false unless @order.recur?

    recent_order = @customer.orders.processed.most_recent.where('created_at > ?', @order.created_at).first
    return false unless recent_order.present?

    logger.info("Transitioning order to `created`, customer completed an order after " +
                "getting renewal notice, recent_order.id=#{recent_order.id}")
    @order.state_machine.transition_to!(:created)
    true
  end

  def pay_order!
    if @order.state_machine.can_transition_to?(:paid)
      @order.state_machine.transition_to!(:paid)
      Event.create(name: 'order.updated', eventable: @order, data: @order.serialize)
    end
  end

  def queue_order!
    if @order.state_machine.can_transition_to?(:queued)
      @order.state_machine.transition_to!(:queued)
      Event.create(name: 'order.updated', eventable: @order, data: @order.serialize)
    end
  end

  def customer_mark_failed_card!
    @customer.tags += [Customer::VIP_FAILED_CARD_TAG]
    @customer.save!
  end

  def clear_customer_vip_status!
    @customer.clear_vip
    @customer.save!
  end

  def logger
    Rails.logger
  end
end
