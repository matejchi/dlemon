class CreateStripeCustomerJob
  include Sidekiq::Worker

  def perform(id)
    @customer = Customer.find(id)
    @stripe_customer = Stripe::Customer.create(email: @customer.email)
    @customer.update!(stripe_id: @stripe_customer.id)
  end
end
