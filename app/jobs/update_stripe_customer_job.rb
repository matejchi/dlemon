class UpdateStripeCustomerJob
  include Sidekiq::Worker

  def perform(id)
    customer = Customer.find(id)

    @stripe_customer = customer.stripe_resource
    @stripe_customer.email = customer.email

    if customer.shipping_address.present?
      @stripe_customer.shipping = {
        address: {
          line1: customer.shipping_address.street1,
          line2: customer.shipping_address.street2,
          city: customer.shipping_address.city,
          state: customer.shipping_address.state,
          country: customer.shipping_address.country,
          postal_code: customer.shipping_address.zip
        },
        name: customer.full_name,
        phone: customer.phone_number
      }
    end

    @stripe_customer.save
  end
end
