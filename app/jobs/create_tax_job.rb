class CreateTaxJob
  include Sidekiq::Worker

  def perform(transaction_id)
    charge_transaction = Transaction.find(transaction_id)
    TaxService.new.create_order(charge_transaction.order, charge_transaction)
  end
end

