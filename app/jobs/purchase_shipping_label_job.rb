class PurchaseShippingLabelJob
  include Sidekiq::Worker
  include Sidekiq::RetryMonitoring::MonitoredWorker

  # @param id [String] Shipment id
  def perform(id, last_transition_id = nil)
    @shipment = Shipment.find(id)
    purchase_at = Time.current

    return if @shipment.purchased_at.present?

    # Skip if shipment's order is in a state where it doesn't make sense to purchase a label anymore
    # return if ['unqueued', 'canceled', 'returned'].include?(@shipment.order.status)

    @ep_shipment, @ep_rate, @fulfillment_address = router.route(@shipment, purchase_at)
    @ep_shipment = @ep_service.purchase_shipping_label(@shipment, @ep_shipment, @ep_rate)

    # Persist selected route informations
    @shipment.update!(Shipment.from_easypost(@ep_shipment).merge({
      purchased_at: purchase_at,
      fulfillable_at: @ep_shipment.options.label_date,
      fulfillment_address: @fulfillment_address
    }))

    if oudated_transition?(last_transition_id)
      RefundShippingLabelJob.perform_async(id)
    else
      GenerateShippingLabelJob.perform_async(id, @ep_shipment.postage_label.label_url)
    end
  end

  def warn(jid, id, *_rest)
    shipment = Shipment.find(id)
    order = shipment.order

    data = {
      channel: "customerservice",
      icon_emoji: ":package:",
      attachments: [{
        "title" => "Failed to purchase label for order ##{order.reference}",
        "title_link" => "https://admin.dirtylemon.com/orders/#{order.reference}",
        "color": "#ff3333",
        "fields": [
          {
            "title": "Address",
            "value": order.shipping.one_line,
            "short": true
          }
        ]
      }]
    }

    NotifySlackJob.perform_async(data.stringify_keys)
  end


  private

  def ep_service
    @ep_service ||= EasypostService.new
  end

  def router
    @router ||= DirtyLemon::ShipmentRouter.new(ep_service, DirtyLemon::Settings.carrier_accounts, FulfillmentAddress.all)
  end

  def oudated_transition?(last_transition_id)
    return false unless last_transition_id

    # Ensure any pending state transition are over or stopped by locking on
    # the order, which gets updated on every state transition
    @shipment.order.with_lock do
      @shipment.order.state_machine.last_transition.id != last_transition_id
    end
  end
end
