class NotifySlackJob
  include Sidekiq::Worker

  def perform(data)
    unless ENV['SLACK_API_TOKEN'].present?
      return Rails.logger.info("No Slack API Token – would have sent: #{data.inspect}")
    end

    client = Slack::Web::Client.new(token: ENV['SLACK_API_TOKEN'])
    client.chat_postMessage(data.symbolize_keys)
  end
end

