class FulfillOrdersJob
  include Sidekiq::Worker

  # @param id [String] Batch id
  def perform(id)
    @batch = Batch.find(id)
    @fulfillment_address = @batch.fulfillment_address
    @enqueued_at = @batch.enqueued_at

    # - shipment must not belong to a batch
    # - shipment must have been created in the last 7 days
    # - shipment must be purchased and never refunded
    # - order must not belong to a batch
    # - order must be queued
    @shipments = Shipment.where({
      batch_id: nil,
      created_at: @enqueued_at.weeks_ago(1).at_midnight..@enqueued_at,
      fulfillment_address: @fulfillment_address,
      refunded_at: nil
    })
    .where.not(tracking_number: nil)
    .joins(:order)
    .where(orders: {
      batch_id: nil,
      status: 'queued'
    })

    # Abort silently when there are no shipments
    return if @shipments.none?

    # Use clean relations
    @shipment_ids = @shipments.ids
    @shipments = Shipment.where(id: @shipment_ids)
    @order_ids = @shipments.select(:order_id).distinct.pluck(:order_id)
    @orders = Order.where(id: @order_ids)

    # Fullfill orders
    @orders.find_each do |order|
      ActiveRecord::Base.transaction do
        begin
          order.state_machine.transition_to!(:fulfilled)
          order.update!(batch_id: @batch.id)
          @shipments.where(order: order).update_all(batch_id: @batch.id)
        rescue Statesman::TransitionFailedError, Statesman::GuardFailedError => e
          raise ActiveRecord::Rollback, e.message
        end
      end
    end

    # Copy shipping labels for orders that were fulfilled into warehouse AWS S3 bucket
    @shipments.joins(:order).where(orders: { status: 'fulfilled' }).each do |shipment|
      CopyShippingLabelJob.perform_async(shipment.id)
    end

    # Send fulfillment email to warehouse
    OrderMailer.fulfillment(@batch.id, @fulfillment_address.id).deliver_later
  end
end
