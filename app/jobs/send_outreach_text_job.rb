class SendOutreachTextJob
  include Sidekiq::Worker

  def perform(outreach_id)
    @outreach = Outreach.find(outreach_id)
    @customer = @outreach.customer
    @campaign_details = Outreach.campaign_details(@outreach.campaign_name)

    if @campaign_details.blank?
      return Rails.logger.info("Skipping unknown outreach campaign: #{@outreach.campaign_name}")
    end

    @outreach.with_lock do
      if @outreach.sent_at.present?
        return Rails.logger.info("Skipping sent outreach: ##{outreach_id}")
      end

      mark_as_sent!
      persist_message!
      send_sms
    end
  rescue Twilio::REST::RestError => e
    if e.message =~ /violates a blacklist rule/i
      Customer.transaction do
        @outreach.destroy
        @customer.update_attribute(:unsubscribed_at, Time.current)
      end
    elsif e.message =~ /is not a mobile number/
      @outreach.destroy
    else
      raise e
    end
  end


  private

  def mark_as_sent!
    @outreach.update_attribute(:sent_at, Time.current)
  end

  def persist_message!
    Session.create_message!(@customer, {
      sender_role: 'outreach',
      content_type: 'text',
      content: @campaign_details[:content],
      sent_at: Time.now
    })
  end

  def send_sms
    data = {
      from: DirtyLemon::Settings.localized_phone_number(@customer.country_code),
      to: @customer.phone_number || @customer.unconfirmed_phone_number,
      body: @campaign_details[:content],
      media_url: @campaign_details[:media_url],
    }

    client.messages.create(data)
  end

  def client
    Twilio::REST::Client.new
  end
end
