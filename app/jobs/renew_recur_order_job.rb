class RenewRecurOrderJob

  include Sidekiq::Worker

  def perform(customer_id)
    Rails.logger.tagged(self.class.name, "customer=#{customer_id}") do |logger|
      @customer = Customer.find(customer_id)
      @template_order = @customer.orders.processed.most_recent.first
      @resulting_order = nil

      if @customer.renew_recur_order_on.future?
        return logger.info('`renew_recur_order_on` is the future, skipping. Job queued multiple times?')
      end

      unless @customer.vip?
        logger.info('No longer VIP, nullifying `renew_recur_order_on`')
        return @customer.update_attributes!(renew_recur_order_on: nil)
      end

      unless @template_order.present?
        logger.info('Customer has no order to renew, resetting VIP status')
        @customer.clear_vip
        return @customer.save!
      end

      most_recent_order = @customer.orders
                                   .processed
                                   .where('created_at >= ?', DirtyLemon::Recurring::MIN_RENEWAL_INTERVAL.ago)
                                   .most_recent
                                   .first

      if most_recent_order.present?
        Raven.capture_message("Prevented customer from being double charged. " \
                              "This should not happen, any payment should bump the recur date")
        @customer.touch_renew_recur_order_on
        @customer.save!
        return logger.info('skipping until next month, has recent order. ' \
                           "id=#{most_recent_order.id} " \
                           "recur=#{most_recent_order.recur.inspect}")
      end

      ActiveRecord::Base.transaction do
        @customer.touch_renew_recur_order_on
        @customer.save!
        @resulting_order = duplicate_order!
        @resulting_order.scheduled_confirm_on = Time.current + DirtyLemon::Recurring::TEXT_PAYMENT_DELAY
        @resulting_order.state_machine.transition_to!(:scheduled)
      end

      # String key are intended: Sidekiq raises on symbols
      BotpressTriggerJob.perform_async('order.renew-notice', customer_id, @resulting_order.serialize.as_json)
    end
  end

  private

  def duplicate_order!
    line_items_params = @template_order.line_items.map do |line_item|
      { sku: line_item.sku.stripe_id, quantity: line_item.quantity }
    end

    order_action = CreateOrderAction.new(customer: @customer,
                                         destination: {},
                                         line_items: line_items_params,
                                         coupons: [],
                                         recur: true)

    order_action.prepare

    raise ActiveRecord::RecordInvalid, order_action.order unless order_action.valid?

    order_action.place!
    order_action.order.save!
    order_action.order
  end
end
