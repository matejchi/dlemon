class EmitEventJob
  include Sidekiq::Worker
  sidekiq_options queue: :events

  # @param id [String] Event ID
  # @param dispatcher [String]
  def perform(id, dispatcher)
    @event = Event.find(id)

    @dispatcher = dispatcher.constantize.new

    @dispatcher.emit(@event)
  end
end
