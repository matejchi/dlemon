class GeocodeJob
  include Sidekiq::Worker

  # @param id [String] Address id
  def perform(id)
    begin
      @address = Address.find(id)
    rescue ActiveRecord::RecordNotFound
      return # noop
    end

    @service = GoogleMapService.new

    # Get Latitude and Longitude
    lat, lng = @service.get_coordinates(@address)

    # Get timezone, fallback on default for address
    time_zone = @service.get_time_zone(@address) || @address.default_time_zone

    ActiveRecord::Base.transaction do
      @address.update!(lat: lat, lng: lng) if lat && lng
      @address.customer.update!(time_zone: time_zone) if time_zone
    end
  end
end
