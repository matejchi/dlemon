class DeleteCustomerJob
  include Sidekiq::Worker

  # @param id [String] Customer id
  def perform(id)
    @customer = Customer.find(id)

    ActiveRecord::Base.transaction do
      @customer.orders
        .select { |order| order.state_machine.can_transition_to?(:canceled) }
        .each { |order| order.state_machine.transition_to!(:canceled) }
      @customer.destroy!
    end
  end
end
