class CopyShippingLabelJob
  include Sidekiq::Worker

  # @param id [String] Shipment id
  def perform(id)
    @shipment = Shipment.find(id)

    # Copy shipping label files to AWS S3
    DirtyLemon::ShipmentHandler.new.copy_to_warehouse(@shipment)
  end
end
