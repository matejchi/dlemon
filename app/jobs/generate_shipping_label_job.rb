class GenerateShippingLabelJob
  include Sidekiq::Worker

  # @param id [String] Shipment id
  # @param zpl_url [String] ZPL file url representing a shipping label
  def perform(id, zpl_url)
    @shipment = Shipment.find(id)

    DirtyLemon::ShipmentHandler.new.generate_shipping_labels(@shipment, zpl_url)
  end
end
