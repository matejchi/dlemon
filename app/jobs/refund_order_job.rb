class RefundOrderJob
  include Sidekiq::Worker

  def perform(id)
    order = Order.find(id)
    transactions = order.transactions

    charge_transaction = transactions.charge
    return unless charge_transaction.present?

    Stripe::Refund.create(charge: charge_transaction.stripe_id)
  end
end
