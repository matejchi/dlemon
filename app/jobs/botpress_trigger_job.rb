class BotpressTriggerJob
  include Sidekiq::Worker

  def perform(name, customer_id, data)
    customer = Customer.find(customer_id)
    BotpressService.new.trigger(name, customer, data)
  end
end
