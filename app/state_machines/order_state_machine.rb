class OrderStateMachine
  include Statesman::Machine

  state :created, initial: true
  state :scheduled
  state :paid
  state :queued
  state :unqueued
  state :fulfilled
  state :canceled
  state :returned

  transition from: :created,   to: [:scheduled, :canceled, :paid]
  transition from: :scheduled, to: [:created, :canceled, :paid]
  transition from: :paid,      to: [:queued, :canceled]
  transition from: :queued,    to: [:fulfilled, :unqueued]
  transition from: :unqueued,  to: [:queued, :canceled]
  transition from: :fulfilled, to: :returned

  guard_transition(to: :scheduled) do |order|
    # Ensure order can be paid to catch errors early on
    order.state_machine.can_transition_to?(:paid)
    order.errors.add(:scheduled_confirm_on, :required, message: "Order must have a valid confirm date") unless order.scheduled_confirm_on.present?

    order.errors.none?
  end

  # Required:
  # - order with shipping: email, name, street1, city, state, country, zip
  # - customer with card if total is > $0
  guard_transition(to: :paid) do |order|
    order.errors.add(:shipping, :invalid, message: "Order must have valid shipping information") unless order.shipping.valid?
    order.errors.add(:shipping, :invalid, message: "Order must have a valid shipping phone number") unless order.shipping.phone_number.present?
    order.errors.add(:shipping, :invalid, message: "Order must have a valid shipping email") unless order.shipping.email.present?
    order.errors.add(:customer, :invalid, message: "Customer must have a valid card") unless order.total.zero? || order.customer.try(:card).try(:valid?)

    order.errors.none?
  end

  # Required:
  # - order with shipping: phone number, name, street1, city, state, country, zip
  guard_transition(to: :queued) do |order, transition|
    order.errors.add(:shipping, :invalid, message: "Order must have valid shipping information") unless order.shipping.valid?
    order.errors.add(:shipping, :invalid, message: "Order must have a valid shipping phone number") unless order.shipping.phone_number.present?

    order.errors.none?
  end

  before_transition(from: :scheduled) do |order, transition|
    order.scheduled_confirm_on = nil
  end

  before_transition(to: :paid) do |order, transition|
    PayOrderAction.call(order)
  end

  before_transition(to: :queued) do |order, transition|
    order.create_shipments_by_line_item!
  end

  after_transition(to: :queued, after_commit: true) do |order, transition|
    # Purchase shipping labels
    order.shipments.where(refunded_at: nil, purchased_at: nil).pluck(:id).each do |shipment_id|
      PurchaseShippingLabelJob.perform_async(shipment_id, transition.id)
    end
  end

  before_transition(from: :queued, to: :unqueued) do |order, transition|
    # - Skip refund if shipment hasn't been purchased yet
    # - Skip refund if shipment has already been refunded
    order.shipments.where.not(easypost_shipment_id: nil).or(order.shipments.where.not(refunded_at: nil)).ids.each do |id|
      RefundShippingLabelJob.perform_async(id)
    end
  end

  after_transition(to: [:returned, :canceled], after_commit: true) do |order, transition|
    RefundOrderJob.perform_async(order.id)
  end

  after_transition(to: :paid, after_commit: true) do |order, transition|
    # Free orders don't have a charge
    if order.transactions.charge.present?
      # Even though we're in `after_commit`, the transaction is sometimes wrapped
      # in another transaction, which delays the "actual" commit, and thus code
      # runs first.
      CreateTaxJob.perform_in(30.seconds, order.transactions.charge.id)
    end

    OrderMailer.confirmation(order.id).deliver_later
  end

  # Persist state on the model object
  after_transition do |model, transition|
    model.update!(status: transition.to_state)
  end
end
