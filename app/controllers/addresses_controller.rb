class AddressesController < ApplicationController
  before_action :set_customer
  before_action :set_address

  def show
  end

  # GET /customers/1/addresses/1/edit
  def edit
  end

  # PATCH /customers/1/addresses/1
  def update
    # Validate address
    # TODO Show feedback for normalized address changes
    # TODO Uncomment address checking if billing address edit link is ever implemented
    @address.assign_attributes(address_params)

    if @address.valid?
      EasypostService.validate_address(@address)

      if @address.errors.none? && @address.save
        Event.create(name: 'address.updated', eventable: @address, data: @address.serialize)
        GeocodeJob.perform_async(@address.id)

        UpdateOrdersAddressAction.call(@customer) #if @address.is_a?(ShippingAddress)

        redirect_to(customer_address_path(customer_id: @customer.to_param, id: @address.to_param, t: params[:t]),
          flash: { success: "Your address has been successfuly updated" })
      else
        render :edit
      end
    else
      render :edit
    end
  end


  private

  # @raise [ActionController::ParameterMissing]
  def address_params
    params.require(:address).permit(:street1, :street2, :city, :state, :country, :zip)
  end

  # @raise [ActiveRecord::RecordNotFound]
  def set_customer
    @customer = Customer.find(params[:customer_id])
  end

  # @raise [ActiveRecord::RecordNotFound]
  def set_address
    @address = @customer.addresses.find(params[:id])
  end
end
