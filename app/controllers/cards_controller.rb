class CardsController < ApplicationController

  # GET /customers/1/card/edit
  def edit
    @customer = Customer.find(params[:customer_id])
    @card = @customer.card
  end
end
