class Stripe::EventsController < ApplicationController
  skip_before_action :verify_authenticity_token

  def create
    return head :ok if test_event_in_prod?
    @event = Stripe::Event.retrieve(params[:id])

    # @NOTE: Stripe events *may* fire multiple times, but we don't have to
    # worry about that since our handlers are idempotent.
    handler_name = "webhook_#{@event.type.gsub('.', '_')}"
    send(handler_name) if respond_to?(handler_name, true)

    head :ok
  rescue Stripe::InvalidRequestError => error
    # Ignore 404 for test events – most likely a dev machine
    # triggering events on the staging webhook
    return head :ok if error.http_status == 404 && test_event?
    raise error
  end


  private

  def webhook_charge_refunded
    SyncStripeRefundsAction.call(@event.data.object)
  rescue ActiveRecord::RecordNotFound => error
    if test_event?
      Rails.logger.info("Swalling ActiveRecord::RecordNotFound, event isn't in live mode. " +
                        "Message: #{error.message}")
    else
      raise error
    end
  end

  def webhook_charge_refund_updated
    Raven.capture_message("[Action required] Stripe refund update failed", extra: {
      stripe_event_id: @event.id,
      refund_id: @event.data.object.id
    })
  end

  def stripe_params
    params.require(:order).permit!
  end

  def test_event_in_prod?
    Stripe.api_key.start_with?('sk_live') && test_event?
  end

  def test_event?
    !params[:livemode]
  end
end
