class Api::V1::BotController < Api::V1::ApplicationController

  # POST /bot/trigger
  def trigger
    @resource_class = trigger_params[:resource_type].safe_constantize
    forbidden_error("Invalid resource_type") && return unless @resource_class

    forbidden_error("Invalid name") && return unless trigger_actions.include?(trigger_params[:name])

    @resource = @resource_class.find(trigger_params[:resource_id])

    @customer = nil
    case @resource_class.to_s
    when 'Customer'
      @customer = @resource
    when 'Order'
      @customer = @resource.customer
    else
      forbidden_error("Invalid resource_type") && return
    end

    if @customer.unsubscribed?
      forbidden_error("Customer is unsubscribed")
      return
    end

    unless @customer.phone_number? || @customer.unconfirmed_phone_number?
      forbidden_error("Customer must have a valid phone number")
      return
    end

    BotpressService.new.trigger(trigger_params[:name], @customer, @resource.serialize)

    head 200
  end


  private

  # @raise [ActionController::ParameterMissing]
  def trigger_params
    params.require(:trigger).permit(:name, :resource_id, :resource_type).tap do |param|
      param.require(:name)
      param.require(:resource_id)
      param.require(:resource_type)
    end
  end

  # List of available bot triggers
  # @return [Array]
  def trigger_actions
    ['order.confirmation', 'order.confirmed']
  end
end
