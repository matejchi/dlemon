class Api::V1::AddressesController < Api::V1::ApplicationController
  before_action :authenticate

  # GET /addresses/1
  def show
    @address = ::Address.find(params[:id])
    render resource: @address
  end

  # POST /customers/1/addresses
  def create
    @customer = Customer.find(params[:customer_id])

    @api_address = Api::Address.new(address_params)

    # Validate address
    if perform_validation?
      @api_address.valid? && EasypostService.validate_address(@api_address)
      raise ActiveRecord::RecordInvalid.new(@api_address) if @api_address.errors.any?
    end

    # Create identical addresses since most customers will have the same shipping
    # and billing address
    @shipping_address = @customer.shipping_address
    @billing_address = @customer.billing_address

    ActiveRecord::Base.transaction do
      @shipping_address.update!(@api_address.attributes)
      @billing_address.update!(@api_address.attributes)
    end

    GeocodeJob.perform_async(@shipping_address.id)

    Event.create(name: 'address.created', eventable: @shipping_address, data: @shipping_address.serialize)
    Event.create(name: 'address.created', eventable: @billing_address, data: @billing_address.serialize)

    render resource: @shipping_address, status: 201
  end

  # PATCH /addresses/1?validate=false
  def update
    @address = ::Address.find(params[:id])

    @api_address = Api::Address.new(address_params)

    # Validate address
    if perform_validation?
      @api_address.valid? && EasypostService.validate_address(@api_address)
      raise ActiveRecord::RecordInvalid.new(@api_address) if @api_address.errors.any?
    end

    @address.update!(@api_address.attributes.merge(lat: nil, lng: nil))

    Event.create(name: 'address.updated', eventable: @address, data: @address.serialize)

    UpdateOrdersAddressAction.call(@address.customer) if @address.is_a?(ShippingAddress)

    GeocodeJob.perform_async(@address.id)

    render resource: @address
  end

  # POST /addresses/validate
  def validate
    @api_address = Api::Address.new(address_params)

    # Validate address
    @api_address.valid? && EasypostService.validate_address(@api_address)
    raise ActiveRecord::RecordInvalid.new(@api_address) if @api_address.errors.any?

    render resource: @api_address
  end

  private

  # @raise [ActionController::ParameterMissing]
  # @return [ActionController::Parameters]
  def address_params
    payload = params.require(:address)

    # Required by EasyPost address validation API
    payload.require(:street1)
    payload.require(:city)
    payload.require(:state)
    payload.require(:country)
    payload.require(:zip)

    payload.permit(:street1, :street2, :city, :state, :country, :zip)
  end

  # @return [Boolean]
  def perform_validation?
    return true unless params[:validate].present?
    params[:validate].to_bool
  end
end
