class Api::V1::SkusController < Api::V1::ApplicationController
  before_action :authenticate

  # GET /skus
  def index
    @skus = Sku.all
    render resource: @skus
  end
end
