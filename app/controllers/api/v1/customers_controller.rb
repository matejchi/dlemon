class Api::V1::CustomersController < Api::V1::ApplicationController
  before_action :authenticate
  before_action :set_customer, only: [:show, :update, :destroy, :confirm, :generate_confirmation, :subscribe, :unsubscribe, :stats]
  before_action :enforce_phone_number, only: [:confirm, :generate_confirmation, :subscribe, :unsubscribe]

  # GET /customers?phone_number=1234567890
  def index
    phone_number = params.require(:phone_number)
    @customers = Customer.where(phone_number: phone_number)
      .or(Customer.where(unconfirmed_phone_number: phone_number)).all

    render resource: @customers
  end

  # GET /customers/1
  def show
    render resource: @customer
  end

  # POST /customers
  def create
    ActiveRecord::Base.transaction do
      @customer = Customer.create!(customer_params)
      @customer.bootstrap
    end

    # Confirm customer
    if @customer.unconfirmed_phone_number? && should_perform_confirmation?
      @customer.update!(confirmed_at: Time.current, phone_number: @customer.unconfirmed_phone_number)
    end

    render resource: @customer, status: 201
  end

  # PATCH /customers/1
  def update
    @customer.update!(customer_params.except(:unconfirmed_phone_number))
    Event.create(name: 'customer.updated', eventable: @customer, data: @customer.serialize)

    render resource: @customer
  end

  # DELETE /customers/1
  def destroy
    DeleteCustomerJob.perform_async(@customer.id)

    head 204
  end

  # POST /customers/1/confirm
  def confirm
    # Don't allow multiple confirmations
    forbidden_error("Customer is already confirmed") && return if @customer.confirmed?

    # Don't allow to confirm a customer without phone number
    forbidden_error("Customer must have a phone number") && return unless @customer.unconfirmed_phone_number?

    if @customer.confirm(params.require(:customer).require(:confirmation_token))
      @customer.phone_number = @customer.unconfirmed_phone_number
      @customer.save!
    else
      @customer.errors.add(:confirmation_token, :invalid)
      raise ActiveRecord::RecordInvalid.new(@customer)
    end

    Event.create(name: 'customer.updated', eventable: @customer, data: @customer.serialize)

    render resource: @customer
  end

  # POST /customers/1/generate_confirmation
  def generate_confirmation
    # Don't allow multiple confirmations
    forbidden_error("Customer is already confirmed") && return if @customer.confirmed?

    @customer.generate_confirmation_token!

    # Include the verification token in this event's data payload, allowing
    # services receiving it, like the Bot, to send it to the Customer
    Event.create({
      name: 'customer.verification',
      eventable: @customer,
      data: @customer.serialize.merge(confirmation_token: @customer.confirmation_token)
    })

    head 201
  end

  # POST /customers/1/subscribe
  def subscribe
    # Don't allow multiple subscriptions
    forbidden_error("Customer is already subscribed") && return unless @customer.unsubscribed?

    @customer.update!(unsubscribed_at: nil)
    Event.create(name: 'customer.subscribed', eventable: @customer, data: @customer.serialize)

    head 204
  end

  # POST /customers/1/unsubscribe
  def unsubscribe
    # Don't allow multiple unsubscriptions
    forbidden_error("Customer is already unsubscribed") && return if @customer.unsubscribed?

    @customer.update!(unsubscribed_at: Time.current)
    Event.create(name: 'customer.unsubscribed', eventable: @customer, data: @customer.serialize)

    head 204
  end

  #GET /v1/customers/need_attention
  def need_attention
    render json: { count: Customer.needs_attention.count }
  end

  #GET /v1/customers/:id/stats
  def stats
    render json: { avg_shipping_cost: @customer.avg_shipping_cost.round(2),
                   total_lifetime: @customer.total_lifetime.round(2) }
  end

  private

  # @raise [ActionController::ParameterMissing]
  def customer_params
    params.require(:customer).permit(:first_name, :last_name, :email, :unconfirmed_phone_number,
                                     :needs_attention, :renew_recur_order_on, tags: [])
  end

  # @raise [ActiveRecord::RecordNotFound]
  def set_customer
    # @customer = Customer.find(params[:id])
    @customer = Customer.joins(:shipping_address, :billing_address, :card).includes(:shipping_address, :billing_address, :card).find(params[:id])
  end

  # Don't allow action without customer phone number
  def enforce_phone_number
    unless @customer.phone_number? || @customer.unconfirmed_phone_number?
      forbidden_error("Customer must have a valid phone number")
    end
  end

  # @return [Boolean]
  def should_perform_confirmation?
    params[:confirmation].present? ? params[:confirmation].to_bool : false
  end
end
