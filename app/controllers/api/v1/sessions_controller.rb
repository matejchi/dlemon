class Api::V1::SessionsController < Api::V1::ApplicationController
  before_action :set_customer, only: [:show, :update]
  before_action :authenticate

  # GET /sessions/recent
  def recent
    @sessions = Session.interactive
      .order(created_at: :desc)
      .distinct(:customer_id)
      .includes(:customer)
      .preload(:messages)
      .joins(:customer)
      .limit(params[:limit] || 100)
      .offset(params[:offset] || 0)

    if params[:needs_attention].in?(['true', 'false'])
      @sessions = @sessions.where(customers: {needs_attention: params[:needs_attention]})
    end

    render resource: @sessions
  end

  # GET /customers/1/sessions
  def index
    if params[:customer_id]
      @customer = Customer.find(params[:customer_id])
      @sessions = @customer.sessions.order(created_at: :desc)
        .preload(:messages)
        .limit(params[:limit] || 100)
        .offset(params[:offset] || 0)
    else
      @sessions = Session.order(created_at: :desc)
        .distinct(:customer_id)
        .preload(:messages)
        .limit(params[:limit] || 100)
        .offset(params[:offset] || 0)
    end

    render resource: @sessions
  end

  # GET /customers/1/sessions/1
  # GET /customers/1/sessions/current
  def show
    if params[:id] == 'current'
      @session = Session.current!(@customer)
    else
      @session = @customer.sessions.find(params[:id])
    end

    render resource: @session
  end

  # PATCH /customers/1/sessions/1
  # PATCH /customers/1/sessions/current
  def update
    if params[:id] == 'current'
      @session = Session.current!(@customer)
    else
      @session = @customer.sessions.find(params[:id])
    end

    ActiveRecord::Base.transaction do
      @session.update!(session_params)
    end

    render resource: @session, status: 200
  end


  private

  # @raise [ActionController::ParameterMissing]
  def session_params
    params.require(:session).permit(tags: [], topics: [])
  end

  # @raise [ActiveRecord::RecordNotFound]
  def set_customer
    @customer = Customer.find(params[:customer_id])
  end
end
