class Api::V1::CardsController < Api::V1::ApplicationController

  # POST /customers/1/card
  def create
    @customer = Customer.find(params[:customer_id])
    ActiveRecord::Base.transaction do
      @customer.card.persist_from_stripe(params.require(:stripeToken))
      @customer.save!
    end

    @card = @customer.card

    Event.create(name: 'card.created', eventable: @card, data: @card.serialize)

    # FIXME: should rescue ActiveRecord::InvalidRecord & return 400
    render resource: @card, status: 201
  end
end
