class Api::V1::AnalyticsController < Api::V1::ApplicationController

  def index
    render json: ::Analytics::MetricsBuilder.build_metrics(metrics, context, filters)
  end

  private

  def metrics
    params.require(:metrics)
  end

  def context
    params.require(:context).permit(:resource, :id).to_h.with_indifferent_access
  end

  def filters
  end
end

