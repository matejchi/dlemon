class Api::V1::MessagesController < Api::V1::ApplicationController
  before_action :enforce_current, only: [:create]
  before_action :authenticate

  # GET /customers/1/sessions/1/messages
  # GET /customers/1/sessions/current/messages
  def index
    @customer = Customer.find(params[:customer_id])

    if params[:session_id] == 'current'
      @session = Session.current!(@customer)
    else
      @session = @customer.sessions.find(params[:session_id])
    end

    @messages = @session.messages.order(sent_at: :desc)
      .limit(params[:limit] || 100)
      .offset(params[:offset] || 0)

    render resource: @messages
  end

  # POST /customers/1/sessions/current/messages
  def create
    ActiveRecord::Base.transaction do
      @customer = Customer.find(params[:customer_id])

      if @customer.unsubscribed?
        forbidden_error("Customer is unsubscribed")
        return
      end

      unless @customer.phone_number? || @customer.unconfirmed_phone_number?
        forbidden_error("Customer must have a valid phone number")
        return
      end

      @message = Session.create_message!(@customer, message_params)
      @session = @message.session
    end

    render resource: @message, status: 201
  end


  private

  # @raise [ActionController::ParameterMissing]
  def message_params
    params.require(:message).permit(:content, :content_type, :sender_role, :sent_at)
  end

  def enforce_current
    raise ActiveRecord::RecordNotFound if params[:session_id] != 'current'
  end
end
