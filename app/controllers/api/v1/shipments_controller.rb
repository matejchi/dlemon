class Api::V1::ShipmentsController < Api::V1::ApplicationController
  include ActionController::HttpAuthentication::Basic::ControllerMethods

  # POST /shipments/webhook
  def webhook
    authorize; return if performed?

    if event.description == 'tracker.updated'
      @tracker = EasyPost::Tracker.construct_from(params[:result].to_unsafe_hash)

      @shipment = Shipment.find_by!(easypost_tracker_id: @tracker.id)

      @shipment.tracking_status    = @tracker.status
      @shipment.tracking_status_at = DateTime.parse(@tracker.updated_at)
      @shipment.eta                = @tracker.est_delivery_date.present? ? DateTime.parse(@tracker.est_delivery_date) : nil

      @shipment.save!

      Event.create(name: 'shipment.updated', eventable: @shipment, data: @shipment.serialize)
    end
    
    head 200
  end


  private

  def event_params
    params.permit(:id, :object, :created_at, :updated_at, :description, :mode,
      :previous_attributes, :pending_urls)
  end

  # @return [EasyPost::Event]
  def event
    EasyPost::Event.construct_from(event_params.to_unsafe_hash)
  end

  # Authenticates the request with HTTP basic auth
  def authorize
    if authenticate_with_http_basic do |_, password|
        ActiveSupport::SecurityUtils.secure_compare(password, ENV.fetch('EASYPOST_WEBHOOK_SECRET'))
      end
    else
      request_http_basic_authentication
    end
  end
end
