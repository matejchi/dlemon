class Api::V1::CouponsController < Api::V1::ApplicationController
  before_action :authenticate

  # GET /coupons
  def index
    @coupons = Coupon.all

    render resource: @coupons
  end
end
