class Api::V1::OrdersController < Api::V1::ApplicationController

  before_action :set_order, except: [:index, :create]
  before_action :authenticate

  # GET /customers/1/orders/1
  def index
    @customer = Customer.find(params[:customer_id])
    @orders = @customer.orders.order(created_at: :desc)
      .limit(params[:limit] || 100)
      .offset(params[:offset] || 0)

    render resource: @orders
  end

  # GET /orders/1
  def show
    render resource: @order
  end

  # POST /customers/1/orders
  def create
    @customer = Customer.find(params[:customer_id])

    dest = order_params[:shipping] || {}

    # temporary, till all consumers update
    if order_params.include?(:coupon)
      coupon_codes = [order_params[:coupon]].select(&:present?)
    else
      coupon_codes = order_params[:coupons]
    end

    @order_action = CreateOrderAction.new(customer: @customer,
                                          destination: dest,
                                          line_items: order_params[:line_items],
                                          recur: order_params[:recur],
                                          coupons: coupon_codes)

    # raise if there's an issue with order validity (mostly, shipping)
    raise ActiveRecord::RecordInvalid, @order_action.order unless @order_action.valid?

    # setup line items, coupons etc
    @order_action.prepare

    # action it...
    @order_action.place!

    render resource: @order_action.order, status: 201
  end

  # PATCH /orders/1
  def update
    if %w[queued canceled fulfilled].include?(@order.status)
      forbidden_error("Can't update a #{@order.status} order") && return
    end

    @order.shipping.attributes = order_params[:shipping].to_unsafe_hash

    if @order.shipping.valid?
      @order.update!(shipping: @order.shipping)
    else
      raise ActiveRecord::RecordInvalid, @order.shipping
    end

    Event.create(name: 'order.updated', eventable: @order, data: @order.serialize)

    render resource: @order, status: 200
  end

  # POST /orders/1/schedule
  def schedule
    @order.scheduled_confirm_on = params.require(:order).require(:scheduled_confirm_on)
    backfill_empty_shipping!

    # Unlike other endpoints, allow "transitions" to schedule even if scheduled
    # to reschedule to a different date
    if @order.status == 'scheduled'
      @order.save!
      Event.create(name: 'order.updated', eventable: @order, data: @order.serialize)

      render resource: @order
    else
      process_transition!(:scheduled)
    end
  end

  # POST /orders/1/pay
  def pay
    backfill_empty_shipping!
    process_transition!(:paid)
  end

  # POST /orders/1/queue
  def queue
    backfill_empty_shipping!
    process_transition!(:queued)
  end

  # POST /orders/1/unqueue
  def unqueue
    process_transition!(:unqueued)
  end

  # POST /orders/1/confirm
  def confirm
    backfill_empty_shipping!

    raise ActiveRecord::RecordInvalid, @order unless @order.transition!(:paid)
    Event.create(name: 'order.updated', eventable: @order, data: @order.serialize)

    raise ActiveRecord::RecordInvalid, @order unless @order.transition!(:queued)
    Event.create(name: 'order.updated', eventable: @order, data: @order.serialize)

    render resource: @order
  end

  # POST /orders/1/fulfill
  def fulfill
    process_transition!(:fulfilled)
  end

  # POST /orders/1/cancel
  def cancel
    process_transition!(:canceled)
  end

  # POST /orders/1/return
  def return
    process_transition!(:returned)
  end


  private

  def process_transition!(state)
    raise ActiveRecord::RecordInvalid, @order unless @order.transition!(state)
    Event.create(name: 'order.updated', eventable: @order, data: @order.serialize)
    render resource: @order
  end

  def backfill_empty_shipping!
    @order.shipping.email ||= @order.customer.email
    @order.shipping.phone_number ||= @order.customer.phone_number || @order.customer.unconfirmed_phone_number
    @order.save!
  end

  # @raise [ActionController::ParameterMissing]
  def order_params
    params.require(:order).permit(:coupon, :coupons, :recur,
                                  line_items: [:quantity, :sku],
                                  shipping: [:name, :company, :phone_number, :email, :street1, :street2, :city, :state, :country, :zip])
  end

  # @raise [ActiveRecord::RecordNotFound]
  def set_order
    @order = Order.find(params[:order_id] || params[:id])
  end

end
