class Api::ApplicationController < ActionController::API
  include ApplicationHelper

  after_action :tag_request, if: -> { current_user.present? }

  rescue_from StandardError, with: :internal_server_error unless Rails.env.development?
  rescue_from ActiveRecord::RecordNotFound, with: :not_found_error
  rescue_from ActiveRecord::RecordInvalid, with: :invalid_error
  rescue_from ActiveRecord::RecordNotUnique, with: :conflict_error
  rescue_from ActionController::ParameterMissing, with: :parameter_missing_error
  rescue_from ActionController::BadRequest, with: :bad_request_error

  # Returns the current user on successful authentication
  # @return [User, Nil]
  def current_user
    @current_user
  end

  # Authenticates the current user based on headers token
  def authenticate
    unauthorized_error unless authenticate_by_header
  end

  # Sets the current user from headers
  # Expects authorization header in this format:
  #   Authorization: Bearer WCZZYjnOQFUYfJIN2ShH1iD24UHo58A6TI
  # @return [Boolean]
  def authenticate_by_header
    return false if request.authorization.blank?

    token, _ = ActionController::HttpAuthentication::Token.token_and_options(request)

    @current_user = User.where.not(token: nil).find_by(token: token)
  end


  private

  # Enrich AppSignal report
  def tag_request
    Raven.user_context(id: current_user.id)
    Appsignal.tag_request(user: current_user.id)
  end

  # @param message [String, Hash]
  def bad_request_error(message)
    error('BadRequest', message, 400)
  end

  def unauthorized_error
    self.headers["WWW-Authenticate"] = %(Token realm="Application")
    error('Unauthorized', "No valid API key provided.", 401)
  end

  # @param message [String, Hash]
  def forbidden_error(message=nil)
    error('Forbidden', message || "Unauthorized to complete that action.", 403)
  end

  # @param e [ActiveRecord::RecordNotFound]
  def not_found_error(e)
    error('RecordNotFound', "The requested #{e.model} doesn't exist.", 404)
  end

  def conflict_error
    error('Conflict', "The requested resource already exist.", 409)
  end

  # @param e [ActiveRecord::RecordInvalid]
  def invalid_error(e)
    error('RecordInvalid', e.record.errors.to_hash, 422)
  end

  # @param e [ActiveRecord::ParameterMissing]
  def parameter_missing_error(e)
    error('ParameterMissing', e.message, 422)
  end

  # @param e [StandardError]
  def internal_server_error(e)
    logger.error(e)
    Raven.capture_exception(e)
    error('InternalServerError', "Sorry, there is a problem on our side.")
  end

  # @param code [String]
  # @param message [String, Hash]
  # @param status [Integer]
  def error(code, message, status=500)
    render json: { code: code, error: message }, status: status
  end
end
