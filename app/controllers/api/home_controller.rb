class Api::HomeController < Api::ApplicationController

  # GET, POST api.domain.tld
  def index
    render json: { message: 'Hello! You are reaching DirtyLemon\'s API.' }, status: 200
  end
end
