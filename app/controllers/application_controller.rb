class ApplicationController < ActionController::Base
  include ApplicationHelper

  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  after_action :tag_request, if: -> { @customer.present? }

  private

  # Enrich AppSignal report
  def tag_request
    Raven.user_context(id: @customer.id)
    Appsignal.tag_request(customer: @customer.id)
  end
end
