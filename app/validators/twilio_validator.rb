class TwilioValidator

  # @param record [ApplicationRecord]
  def initialize(record)
    @record = record
  end

  # Validates the existence of a phone number using Twilio
  def validate
    if phone = TwilioService.new.validate(@record.unconfirmed_phone_number)
      @record.unconfirmed_phone_number = phone.phone_number
      @record.country_code             = phone.country_code
    else
      @record.errors.add(:unconfirmed_phone_number, :invalid)
    end
  end
end
