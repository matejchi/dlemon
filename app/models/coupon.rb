class Coupon < ApplicationRecord
  include SerializableResource

  has_many :order_coupons
  has_many :orders, through: :order_coupons

  validates :code, presence: true, uniqueness: true

  def compute(amount)
    if amount_off.present?
      [amount_off, amount].min
    else
      (amount * percent_off.fdiv(100)).round(2)
    end
  end
end
