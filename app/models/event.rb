class Event < ApplicationRecord
  include SerializableResource

  belongs_to :eventable, polymorphic: true, required: false
  validates :name, presence: true

  after_commit on: :create do
    EmitEventJob.perform_async(id, 'DirtyLemon::ActionCableDispatcher')
    EmitEventJob.perform_async(id, 'DirtyLemon::BotpressDispatcher')
  end
end
