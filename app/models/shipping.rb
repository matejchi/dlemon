class Shipping
  include Virtus.model(nullify_blank: true)
  include ActiveModel::Validations
  include SerializableResource
  include Serializable

  attribute :name, String
  attribute :company, String
  attribute :phone_number, String
  attribute :email, String
  attribute :street1, String
  attribute :street2, String
  attribute :city, String
  attribute :state, String
  attribute :country, String
  attribute :zip, String

  validates :name, :street1, :city, :state, :country, :zip, presence: true
  validates :email, format: { with: Devise.email_regexp }, if: -> { email.present? }

  # For user serializable need return id
  # @return [nil]
  def id
    nil
  end

  # returns the shipping rate for this address
  # @return [Numeric]
  def rate
    DirtyLemon::Settings.shipping_rate(country)
  end

  # Return a configured Shipping from a customer
  # @param [Customer]
  # @param overrides [Hash]
  # @return [Shipping]
  def self.from_customer(customer, overrides={})
    attrs = ActiveSupport::HashWithIndifferentAccess.new({
      name: customer.full_name,
      phone_number: customer.phone_number || customer.unconfirmed_phone_number,
      email: customer.email
    })

    if customer.shipping_address
      attrs.merge!(customer.shipping_address.attributes.symbolize_keys
        .slice(:street1, :street2, :city, :state, :country, :zip))
    end

    attrs.merge!(overrides)

    new(attrs)
  end

  def one_line
    [name, company, street1, street2, city, state, country, zip].select(&:present?).join(', ')
  end
end
