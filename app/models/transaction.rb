class Transaction < ApplicationRecord
  belongs_to :order
  belongs_to :customer

  scope :for_type, -> (t){ where(type: "#{t.to_s.titleize}Transaction") }

  before_validation :set_default_customer, on: :create


  private

  def set_default_customer
    self.customer ||= order.customer
  end
end
