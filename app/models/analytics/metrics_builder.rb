module Analytics
  class MetricsBuilder

    def self.build_metrics(metrics, context, filters)
      klass_name = context[:resource]
      id         = context[:id]

      resource_klass = "::#{klass_name.camelize}".constantize rescue nil
      resource_metrics_klass = "Analytics::Metrics::#{klass_name.camelize}".constantize rescue nil

      unless resource_klass && resource_metrics_klass
        raise ActionController::BadRequest.new("could not process context #{klass_name} for metrics")
      end

      {
        resource: klass_name,
        id:       id,
        metrics:  resource_metrics_klass.new(klass_name, id, filters).build_metrics(metrics)
      }
    end

  end
end
