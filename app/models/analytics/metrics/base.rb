module Analytics
  module Metrics
    class Base
      attr_reader :resource_klass, :resource_id

      def initialize(resource_klass, resource_id, filters)
        @resource_klass  = resource_klass
        @resource_id = resource_id
      end

      def build_metrics(metrics)
        return resource_missing unless resource_klass.find_by_id(resource_id)

        metrics.inject({}) do |result, metric|
          result[metric] = send(metric)
          result
        end
      end

      def resource_missing
        { error: 404, msg: "couldn't find #{resource_klass} with id #{resource_id}" }
      end

      def method_missing(name, *args, &block)
        { error: 404, msg: "metric not found" }
      end

    end
  end
end
