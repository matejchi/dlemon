module Analytics
  module Metrics
    class Customer < Base

      def last_order
        ::Order.where(customer_id: resource_id).last
      end

      def most_ordered_sku
        ::Sku.joins(line_items: [:order]).where(orders: { customer_id: resource_id })
          .select('skus.*, count(orders.id) as order_count')
          .group('skus.id').order('order_count desc').first
      end

      def total_orders_summary
        {
          usd:   ::Order.where(customer_id: resource_id).sum(:total),
          count: ::Order.where(customer_id: resource_id).count
        }
      end

    end
  end
end
