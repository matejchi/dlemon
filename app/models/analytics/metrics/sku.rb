module Analytics
  module Metrics
    class Sku < Base

      def last_order
        ::Order.joins(line_items: [:sku]).where('line_items.sku_id = (?)', resource_id).order(:created_at).last
      end

      def total_orders_summary
        {
          usd:   ::Order.joins(line_items: [:sku]).where('line_items.sku_id = (?)', resource_id).sum(:total),
          count: ::Order.joins(line_items: [:sku]).where('line_items.sku_id = (?)', resource_id).count
        }
      end

    end
  end
end
