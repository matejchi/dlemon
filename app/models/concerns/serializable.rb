module Serializable
  extend ActiveSupport::Concern

  class_methods do
    # @param obj [Hash] A hash-like object
    def load(obj)
      new(obj)
    end

    # @param obj [Hash] A hash-like object
    # @return [Hash]
    def dump(obj)
      new(obj).to_h
    end
  end

  # @return [Boolean]
  def empty?
    attributes.values.all?(&:blank?)
  end
end
