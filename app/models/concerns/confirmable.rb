# Confirmable
#
# Expects a model with these attributes:
#   :confirmation_token, String # A unique random token
#   :confirmed_at, DateTime # A timestamp when the user entered the confirmation token
#   :confirmation_generated_at, DateTime # A timestamp when the confirmation_token was generated (not sent)
module Confirmable
  extend ActiveSupport::Concern

  included do
    cattr_reader :confirm_within
    self.class_variable_set("@@confirm_within", 24.hours)
  end

  # @param token [String]
  # @return [Boolean]
  def confirm(token)
    if confirmation_period_expired?
      self.errors.add(:confirmation_token, :confirmation_period_expired, period: Devise::TimeInflector.time_ago_in_words(self.class.confirm_within.ago))
      return false
    end

    if token == confirmation_token
      self.confirmed_at = Time.current
    end

    confirmed?
  end

  # @return [Boolean]
  def confirmed?
    confirmed_at?
  end

  # @return [Boolean]
  def generate_confirmation_token!
    update!(confirmation_token: SecureRandom.random_number(90000..100000), confirmation_generated_at: Time.current)
  end

  private

  # @return [Boolean]
  def confirmation_period_expired?
    self.class.confirm_within && self.confirmation_generated_at && (Time.current > self.confirmation_generated_at + self.class.confirm_within)
  end
end
