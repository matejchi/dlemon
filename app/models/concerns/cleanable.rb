module Cleanable
  extend ActiveSupport::Concern

  class_methods do
    def clean(*list)
      cattr_accessor :clean_attributes
      self.clean_attributes = list
    end
  end

  included do
    before_validation :strip_whitespace

    def strip_whitespace
      clean_attributes.compact.each { |attribute| send("#{attribute}=", send(attribute).try(:strip)) }
    end
  end
end
