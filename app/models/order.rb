class Order < ApplicationRecord
  include SerializableResource

  # Let's consider these statuses as processed- we shouldn't order on top of those
  PROCESSED_ORDER_STATUSES = %w[fulfilled queued paid unqueued].freeze
  UNSHIPPED_STATUSES = %w[created scheduled queued].freeze

  belongs_to :customer, required: true
  belongs_to :batch, required: false

  has_many :line_items, dependent: :destroy, autosave: true
  has_many :shipments, dependent: :destroy
  has_many :transitions, class_name: 'OrderTransition', autosave: false
  has_many :events, as: :eventable, dependent: :destroy
  has_many :transactions, dependent: :destroy do
    def type(cls)
      self.select { |txn| cls === txn }
    end

    def charge
      self.type(ChargeTransaction).first
    end
  end

  has_many :order_coupons
  has_many :coupons, through: :order_coupons

  validates :currency, :reference, presence: true
  validates :tax, :total, :shipping_fee, presence: true, on: :update
  validates :reference, uniqueness: { case_sensitive: true }

  serialize :shipping, Shipping

  after_initialize :set_defaults, unless: :persisted?

  before_create :set_vip_status
  before_validation :set_reference, on: :create

  after_commit on: :create do
    Event.create(name: 'order.created', eventable: self, data: self.serialize)
  end

  scope :recur, -> { where(recur: true) }
  scope :processed, -> { where(status: PROCESSED_ORDER_STATUSES) }
  scope :most_recent, -> { order(created_at: :desc).limit(1) }
  scope :unshipped, -> { where(status: UNSHIPPED_STATUSES) }

  # @return [OrderStateMachine]
  def state_machine
    @state_machine ||= OrderStateMachine.new(
      self,
      transition_class: OrderTransition,
      association_name: :transitions
    )
  end

  def transition!(destination)
    if state_machine.can_transition_to?(destination)
      if block_given? # Only wrap in a transaction if required
        ActiveRecord::Base.transaction do
          yield
          state_machine.transition_to!(destination)
        end
      else
        state_machine.transition_to!(destination)
      end

      true
    else
      errors.add(:status, :invalid, message: "Can't transition order to '#{destination}'")

      false
    end
  end

  def create_shipments_by_line_item!
    # Each case of product is handled as a single shipment by the warehouse
    line_items.each do |line_item|
      line_item.quantity.times { shipments.create!(sku: line_item.sku) }
    end
  end


  private

  # Sets the order's default attribute values
  def set_defaults
    self.status       = 'created'
    self.currency     = 'usd'
    self.tax          = 0
    self.total        = 0
    self.shipping_fee = 0
  end

  def set_vip_status
    self.vip = customer&.vip?
  end

  # Sets a short, unique reference ID
  def set_reference
    self.reference = loop do
      ref = SecureRandom.hex(3)
      break ref unless Order.exists?(reference: ref)
    end
  end
end
