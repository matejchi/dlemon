class FulfillmentAddress < ApplicationRecord
  acts_as_paranoid
  has_many :shipments

  validates :street1, :city, :state, :country, :zip, presence: true
  validates :carrier_facility, inclusion: { in: %w(njfs spwg) }
end
