class Message < ApplicationRecord
  include SerializableResource

  belongs_to :session, required: true
  has_many :events, as: :eventable, dependent: :destroy

  validates :content, :content_type, :sender_role, :sent_at, presence: true
  validates :content_type, inclusion: { in: %w(text) }
  validates :sender_role, inclusion: { in: %w(end-user agent bot notification outreach) }

  before_save :sync_session_sender_roles

  after_commit on: :create do
    Event.create(name: 'message.created', eventable: self, data: self.serialize)
  end


  private

  def sync_session_sender_roles
    session.sender_roles = (session.sender_roles + [sender_role]).uniq
    session.save if session.changed?
  end
end
