class Address < ApplicationRecord
  include SerializableResource
  include Cleanable

  clean :street1, :street2, :city, :state, :country, :zip

  belongs_to :customer, required: true
  has_many :events, as: :eventable, dependent: :destroy

  validates :street1, :city, :state, :country, :zip, presence: true

  # @return [Integer]
  # @note Price is per shipment
  def shipping_rate
    {
      'GB' => 12,
      'AU' => 24
    }[country] || 0
  end

  # @return [String]
  def short_url
    edit_url
  end

  def default_time_zone
    DirtyLemon::TimeZoneMapping.find(country, state)
  end


  private

  def edit_url
    Rails.application.routes.url_helpers.edit_customer_address_url(customer, self)
  end

end
