class Session < ApplicationRecord
  NON_INTERACTIVE_ROLES = ['outreach', 'notification'].freeze
  SESSION_DURATION = 2.hours

  include SerializableResource

  belongs_to :customer, required: true
  has_many :messages, -> { order(sent_at: :asc) }, dependent: :destroy, inverse_of: :session
  has_many :events, as: :eventable, dependent: :destroy

  scope :any_tags, -> (tags){ where('tags && ARRAY[?]', tags) }
  scope :all_tags, -> (tags){ where('tags @> ARRAY[?]', tags) }
  scope :interactive, -> () { where(interactive: true) }

  before_save :set_interactive_flag

  after_commit on: :create do
    Event.create(name: 'session.created', eventable: self, data: self.serialize)
    Event.create(name: 'customers.attention.updated', eventable: self, data: { count_customer: Customer.needs_attention.count })
  end

  after_commit on: :update do
    Event.create(name: 'session.updated', eventable: self, data: self.serialize)
  end

  # Finds or creates a session for a given customer
  #
  # @param customer [Customer]
  # @return [Session]
  def self.latest!(customer)
    ActiveRecord::Base.transaction(requires_new: true) do
      self.current(customer) || Session.create!(customer: customer)
    end
  end

  def self.create_message!(customer, message_params)
    ActiveRecord::Base.transaction do
      if session = current(customer)
        message = session.messages.create!(message_params)
        session.sender_roles = (session.sender_roles + [message.sender_role]).uniq
        session.save! if session.changed?

        message
      else
        session = create!(customer: customer, sender_roles: [message_params[:sender_role]])
        session.messages.create!(message_params)
      end
    end
  end

  # Finds current active session for customer, if any
  #
  # @param customer [Customer]
  # @return [Session, Nil]
  def self.current(customer)
    last_session = customer.sessions.order(:created_at).last
    return nil if last_session.nil?

    is_session_active = last_session.messages
      .where('sent_at > ?', SESSION_DURATION.ago)
      .exists?

    if is_session_active
      last_session
    else
      nil
    end
  end

  # Finds current active session for customer or raise ActiveRecord::RecordNotFound
  #
  # @param customer [Customer]
  # @return [Session]
  def self.current!(customer)
    current(customer) or raise ActiveRecord::RecordNotFound.new("No current session for customer #{customer.id}", Session)
  end

  def set_interactive_flag
    self.interactive = (sender_roles - NON_INTERACTIVE_ROLES).any?
  end
end
