class LineItem < ApplicationRecord
  include SerializableResource
  include Cleanable

  clean :description

  belongs_to :order, required: true
  belongs_to :sku, required: true

  validates :description, :quantity, :amount, presence: true
  validates :quantity, numericality: { greater_than: 0, only_integer: true }
  validates :amount, numericality: { greater_than: 0 }

  def amount
    super || self.amount = compute_amount
  end

  def description
    super || self.description = compute_description
  end


  private

  # Computes the default amount
  # @return [BigDecimal] Total for that LineItem, or nil if can't compute
  def compute_amount
    if quantity? && sku.present?
      item_price = order.vip? ? sku.vip_price : sku.price
      quantity * item_price
    else
      nil
    end
  end

  # Computes the default description
  # @return [String] Something like 2 six-packs of 'product name'
  def compute_description(products_per_packs: 'six')
    if sku.present?
      "#{quantity} #{products_per_packs}-#{'pack'.pluralize(quantity)} of #{sku.product.name}"
    else
      nil
    end
  end

  def describe!(products_per_packs: 'six')
    if sku.present?
      self.description ||= "#{quantity} #{products_per_packs}-#{'pack'.pluralize(quantity)} of #{sku.product.name}"
    end
  end
end
