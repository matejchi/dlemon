# Outreach model – used to manage mass-SMSing client.
#
# Usage:
# 1. Register a CAMPAIGN in the CAMPAIGN constant
#
# 2. Once deployed, add customers matching zip patterns to this campaign.
#   Note that zips are pattern, spawning a campaign to "1234" will target
#   all customers for which the zip starts with "1234". Eg:
#
#     Outreach.spawn_new_campaign(campaign: "MATCHA_OUTREACH", zip: ["1234"])
#
# 3. Schedule outgoing campaign segment to roll out at a given time. See
#    Outreach.generate_schedule for param details. Eg:
#
#      Outreach.schedule(campaign: "MATCHA_OUTREACH", count: 10_000, rate: 200, starting_at: Time.current)
class Outreach < ApplicationRecord
  belongs_to :customer

  CAMPAIGN = HashWithIndifferentAccess.new(
    "MATCHA_OUTREACH" => {
      content: "DIRTY LEMON +matcha is available now.  Our metabolism-boosting elixir w/ pure lemon, matcha, vanilla & cardamom -- Formulated for sustained all-day energy + fat burning support. $45/case (VIP) OR buy two cases & we'll include a case of +matcha for free.  Want to try?",
      media_url: "https://s3.amazonaws.com/api.dirtylemon.production/MATCHA-2a.jpg",
    }
  )

  scope :pending, -> { where(sent_at: nil) }

  # @param campaign_slug [String] Campaign slug (as defined in the `CAMPAIGN` hash)
  # return [Hash] Symbol keys
  #   content [String]: Text content to send
  #   media_url [String | nil]: Media url to send
  def self.campaign_details(campaign_slug)
    CAMPAIGN[campaign_slug]
  end

  # @param campaign [String] Slug of the campaign to spawn
  # @param zip [String | Array<String>] Zip code patterns (matched with LIKE '#{zip}%')
  # @param unconfirmed [Boolean] Wether or not to include unconfirmed customers
  # @yield [ActiveRecord::Relation] Yields the Customers query for chaining
  def self.spawn_new_campaign(campaign: , zip: [], unconfirmed: true)
    zip = Array.wrap(zip)
    zip_where_string = zip.map { "addresses.zip LIKE ?" }.join(" OR ")
    zip_where_params = zip.map { |zip| "#{zip}%" }

    sets = {
      confirmed:   { phone_field: :phone_number,             confirmed_at: true },
      unconfirmed: { phone_field: :unconfirmed_phone_number, confirmed_at: false }
    }
    sets.delete(:unconfirmed) unless unconfirmed

    initial_count = self.count

    sets.each do |set, config|
      query = Customer
        .select('customers.id', ActiveRecord::Base.connection.quote(campaign), config[:phone_field])
        .where.not(id: Order.select(:customer_id).where('created_at > ? ', 1.month.ago))
        .where(unsubscribed_at: nil)

      if zip.any?
        query = query
          .joins(:addresses)
          .where("addresses.country = 'US'")
          .where("addresses.type = 'ShippingAddress'")
          .where(zip_where_string, *zip_where_params)
      end

      query = query.where.not(confirmed_at: nil) if config[:confirmed_at]
      query = yield(query)                       if block_given?

      insert          = Arel::Nodes::InsertStatement.new
      insert.relation = arel_table
      insert.columns  = [arel_table[:customer_id], arel_table[:campaign_name], arel_table[:phone_number]]
      insert.select   = query.arel

      connection.execute("#{insert.to_sql} ON CONFLICT DO NOTHING")
    end

    self.count - initial_count
  end

  # @param campaign [String] Campaign name to send
  # @param rate [Integer] Number of messages to send per hour
  # @param count [Integer] Total number of messages to send
  # @param starting_at [Time] Time at which to start queueing. Defaults to next hour.
  def self.generate_schedule(campaign:, count:, rate: 200, starting_at: nil)
    starting_at = [starting_at, Time.current]
      .compact
      .max
      .utc

    rate_limiter = RateLimiter.new(rate: rate, starting_at: starting_at)

    query = pending
      .joins(:customer)
      .where(campaign_name: campaign)
      .eager_load(:customer)
      .limit(count)

    query.find_each.map do |outreach|
      customer = outreach.customer
      customer_time = Time.current.in_time_zone(customer.time_zone)
      customer_day_start = customer_time.change(hour: DirtyLemon::Settings.customer_day_start).utc.hour
      customer_day_end = customer_time.change(hour: DirtyLemon::Settings.customer_day_end).utc.hour

      send_at = rate_limiter.schedule(customer_day_start, customer_day_end)
      [send_at, outreach]
    end
  end

  # @see #generate_schedule for arguments
  def self.schedule(*args)
    generate_schedule(*args).each do |send_at, outreach|
      SendOutreachTextJob.perform_at(send_at, outreach.id)
    end
  end


  private

  class RateLimiter
    def initialize(rate:, starting_at:)
      @rate = rate
      @starting_at = starting_at
      @slots = Hash.new do |hash, time|
        hash[time] = rate
      end

      load_starting_at_slot!
    end

    # @param earliest [Integer] Earliest *UTC* hour at which we can contact this user
    # @param latest [Integer] Latest *UTC* hour at which we can contact this user
    def schedule(earliest, latest)
      time_pointer = if hour_in_range?(@starting_at.hour, earliest, latest)
                       @starting_at
                     else
                       @starting_at.change(hour: earliest)
                     end

      while true do
        if @slots[time_pointer] > 0 && hour_in_range?(time_pointer.hour, earliest, latest)
          seconds_offset = ((@rate - @slots[time_pointer]) / @rate.to_f) * 3600
          @slots[time_pointer] -= 1

          return time_pointer + seconds_offset.to_i
        end

        time_pointer += 1.hour
      end
    end


    private

    def hour_in_range?(hour, start_hour, end_hour)
      if start_hour >= end_hour
        hour >= start_hour || hour < end_hour
      else
        hour >= start_hour && hour < end_hour
      end
    end


    def load_starting_at_slot!
      slot = @starting_at.at_beginning_of_hour
      return if @slots.has_key?(slot)

      spent_seconds = @starting_at.min * 60 + @starting_at.sec
      start_hour_remaining_pct = (3600 - spent_seconds) / 3600.0

      @slots[slot] = (@rate * start_hour_remaining_pct).floor
    end
  end
end
