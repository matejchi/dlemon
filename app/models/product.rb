class Product < ApplicationRecord
  include SerializableResource

  has_many :skus, class_name: 'Sku'
  has_many :line_items, through: :skus

  validates :name, presence: true
end
