class Sku < ApplicationRecord
  VIP_CASE_DISCOUNT= 20.00
  include SerializableResource

  belongs_to :product, required: true
  has_many :shipments
  has_many :line_items

  validates :stripe_id, :currency, :price, presence: true
  validates :stripe_id, uniqueness: { case_sensitive: false }

  def vip_price
    price - VIP_CASE_DISCOUNT
  end
end
