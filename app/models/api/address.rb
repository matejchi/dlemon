module Api
  class Address
    include Virtus.model(nullify_blank: true)
    include ActiveModel::Validations

    attribute :street1, String
    attribute :street2, String
    attribute :city, String
    attribute :state, String
    attribute :country, String
    attribute :zip, String

    validates :street1, :city, :state, :country, :zip, presence: true
    validates :street1, :street2, format: { without: /po box/i, message: "PO Box are not supported" } 
  end
end
