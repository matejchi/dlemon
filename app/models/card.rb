class Card < ApplicationRecord

  include SerializableResource

  belongs_to :customer, required: true
  has_many :events, as: :eventable, dependent: :destroy

  validates :stripe_id, presence: true
  validates :exp_month, :exp_year, :last4, presence: true
  validates :exp_month, length: { within: 1..2 }
  validates :exp_year, :last4, length: { is: 4 }

  def persist_from_stripe(token)
    # associate stripe customer with card via token
    stripe_customer = customer.stripe_resource
    stripe_customer.source = token
    stripe_customer.save

    # now update that data on this record
    stripe_card = stripe_customer.sources.find do |source|
      source.id == stripe_customer.default_source
    end

    self.update!({
      stripe_id: stripe_card.id,
      exp_month: stripe_card.exp_month,
      exp_year: stripe_card.exp_year,
      last4: stripe_card.last4
    })
  rescue Stripe::CardError => e
    self.errors.add(:base, :invalid, message: e.message)
    raise ActiveRecord::RecordInvalid.new(self)
  end

  # @return [String]
  def short_url
    edit_url
  end


  private

  # @return [String]
  def edit_url
    Rails.application.routes.url_helpers.edit_customer_card_url(customer)
  end
end
