class Shipment < ApplicationRecord
  include SerializableResource

  belongs_to :order, required: true
  belongs_to :sku, required: true
  belongs_to :fulfillment_address, required: false
  belongs_to :batch, required: false
  has_one :customer, through: :order
  has_many :events, as: :eventable, dependent: :destroy

  scope :processed, -> { where(refunded_at: nil).where.not(purchased_at: nil) }

  after_update do
    # Send tracking status email only once per order
    if saved_changes.key?(:tracking_status) && tracking_status == 'pre_transit'
      DirtyLemon::OnceOnly.with(order.id, tracking_status) do
        OrderMailer.shipping_confirmation(order.id, id).deliver_later
      end
    end
  end

  # @param ext [String] Extension to get the key for (png | zpl)
  # @return [String]
  def label_s3_key(ext)
    send("label_#{ext}_s3_key")
  end

  # @param ext [String] Extension to get the key for (png | zpl)
  # @return [String]
  def label_s3_key(ext)
    send("label_#{ext}_s3_key")
  end

  # @param ep_shipment [EasyPost::Shipment]
  # @return [Hash]
  def self.from_easypost(ep_shipment)
    {
      easypost_shipment_id: ep_shipment.id,
      easypost_tracker_id:  ep_shipment.tracker.id,
      carrier:              ep_shipment.selected_rate.carrier,
      service:              ep_shipment.selected_rate.service,
      rate:                 ep_shipment.selected_rate.rate,
      tracking_number:      ep_shipment.tracker.tracking_code,
      tracking_url:         ep_shipment.tracker.public_url,
      tracking_status:      ep_shipment.tracker.status,
      tracking_status_at:   DateTime.parse(ep_shipment.tracker.updated_at),
      eta:                  ep_shipment.tracker.est_delivery_date.present? ? DateTime.parse(ep_shipment.tracker.est_delivery_date) : nil
    }
  end
end
