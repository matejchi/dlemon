class Customer < ApplicationRecord
  VIP_FAILED_CARD_TAG = 'vip failed card'.freeze

  include SerializableResource
  include Confirmable
  include Cleanable

  clean :first_name, :last_name

  has_many :orders, dependent: :destroy do
    def created_recur
      ordered_recur.where(status: "created")
    end

    def ordered_recur
      recur.order(created_at: :desc)
    end
  end

  has_many :addresses, dependent: :destroy
  has_one :card, dependent: :destroy
  has_one :shipping_address, class_name: 'ShippingAddress'
  has_one :billing_address, class_name: 'BillingAddress'
  has_many :sessions, dependent: :destroy
  has_many :messages, through: :sessions
  has_many :events, as: :eventable, dependent: :destroy
  has_many :shipments, through: :orders

  validates :phone_number, presence: true, if: :confirmed?
  validates :phone_number, phony_plausible: true, if: :confirmed?

  validates :country_code, inclusion: { in: ISO3166::Country.codes }, allow_nil: true

  # Validates and sets a formatted unconfirmed_phone_number and country_code
  validate on: :create do |customer|
    TwilioValidator.new(customer).validate if customer.unconfirmed_phone_number?
  end

  phony_normalize :phone_number
  phony_normalize :unconfirmed_phone_number

  after_save :check_customer_attention
  before_create :set_reference

  after_commit :update_stripe_customer, on: [:create, :update]

  after_commit on: :create do
    Event.create(name: 'customer.created', eventable: self, data: self.serialize)
  end

  scope :needs_attention, -> { where(needs_attention: true) }
  scope :vip, -> { where("'vip' = ANY (tags)") }
  scope :recur_order_due, -> { where('renew_recur_order_on <= ?', Time.current) }

  def tags=(value)
    if value.is_a?(Enumerable)
      value = value.map { |tag| tag.to_s.strip.downcase }.reject(&:blank?).uniq
    end

    super(value)
  end

  def avg_shipping_cost
    shipments.processed.average(:rate) || 0.0
  end

  # Return the total of all order with state 'paid'
  # @return [Decimal]
  def total_lifetime
    self.orders.processed.sum(:total)
  end

  def stripe_resource
    Stripe::Customer.retrieve(stripe_id) unless stripe_id.nil?
  end

  # @return [String]
  def full_name
    [first_name, last_name].join(' ').strip
  end

  # @return [Boolean]
  def unsubscribed?
    unsubscribed_at?
  end


  # @return [Boolean]
  def vip?
    tags.include?('vip')
  end

  def clear_vip
    self.renew_recur_order_on = nil
    self.tags -= ['vip']
  end


  # Creates initial blank records in a transaction
  def bootstrap
    ActiveRecord::Base.transaction do
      # Create blank records
      Card.new(customer: self).tap { |a| a.save(validate: false) }
      BillingAddress.new(customer: self).tap { |a| a.save(validate: false) }
      ShippingAddress.new(customer: self).tap { |a| a.save(validate: false) }
    end
  end

  def touch_renew_recur_order_on
    self.renew_recur_order_on = Time.current + DirtyLemon::Recurring::INITAL_RENEWAL_DELAY
  end

  private

  def check_customer_attention
    Event.create(name: 'customers.attention.updated', eventable: self, data: { count_customer: Customer.needs_attention.count }) if self.saved_change_to_needs_attention?
  end

  # enqueues a job to create a record in stripe for this customer
  def update_stripe_customer
    if stripe_id.nil?
      CreateStripeCustomerJob.perform_async(id)
    else
      UpdateStripeCustomerJob.perform_async(id)
    end
  end

  # Sets a short, unique reference ID
  def set_reference
    self.reference = loop do
      ref = SecureRandom.hex(3)
      break ref unless Customer.exists?(reference: ref)
    end
  end
end
