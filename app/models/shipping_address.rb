class ShippingAddress < Address
  validates :type, presence: true
end
