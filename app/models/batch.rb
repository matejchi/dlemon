class Batch < ApplicationRecord
  has_many :shipments
  has_many :orders
  belongs_to :fulfillment_address

  validates :name, :enqueued_at, presence: true

  before_validation :set_computed_name, if: -> { name.blank? }


  private

  def set_computed_name
    self.name = enqueued_at.strftime('%F %R %Z') if enqueued_at.present?
  end
end
