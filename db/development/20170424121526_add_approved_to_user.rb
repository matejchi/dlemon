class AddApprovedToUser < ActiveRecord::Migration[5.0]
  # ADMIN migration
  def change
    add_column :users, :approved, :boolean, default: false
  end
end
