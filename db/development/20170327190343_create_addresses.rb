class CreateAddresses < ActiveRecord::Migration[5.0]
  def change
    create_table :addresses do |t|
      t.integer :customer_id
      t.string :street1
      t.string :street2
      t.string :city
      t.string :state
      t.string :country
      t.string :zip
      t.string :type

      t.timestamps null: false
    end

    add_index :addresses, :customer_id

    # Migrate customer data from orders to customers table
    Customer.find_each do |customer|
      order = customer.orders.order(:created_at).last

      if order
        ShippingAddress.create({
          customer: customer,
          street1: order.shipping_address_street1,
          street2: order.shipping_address_street2,
          city: order.shipping_address_city,
          state: order.shipping_address_state,
          country: order.shipping_address_country || 'US',
          zip: order.shipping_address_zip
        })

        BillingAddress.create({
          customer: customer,
          street1: order.billing_address_street1,
          street2: order.billing_address_street2,
          city: order.billing_address_city,
          state: order.billing_address_state,
          country: order.billing_address_country || 'US',
          zip: order.billing_address_zip
        })

        unless customer.email?
          customer.update_columns(email: order.try(:email))
        end
      end
    end
  end
end
