class CreateSuggestedMessages < ActiveRecord::Migration[5.0]
  def change
    create_table :suggested_messages, id: :uuid, default: -> { "uuid_generate_v4()" } do |t|
      t.uuid :session_id, null: false
      t.string :remote_id
      t.text :content
      t.string :content_type
    end

    add_index :suggested_messages, :session_id
  end
end
