class CreateSessions < ActiveRecord::Migration[5.0]
  def change
    create_table :sessions, id: :uuid do |t|
      t.string :tags, array: true, default: []
      t.string :topics, array: true, default: []
      t.boolean :need_attention, default: false

      t.timestamps
    end

    add_index :sessions, :tags
    add_index :sessions, :topics
  end

  add_column :messages, :session_id, :uuid, null: false
end
