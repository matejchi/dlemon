class RenameCustomersConfirmationSentAt < ActiveRecord::Migration[5.0]
  def change
    rename_column :customers, :confirmation_sent_at, :confirmation_generated_at
  end
end
