class CreateEvents < ActiveRecord::Migration[5.0]
  def change
    create_table :events, id: :uuid, default: -> { "uuid_generate_v4()" } do |t|
      t.string :name, null: false
      t.uuid :eventable_id, null: false
      t.string :eventable_type, null: false
      t.jsonb :data, default: {}
    end

    add_index :events, [:eventable_id, :eventable_type]
  end
end
