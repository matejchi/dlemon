class RemoveCardFromCustomers < ActiveRecord::Migration[5.0]
  def change
    remove_column :customers, :card
  end
end
