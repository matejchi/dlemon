class RemoveOrdersShippingFeeDefault < ActiveRecord::Migration[5.0]
  def change
    change_column_default :orders, :shipping_fee, nil
  end
end
