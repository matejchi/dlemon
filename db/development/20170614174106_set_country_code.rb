class SetCountryCode < ActiveRecord::Migration[5.0]
  def change
    ShippingAddress.find_each do |address|
      address.customer.update_column(:country_code, address.country) 
    end
  end
end
