class CreateCards < ActiveRecord::Migration[5.0]
  def change
    create_table :cards, id: :uuid, default: -> { "uuid_generate_v4()" } do |t|
      t.uuid :customer_id, null: false
      t.string :stripe_id
      t.integer :exp_month
      t.integer :exp_year
      t.integer :last4
      t.string :url

      t.timestamps
    end

    add_index :cards, :customer_id
  end
end
