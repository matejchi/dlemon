class CreateShortUrls < ActiveRecord::Migration[5.0]
  def change
    create_table :short_urls, id: false do |t|
      t.string :token, null: false
      t.string :path, null: false
      t.datetime :expires_at, null: false
    end

    add_index :short_urls, :token, unique: true
  end
end
