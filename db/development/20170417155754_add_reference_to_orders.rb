class AddReferenceToOrders < ActiveRecord::Migration[5.0]
  def change
    add_column :orders, :reference, :string

    references = Set.new

    Order.find_each do |order|
      reference = loop do
        reference = SecureRandom.hex(3)
        break reference if references.add?(reference)
      end

      order.update_column(:reference, reference)
    end

    change_column_null :orders, :reference, false

    add_index :orders, :reference, unique: true
  end
end
