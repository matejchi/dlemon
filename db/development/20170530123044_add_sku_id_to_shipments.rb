class AddSkuIdToShipments < ActiveRecord::Migration[5.0]
  def change
    add_column :shipments, :sku_id, :uuid
  end
end
