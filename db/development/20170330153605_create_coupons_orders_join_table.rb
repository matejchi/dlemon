class CreateCouponsOrdersJoinTable < ActiveRecord::Migration[5.0]
  def change
    create_table :coupons_orders, id: false do |t|
      t.integer :coupon_id
      t.integer :order_id
    end
  end
end
