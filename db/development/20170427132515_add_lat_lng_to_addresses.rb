class AddLatLngToAddresses < ActiveRecord::Migration[5.0]
  def change
    add_column :addresses, :lat, :decimal
    add_column :addresses, :lng, :decimal
  end
end
