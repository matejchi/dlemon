class RemoveStatusRelatedFields < ActiveRecord::Migration[5.0]
  def change
    remove_column :orders, :confirmed_at
    remove_column :orders, :canceled_at

    remove_column :shipments, :shipped_at
    remove_column :shipments, :canceled_at
  end
end
