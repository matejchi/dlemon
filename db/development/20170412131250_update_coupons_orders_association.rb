class UpdateCouponsOrdersAssociation < ActiveRecord::Migration[5.0]
  def change
    drop_table :coupons_orders

    add_column :orders, :coupon_id, :string

    add_index :orders, :coupon_id
  end
end
