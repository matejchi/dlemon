class RenameProductLabelToDescription < ActiveRecord::Migration[5.0]
  def change
    rename_column :products, :label, :description
  end
end
