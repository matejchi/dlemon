class AddEasypostFromAddressIdToShipments < ActiveRecord::Migration[5.0]
  def change
    add_column :shipments, :easypost_from_address_id, :string
  end
end
