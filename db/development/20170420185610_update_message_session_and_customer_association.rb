class UpdateMessageSessionAndCustomerAssociation < ActiveRecord::Migration[5.0]
  def change
    remove_column :messages, :customer_id, :uuid
    add_column :sessions, :customer_id, :uuid
  end
end
