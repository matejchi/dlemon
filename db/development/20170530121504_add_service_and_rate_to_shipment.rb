class AddServiceAndRateToShipment < ActiveRecord::Migration[5.0]
  def change
    add_column :shipments, :service, :string
    add_column :shipments, :rate, :decimal
  end
end
