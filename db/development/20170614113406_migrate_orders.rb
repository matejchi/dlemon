class MigrateOrders < ActiveRecord::Migration[5.0]
  def change
    add_column :orders, :shipping, :jsonb, default: {}

    Order.find_each do |order|
      order.shipping = Shipping.new(
        name: order.customer.full_name,
        phone_number: order.customer.phone_number,
        street1: order.shipping_address_street1,
        street2: order.shipping_address_street2,
        city: order.shipping_address_city,
        state: order.shipping_address_state,
        country: order.shipping_address_country,
        zip: order.shipping_address_zip
      )
      order.currency = 'USD'
      order.save!

      if transition = OrderTransition.where(order_id: order.id).order(:created_at).last
        order.update_column(:status, transition.to_state)
      else
        order.update_column(:status, :created)
      end
    end

    remove_column :orders, :email
    remove_column :orders, :shipping_address_street1
    remove_column :orders, :shipping_address_street2
    remove_column :orders, :shipping_address_city
    remove_column :orders, :shipping_address_state
    remove_column :orders, :shipping_address_country
    remove_column :orders, :shipping_address_zip
    remove_column :orders, :billing_address_street1
    remove_column :orders, :billing_address_street2
    remove_column :orders, :billing_address_city
    remove_column :orders, :billing_address_state
    remove_column :orders, :billing_address_country
    remove_column :orders, :billing_address_zip
  end
end
