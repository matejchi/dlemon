class AddCurrencyToOrders < ActiveRecord::Migration[5.0]
  def change
    add_column :orders, :currency, :string
  end
end
