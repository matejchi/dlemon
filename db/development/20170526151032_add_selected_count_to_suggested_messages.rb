class AddSelectedCountToSuggestedMessages < ActiveRecord::Migration[5.0]
  def change
    add_column :suggested_messages, :selected_count, :integer, default: 0
  end
end
