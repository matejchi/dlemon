class RenameOrdersStateToStatus < ActiveRecord::Migration[5.0]
  def change
    rename_column :orders, :state, :status
  end
end
