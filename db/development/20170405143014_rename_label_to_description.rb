class RenameLabelToDescription < ActiveRecord::Migration[5.0]
  def change
    rename_column :line_items, :unit_price, :amount
    rename_column :line_items, :label, :description
  end
end
