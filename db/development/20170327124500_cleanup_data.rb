class CleanupData < ActiveRecord::Migration[5.0]
  def change
    Order.where(customer_id: nil).delete_all # 25
    Shipment.where(order_id: nil).delete_all # 148
    LineItem.where(order_id: nil).delete_all # 0
    OrderTransition.where.not(order_id: Order.ids).delete_all # 181
  end
end
