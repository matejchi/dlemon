class UpdateCoupons < ActiveRecord::Migration[5.0]
  def change
    remove_column :coupons, :id, :uuid
    add_column :coupons, :id, :string, null: false

    add_column :coupons, :stripe_id, :string

    remove_column :coupons, :name
    remove_column :coupons, :discount
    remove_column :coupons, :active_from
    remove_column :coupons, :active_until

    execute "ALTER TABLE coupons ADD PRIMARY KEY (id);"
  end
end
