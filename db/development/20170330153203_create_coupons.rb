class CreateCoupons < ActiveRecord::Migration[5.0]
  def change
    create_table :coupons do |t|
      t.string :name
      t.integer :discount
      t.datetime :active_from
      t.datetime :active_until

      t.timestamps null: false
    end

    drop_table :codes
  end
end
