class AddRefundAndChargeToOrders < ActiveRecord::Migration[5.0]
  def change
    add_column :orders, :charge, :jsonb, default: {}, null: false
    add_column :orders, :refund, :jsonb, default: {}, null: false
  end
end
