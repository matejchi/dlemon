class RemoveShortUrlsExpiresAt < ActiveRecord::Migration[5.0]
  def change
    remove_column :short_urls, :expires_at
  end
end
