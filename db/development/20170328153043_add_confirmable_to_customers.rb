class AddConfirmableToCustomers < ActiveRecord::Migration[5.0]
  def change
    add_column :customers, :unconfirmed_phone_number, :string
    add_column :customers, :confirmed_at, :datetime
    add_column :customers, :confirmation_sent_at, :datetime
    add_column :customers, :confirmation_token, :string

    remove_column :customers, :verify_code
    remove_column :customers, :verify_token
    remove_column :customers, :verify_by
  end
end
