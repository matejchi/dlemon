class AddCanceledAtToOrders < ActiveRecord::Migration[5.0]
  def change
    add_column :orders, :canceled_at, :datetime
  end
end
