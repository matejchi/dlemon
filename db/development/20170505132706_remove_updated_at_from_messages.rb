class RemoveUpdatedAtFromMessages < ActiveRecord::Migration[5.0]
  def change
    remove_column :messages, :updated_at, :datetime
  end
end
