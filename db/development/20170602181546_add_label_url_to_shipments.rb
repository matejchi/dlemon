class AddLabelUrlToShipments < ActiveRecord::Migration[5.0]
  def change
    add_column :shipments, :label_url, :string
  end
end
