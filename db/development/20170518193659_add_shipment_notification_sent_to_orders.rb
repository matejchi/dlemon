class AddShipmentNotificationSentToOrders < ActiveRecord::Migration[5.0]
  def change
    add_column :orders, :shipments_notification_sent, :boolean, default: false
  end
end
