class AddCardToCustomers < ActiveRecord::Migration[5.0]
  def change
    add_column :customers, :card, :jsonb, default: {}, null: false
  end
end
