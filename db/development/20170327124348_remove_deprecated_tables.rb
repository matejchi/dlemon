class RemoveDeprecatedTables < ActiveRecord::Migration[5.0]
  def change
    drop_table :words
    drop_table :shipping_csvs
    drop_table :shipment_transitions
    drop_table :requests
    drop_table :outbound_messages
    drop_table :messages
    drop_table :message_templates
    drop_table :distros
    drop_table :dirty_lemon_phones
    drop_table :delayed_jobs
    drop_table :bulk_sent_messages
    drop_table :versions
  end
end
