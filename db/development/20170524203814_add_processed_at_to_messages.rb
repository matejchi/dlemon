class AddProcessedAtToMessages < ActiveRecord::Migration[5.0]
  def change
    add_column :messages, :processed_at, :datetime
  end
end
