class UpdateProducts < ActiveRecord::Migration[5.0]
  def change
    add_column :products, :stripe_id, :string
    remove_column :products, :preorder_only
  end
end
