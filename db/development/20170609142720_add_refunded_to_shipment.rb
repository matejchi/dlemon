class AddRefundedToShipment < ActiveRecord::Migration[5.0]
  def change
    add_column :shipments, :refunded, :boolean, default: false
  end
end
