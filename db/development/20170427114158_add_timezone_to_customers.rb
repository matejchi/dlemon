class AddTimezoneToCustomers < ActiveRecord::Migration[5.0]
  def change
    add_column :customers, :time_zone, :string, default: "UTC", null: false
  end
end
