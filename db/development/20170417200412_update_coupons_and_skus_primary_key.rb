class UpdateCouponsAndSkusPrimaryKey < ActiveRecord::Migration[5.0]
  def change
    Coupon.all.each do |coupon|
      coupon.update!(stripe_id: coupon.id)
    end

    remove_column :coupons, :id
    remove_column :skus, :id

    add_column :coupons, :id, :uuid, null: false, default: -> { "uuid_generate_v4()" }, primary_key: true
    add_column :skus, :id, :uuid, null: false, default: -> { "uuid_generate_v4()" }, primary_key: true

    add_index :coupons, :stripe_id, unique: true
    add_index :skus, :stripe_id, unique: true
  end
end
