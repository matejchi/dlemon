class RemoveSkuIsPromoAndUnitPriceFromProducts < ActiveRecord::Migration[5.0]
  def change
    remove_column :products, :sku
    remove_column :products, :is_promo
    remove_column :products, :unit_price
  end
end
