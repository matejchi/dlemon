class UpdateShipmentAttributes < ActiveRecord::Migration[5.0]
  def change
    add_column :shipments, :easypost_shipment_id, :string
    add_column :shipments, :easypost_tracker_id, :string
    add_column :shipments, :carrier, :string
    add_column :shipments, :tracking_url, :string
    add_column :shipments, :tracking_status, :string
    add_column :shipments, :tracking_status_details, :text
    add_column :shipments, :tracking_status_at, :datetime
    add_column :shipments, :eta, :datetime

    add_column :shipments, :fulfilled_at, :datetime
  end
end
