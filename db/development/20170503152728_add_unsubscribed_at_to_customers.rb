class AddUnsubscribedAtToCustomers < ActiveRecord::Migration[5.0]
  def change
    add_column :customers, :unsubscribed_at, :datetime
  end
end
