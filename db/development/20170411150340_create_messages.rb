class CreateMessages < ActiveRecord::Migration[5.0]
  def change
    create_table :messages do |t|
      t.text :content
      t.string :content_type
      t.string :sender_role
      t.uuid :customer_id, null: false

      t.timestamps null: false
    end
  end
end
