class RemoveTrackingStatusDetalsAndFulfilledAtFromShipments < ActiveRecord::Migration[5.0]
  def change
    remove_column :shipments, :fulfilled_at
    remove_column :shipments, :tracking_status_details
  end
end
