class CustomerTransition < ActiveRecord::Base
  belongs_to :customer, inverse_of: :transitions
end

class ConfirmCustomers < ActiveRecord::Migration[5.0]
  def change
    # Update all confirmed customers
    confirmed_customers = Customer.where(id: CustomerTransition.where(to_state: 'ready_to_order').pluck(:customer_id))

    confirmed_customers.find_each do |customer|
      customer.update_column(:confirmed_at, CustomerTransition.where(to_state: 'ready_to_order', customer_id: customer.id).first.created_at)
    end

    drop_table :customer_transitions
  end
end
