class CleanupTables < ActiveRecord::Migration[5.0]
  def change
    disable_extension "pg_stat_statements"

    remove_column :users, :password
    remove_column :users, :remember_token
    remove_column :users, :confirmation_token
    remove_column :users, :phone_number
    remove_column :users, :is_admin

    change_column_default :products, :unit_price, 0

    remove_column :orders, :distro_id
    remove_column :orders, :prefix
    remove_column :orders, :first_name
    remove_column :orders, :last_name
    remove_column :orders, :sample
    remove_column :orders, :is_promo

    change_column :customers, :crm_id, :string
    rename_column :customers, :collected_email, :email
    rename_column :customers, :stripe_customer_id, :stripe_id
    remove_column :customers, :needs_attention
    remove_column :customers, :needs_follow_up
    remove_column :customers, :last_message_at
    remove_column :customers, :interested_in_skin
    remove_column :customers, :code_id
    remove_column :customers, :notified_of_being_unsubscribed_at
    remove_column :customers, :dirty_lemon_phone_id

    change_column_null :orders, :customer_id, false
    change_column_null :line_items, :product_id, false
    change_column_null :line_items, :order_id, false
    change_column_null :shipments, :order_id, false
  end
end
