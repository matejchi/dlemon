class UseUuids < ActiveRecord::Migration[5.0]
  def change
    enable_extension 'uuid-ossp'

    # Create UUIDs
    ############################################################################
    add_column :addresses, :uuid, :uuid, default: "uuid_generate_v4()", null: false unless Address.column_for_attribute(:uuid).type
    add_column :coupons, :uuid, :uuid, default: "uuid_generate_v4()", null: false unless Coupon.column_for_attribute(:uuid).type
    add_column :customers, :uuid, :uuid, default: "uuid_generate_v4()", null: false unless Customer.column_for_attribute(:uuid).type
    add_column :line_items, :uuid, :uuid, default: "uuid_generate_v4()", null: false unless LineItem.column_for_attribute(:uuid).type
    add_column :orders, :uuid, :uuid, default: "uuid_generate_v4()", null: false unless Order.column_for_attribute(:uuid).type
    add_column :products, :uuid, :uuid, default: "uuid_generate_v4()", null: false unless Product.column_for_attribute(:uuid).type
    add_column :shipments, :uuid, :uuid, default: "uuid_generate_v4()", null: false unless Shipment.column_for_attribute(:uuid).type
    add_column :users, :uuid, :uuid, default: "uuid_generate_v4()", null: false unless User.column_for_attribute(:uuid).type
    add_column :order_transitions, :uuid, :uuid, default: "uuid_generate_v4()", null: false unless OrderTransition.column_for_attribute(:uuid).type

    # line_items
    ############################################################################
    unless LineItem.column_for_attribute(:product_id).type == :uuid
      add_column :line_items, :product_uuid, :uuid

      Product.pluck(:id, :uuid).each do |id, uuid|
        LineItem.where(product_id: id).update_all(product_uuid: uuid)
      end

      remove_column :line_items, :product_id
      rename_column :line_items, :product_uuid, :product_id
    end

    unless LineItem.column_for_attribute(:order_id).type == :uuid
      add_column :line_items, :order_uuid, :uuid

      Order.pluck(:id, :uuid).each do |id, uuid|
        LineItem.where(order_id: id).update_all(order_uuid: uuid)
      end

      remove_column :line_items, :order_id
      rename_column :line_items, :order_uuid, :order_id
    end

    # orders
    ############################################################################

    unless Order.column_for_attribute(:customer_id).type == :uuid
      add_column :orders, :customer_uuid, :uuid

      Customer.pluck(:id, :uuid).each do |id, uuid|
        Order.where(customer_id: id).update_all(customer_uuid: uuid)
      end

      remove_column :orders, :customer_id
      rename_column :orders, :customer_uuid, :customer_id
    end

    # shipments
    ############################################################################

    unless Shipment.column_for_attribute(:order_id).type == :uuid
      add_column :shipments, :order_uuid, :uuid

      Order.pluck(:id, :uuid).each do |id, uuid|
        Shipment.where(order_id: id).update_all(order_uuid: uuid)
      end

      remove_column :shipments, :order_id
      rename_column :shipments, :order_uuid, :order_id
    end

    # addresses
    ############################################################################

    unless Address.column_for_attribute(:customer_id).type == :uuid
      add_column :addresses, :customer_uuid, :uuid

      Customer.pluck(:id, :uuid).each do |id, uuid|
        Address.where(customer_id: id).update_all(customer_uuid: uuid)
      end

      remove_column :addresses, :customer_id
      rename_column :addresses, :customer_uuid, :customer_id
    end

    # order_transitions
    ############################################################################

    unless OrderTransition.column_for_attribute(:order_id).type == :uuid
      add_column :order_transitions, :order_uuid, :uuid

      Order.pluck(:id, :uuid).each do |id, uuid|
        OrderTransition.where(order_id: id).update_all(order_uuid: uuid)
      end

      remove_column :order_transitions, :order_id
      rename_column :order_transitions, :order_uuid, :order_id
    end


    # Make primary key
    ############################################################################
    remove_column :addresses, :id
    remove_column :coupons, :id
    remove_column :customers, :id
    remove_column :line_items, :id
    remove_column :orders, :id
    remove_column :products, :id
    remove_column :shipments, :id
    remove_column :users, :id
    remove_column :order_transitions, :id

    rename_column :addresses, :uuid, :id
    rename_column :coupons, :uuid, :id
    rename_column :customers, :uuid, :id
    rename_column :line_items, :uuid, :id
    rename_column :orders, :uuid, :id
    rename_column :products, :uuid, :id
    rename_column :shipments, :uuid, :id
    rename_column :users, :uuid, :id
    rename_column :order_transitions, :uuid, :id

    execute "ALTER TABLE addresses ADD PRIMARY KEY (id);"
    execute "ALTER TABLE coupons ADD PRIMARY KEY (id);"
    execute "ALTER TABLE customers ADD PRIMARY KEY (id);"
    execute "ALTER TABLE line_items ADD PRIMARY KEY (id);"
    execute "ALTER TABLE orders ADD PRIMARY KEY (id);"
    execute "ALTER TABLE products ADD PRIMARY KEY (id);"
    execute "ALTER TABLE shipments ADD PRIMARY KEY (id);"
    execute "ALTER TABLE users ADD PRIMARY KEY (id);"
    execute "ALTER TABLE order_transitions ADD PRIMARY KEY (id);"

    # Build indexes
    ############################################################################
    add_index :addresses, :customer_id
    add_index :shipments, :order_id
    add_index :orders, :customer_id
    add_index :line_items, :product_id
    add_index :line_items, :order_id
  end
end
