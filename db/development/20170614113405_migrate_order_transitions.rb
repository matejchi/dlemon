class MigrateOrderTransitions < ActiveRecord::Migration[5.0]
  def change
    {
      new: :created,
      with_product: :created,
      with_quantity: :created,
      about_to_confirm: :created,
      confirmed: :paid,
      canceled: :canceled,
      sent_for_shipment: :fulfilled
    }.each do |current, status|
      OrderTransition.where(to_state: current).update_all(to_state: status)
    end
  end
end
