class UpdateLineItemProductsAssociation < ActiveRecord::Migration[5.0]
  def change
    add_column :line_items, :sku_id, :uuid

    Product.all.each do |product|
      LineItem.where(product_id: product.id).update_all(sku_id: product.skus.first.id)
    end

    change_column_null :line_items, :sku_id, false
    remove_column :line_items, :product_id

    add_index :line_items, :sku_id
  end
end
