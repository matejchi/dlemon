class PrepareUpdateLineItemProductsAssociation < ActiveRecord::Migration[5.0]
  def change
    Product.find_by!(description: 'sleep').skus.create!(stripe_id: "868137000122", price: 65, currency: 'usd')
    Product.find_by!(description: 'detox').skus.create!(stripe_id: "868137000108", price: 65, currency: 'usd')
    Product.find_by!(description: 'skin').skus.create!(stripe_id: "868137000115", price: 65, currency: 'usd')
    Product.find_by!(description: 'energy').skus.create!(stripe_id: "868137000139", price: 65, currency: 'usd')
  end
end
