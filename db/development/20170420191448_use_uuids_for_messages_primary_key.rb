class UseUuidsForMessagesPrimaryKey < ActiveRecord::Migration[5.0]
  def change
    remove_column :messages, :id
    add_column :messages, :id, :uuid, primary_key: true, default: -> { "uuid_generate_v4()" }
  end
end
