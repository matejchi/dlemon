class GenerateUrls < ActiveRecord::Migration[5.0]
  def change
    Address.find_each do |address|
      address.generate_short_url!
    end

    Card.find_each do |card|
      card.generate_short_url!
    end
  end
end
