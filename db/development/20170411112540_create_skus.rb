class CreateSkus < ActiveRecord::Migration[5.0]
  def change
    create_table :skus, id: :uuid, default: "uuid_generate_v4()", null: false do |t|
      t.decimal :price
      t.string :currency
      t.uuid :product_id, null: false
      t.string :stripe_id

      t.timestamps
    end
  end
end
