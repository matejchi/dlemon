class AddReferenceToCustomers < ActiveRecord::Migration[5.0]
  def change
    add_column :customers, :reference, :string

    references = Set.new

    Customer.find_each do |customer|
      reference = loop do
        reference = SecureRandom.hex(3)
        break reference if references.add?(reference)
      end

      customer.update_column(:reference, reference)
    end

    change_column_null :customers, :reference, false

    add_index :customers, :reference, unique: true
  end
end
