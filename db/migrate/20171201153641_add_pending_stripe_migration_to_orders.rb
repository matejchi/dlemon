class AddPendingStripeMigrationToOrders < ActiveRecord::Migration[5.1]
  def change
    add_column :orders, :pending_stripe_migration, :boolean, default: false, null: false
    add_index :orders, :created_at, order: 'desc', where: 'pending_stripe_migration = true'
    reversible do |dir|
      dir.up do
        Order.where.not(stripe_id: nil).update_all(pending_stripe_migration: true)
      end
    end
  end
end
