class RenameStripeRefToStripeId < ActiveRecord::Migration[5.1]
  def change
    rename_column :transactions, :stripe_ref, :stripe_id
  end
end
