class AddVipToOrder < ActiveRecord::Migration[5.1]
  def change
    add_column :orders, :vip, :boolean, default: false, null: false

    reversible do |dir|
      dir.up do
        vip_coupons = Coupon.where("stripe_id LIKE 'CASEVIP%'").select(:id)
        Order.where(coupon_id: vip_coupons).update_all(vip: true)
      end
    end
  end
end
