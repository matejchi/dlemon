class AddPurchasedAtAndRefundedAtToShipments < ActiveRecord::Migration[5.0]
  def change
    add_column(:shipments, :purchased_at, :datetime) unless column_exists?(:shipments, :purchased_at)
    add_column(:shipments, :refunded_at, :datetime) unless column_exists?(:shipments, :refunded_at)
  end
end
