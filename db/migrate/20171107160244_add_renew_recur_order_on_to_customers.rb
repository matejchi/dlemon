class AddRenewRecurOrderOnToCustomers < ActiveRecord::Migration[5.1]
  def change
    add_column :customers, :renew_recur_order_on, :datetime, null: true
    add_index :customers, :renew_recur_order_on

    reversible do |dir|
      dir.up do
        execute <<~SQL
          WITH cte AS
          (
            SELECT DISTINCT ON (customer_id) customer_id, id, created_at
            FROM orders
            WHERE recur = 't' AND status IN ('fulfilled', 'queued', 'paid')
            ORDER BY customer_id, created_at DESC
          )

          UPDATE customers
          SET renew_recur_order_on = cte.created_at + INTERVAL '1 day'
          FROM cte
          WHERE customers.id = cte.customer_id
        SQL
      end
    end
  end
end
