class RemoveLabelPngUrlAndLabelZplUrl < ActiveRecord::Migration[5.1]
  def up
    remove_column :shipments, :label_png_url
    remove_column :shipments, :label_zpl_url
  end
end
