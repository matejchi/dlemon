class AddDateAndFulfillmentAddress < ActiveRecord::Migration[5.1]
  def change
    add_reference :batches, :fulfillment_address, type: :uuid, foreign_key: true, null: true
    add_column :batches, :name, :string, null: true


    reversible do |dir|
      # Seed default value and make all columns non-nullable
      dir.up do
        execute %q{
          UPDATE batches SET fulfillment_address_id = (
            SELECT fulfillment_address_id
            FROM shipments
            WHERE
              batches.id = batch_id AND
              fulfillment_address_id IS NOT NULL
            LIMIT 1
          );
        }

        change_column_null :batches, :fulfillment_address_id, false

        Batch.all.each do |batch|
          batch.update_attributes!(name: batch.enqueued_at.at_beginning_of_hour.strftime('%F %R %Z'))
        end
        change_column_null :batches, :name, false
      end
    end

    add_index :batches, [:fulfillment_address_id, :name], unique: true
  end
end
