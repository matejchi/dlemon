class RemoveOrdersPreordered < ActiveRecord::Migration[5.1]
  def change
    remove_column :orders, :preordered, :boolean
  end
end
