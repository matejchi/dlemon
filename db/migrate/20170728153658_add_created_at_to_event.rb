class AddCreatedAtToEvent < ActiveRecord::Migration[5.1]
  def change
    add_column :events, :created_at, :datetime
  end
end
