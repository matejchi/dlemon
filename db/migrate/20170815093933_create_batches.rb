class CreateBatches < ActiveRecord::Migration[5.1]
  def change
    create_table :batches, id: :uuid, default: -> { "uuid_generate_v4()" } do |t|
      t.timestamps null: false
    end
  end
end
