class LetCusomtersTimeZoneBeNull < ActiveRecord::Migration[5.1]
  def up
    change_column_null :customers, :time_zone, true

    # Fix customers for which country_code hasn't been ported
    execute <<~SQL
      UPDATE customers
      SET country_code = addresses.country
      FROM addresses
      WHERE addresses.type = 'ShippingAddress'
        AND addresses.customer_id = customers.id
        AND customers.country_code IS NULL
        AND addresses.country IS NOT NULL
    SQL

    # Nullify timezone for UTC customers that aren't in GB
    Customer
      .where(time_zone: 'UTC')
      .where.not(country_code: 'GB')
      .update_all(time_zone: nil)
  end

  def down
    Customer.where(time_zone: nil).update_all(time_zone: 'UTC')
    change_column_null :customers, :time_zone, false
  end
end
