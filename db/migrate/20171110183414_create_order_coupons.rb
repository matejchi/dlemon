class CreateOrderCoupons < ActiveRecord::Migration[5.1]

  def change
    create_table :order_coupons do |t|
      t.uuid :order_id
      t.uuid :coupon_id
    end
  end

end
