class MoveNeedsAttentionToCustomers < ActiveRecord::Migration[5.1]
  def change
    add_column :customers, :needs_attention, :boolean,  default: false

    reversible do |dir|
      dir.up do
        execute <<~SQL
          UPDATE customers SET needs_attention = sub.need_attention
          FROM (
            SELECT DISTINCT customer_id, need_attention, created_at
            FROM sessions
            ORDER BY created_at DESC
          ) sub
          WHERE sub.customer_id = customers.id
        SQL
      end
    end

    remove_column :sessions, :need_attention, :boolean
  end
end
