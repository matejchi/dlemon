class RenameProducts < ActiveRecord::Migration[5.1]
  def change
    [['[detox]', '+charcoal'], ['[skin+hair]', '+collagen'], ['[energy]', '+ginseng'], ['[sleep]', 'sleep']].each do |v|
      product = Product.find_by_name(v[0])
      if product
        product.update_column(:name, v[1])
      end
    end
  end
end
