class AddShipmentsLabelS3Keys < ActiveRecord::Migration[5.1]
  def change
    add_column :shipments, :label_png_s3_key, :string, null: true
    add_column :shipments, :label_zpl_s3_key, :string, null: true
  end
end
