class AddEnqueuedAtToBatches < ActiveRecord::Migration[5.1]
  def change
    add_column :batches, :enqueued_at, :timestamp, null: false
  end
end
