class AddPendingConfirmAtToOrders < ActiveRecord::Migration[5.1]
  def change
    add_column :orders, :scheduled_confirm_on, :datetime
    add_index :orders, :scheduled_confirm_on, where: 'scheduled_confirm_on IS NOT NULL'
  end
end
