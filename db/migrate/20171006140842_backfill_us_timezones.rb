class BackfillUsTimezones < ActiveRecord::Migration[5.1]
  def up
    utc_customers = Customer
      .joins(:addresses)
      .where.not(confirmed_at: nil)
      .where("addresses.type = ?", "ShippingAddress")
      .where("addresses.country = ?", "US")
      .where(time_zone: 'UTC')


    tz_to_states = DirtyLemon::TimeZoneMapping::MAPPING["US"].inject({}) do |memo, (state, tz)|
      memo.merge(tz => (memo[tz] || []) + [state])
    end

    tz_to_states.each do |tz, states|

    utc_customers
      .where("addresses.state IN (?)", states)
      .update_all(time_zone: tz)
    end
  end

  def down
    # no-op
  end
end
