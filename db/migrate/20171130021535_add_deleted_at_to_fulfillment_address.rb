class AddDeletedAtToFulfillmentAddress < ActiveRecord::Migration[5.1]
  def change
    add_column :fulfillment_addresses, :deleted_at, :datetime
    add_index :fulfillment_addresses, :deleted_at
  end
end
