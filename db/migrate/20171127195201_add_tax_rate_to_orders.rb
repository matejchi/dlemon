class AddTaxRateToOrders < ActiveRecord::Migration[5.1]
  def change
    add_column :orders, :tax_rate, :numeric
  end
end
