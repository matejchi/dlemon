class AddPhoneNumberToOutreaches < ActiveRecord::Migration[5.1]
  def change
    add_column :outreaches, :phone_number, :string, null: true
    add_index :outreaches, [:phone_number, :campaign_name], unique: true

    reversible do |dir|
      dir.up do
        execute <<~SQL
          UPDATE outreaches
          SET phone_number = (
            SELECT phone_number FROM customers where customers.id = outreaches.customer_id
          )
        SQL
        change_column_null :outreaches, :phone_number, false
      end
    end
  end
end
