class RemoveRefundedFromShipments < ActiveRecord::Migration[5.0]
  def change
    remove_column :shipments, :refunded, :boolean if column_exists?(:shipments, :refunded)
  end
end
