class AddIndexToOrderTransitionsOrderId < ActiveRecord::Migration[5.1]
  disable_ddl_transaction!

  def change
    puts "Deleting transitions"
    # Not really reversible but idea is to clear out corrupted data
    reversible do |dir|
      dir.up do
        execute <<~SQL
          DELETE FROM order_transitions
          WHERE order_transitions.order_id NOT IN(
            SELECT id from orders where orders.id = order_transitions.order_id
          )
        SQL
      end
    end

    puts "Done deleting transaction, will build index concurrently"
    add_foreign_key :order_transitions, :orders, on_delete: :cascade
    add_index:order_transitions, :order_id, algorithm: :concurrently
  end
end
