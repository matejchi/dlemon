class AddCalculatedTotalsToOrder < ActiveRecord::Migration[5.1]
  def change
    add_column :orders, :item_total, :numeric
    add_column :orders, :item_discount, :numeric
  end
end
