class RemoveShortUrls < ActiveRecord::Migration[5.1]
  def change
    drop_table :short_urls
  end
end
