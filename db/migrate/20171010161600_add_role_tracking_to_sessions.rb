class AddRoleTrackingToSessions < ActiveRecord::Migration[5.1]
  def change
    add_column :sessions, :sender_roles, :text, array: true, default: []
    reversible do |dir|
      dir.up do
        execute <<~SQL
          UPDATE sessions SET sender_roles = (
            SELECT array_agg(sender_role) FROM
            (
              SELECT DISTINCT sender_role, MIN(created_at)
              FROM messages
              WHERE messages.session_id = sessions.id
              GROUP BY sender_role
              ORDER BY MIN(created_at) ASC
            ) sub
          )
        SQL
      end
    end
  end
end
