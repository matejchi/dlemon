class AddBitlyIdToCardAndAddress < ActiveRecord::Migration[5.1]
  def change
    add_column :cards, :bitly_id, :string
    add_column :addresses, :bitly_id, :string
  end
end
