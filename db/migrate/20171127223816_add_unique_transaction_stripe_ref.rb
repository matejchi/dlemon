class AddUniqueTransactionStripeRef < ActiveRecord::Migration[5.1]
  def change
    add_index :transactions, :stripe_ref, unique: true
  end
end
