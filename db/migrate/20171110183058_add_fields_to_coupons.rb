class AddFieldsToCoupons < ActiveRecord::Migration[5.1]

  def change
    rename_column :coupons, :stripe_id, :code
    add_column :coupons, :description, :string, null: true
    add_column :coupons, :expires, :datetime, null: true
    add_column :coupons, :active, :boolean, default: true
    add_column :coupons, :amount_off, :decimal, precision: 12, scale: 4, null: true
    add_column :coupons, :percent_off, :integer, null: true

    reversible do |dir|
      dir.up do
        stripe_coupons = Stripe::Coupon.list(limit: 100)

        stripe_coupons.each do |c|
          coupon = Coupon.where(code: c.id).first_or_initialize
          coupon.expires = 10.years.from_now
          coupon.amount_off = c.amount_off&.fdiv(100)
          coupon.percent_off = c.percent_off
          coupon.save!
        end
      end
    end
  end
end
