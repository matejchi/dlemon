class RemoveShipmentsShippedAt < ActiveRecord::Migration[5.1]
  def change
    remove_column :shipments, :shipped_at, :datetime
  end
end
