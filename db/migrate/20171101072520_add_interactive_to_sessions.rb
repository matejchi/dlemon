class AddInteractiveToSessions < ActiveRecord::Migration[5.1]
  def change
    add_column :sessions, :interactive, :bool, null: false, default: true

    reversible do |dir|
      dir.up do
        [%w(notification), %w(outreach), %w(outreach notification), %w(notification outreach)].each do |sender_roles|
          Session.where(sender_roles: sender_roles).update_all(interactive: false)
        end
      end
    end
  end
end
