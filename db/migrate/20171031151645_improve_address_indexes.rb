class ImproveAddressIndexes < ActiveRecord::Migration[5.1]
  def change
    add_index :addresses, [:type, :customer_id]
    add_index :addresses, :type
  end
end
