class AddNewFulfillmentAddress < ActiveRecord::Migration[5.1]
  def change
    FulfillmentAddress.find_or_create_by!({
      email: 'p.decker@seafrigo-usa.com',
      carrier_facility: 'sfgo',
      street1: '925 Julia St',
      city: 'Elizabeth',
      state: 'NJ',
      country: 'US',
      zip: '07201',
      time_zone: 'EST'
    })
  end
end
