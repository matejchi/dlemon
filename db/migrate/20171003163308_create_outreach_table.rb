class CreateOutreachTable < ActiveRecord::Migration[5.1]
  def change
    create_table :outreaches, id: :uuid, default: -> { "uuid_generate_v4()" } do |t|
      t.references :customer, foreign_key: true, on_delete: :cascade, type: :uuid
      t.string :campaign_name
      t.datetime :sent_at
      t.index ["customer_id", "campaign_name"], unique: true
      t.index "sent_at"
    end
  end
end
