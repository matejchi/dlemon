class DropProductsStripeId < ActiveRecord::Migration[5.1]
  def change
    remove_column :products, :stripe_id, :string
  end
end
