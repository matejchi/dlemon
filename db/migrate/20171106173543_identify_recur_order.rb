class IdentifyRecurOrder < ActiveRecord::Migration[5.1]
  def up
    execute <<~SQL
      UPDATE orders
      SET recur = 't'
      WHERE id IN(
        SELECT id FROM (
          SELECT DISTINCT ON (customer_id) customer_id, id
          FROM orders
          WHERE vip = 't' AND status IN ('fulfilled', 'queued', 'paid')
          ORDER BY customer_id, created_at DESC
        ) first_recurring_order
      )
    SQL
  end

  def down
    Order.update_all(recur: false)
  end
end
