class SoftDeleteSfgo < ActiveRecord::Migration[5.1]
  def up
    FulfillmentAddress.where(carrier_facility: 'sfgo').first&.destroy
  end

  def down
    FulfillmentAddress.with_deleted.where(carrier_facility: 'sfgo').first&.restore
  end
end
