class AddIndexCustomerNeedsAttention < ActiveRecord::Migration[5.1]
  def change
    add_index :customers, :needs_attention, where: "needs_attention = 't'"
  end
end
