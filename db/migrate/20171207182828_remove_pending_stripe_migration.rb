class RemovePendingStripeMigration < ActiveRecord::Migration[5.1]
  def change
    remove_column :orders, :pending_stripe_migration, :boolean
  end
end
