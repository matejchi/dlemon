class AddBatchIdToShipmentsAndOrders < ActiveRecord::Migration[5.1]
  def change
    add_column :shipments, :batch_id, :uuid
    add_column :orders, :batch_id, :uuid
  end
end
