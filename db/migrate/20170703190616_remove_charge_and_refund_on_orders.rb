class RemoveChargeAndRefundOnOrders < ActiveRecord::Migration[5.0]
  def change
    remove_column :orders, :charge, :jsonb
    remove_column :orders, :refund, :jsonb
  end
end
