class CreateTransactions < ActiveRecord::Migration[5.1]
  def change
    create_table :transactions, id: :uuid, default: -> { "uuid_generate_v4()" } do |t|
      t.references :order, foreign_key: true, on_delete: :cascade, type: :uuid
      t.references :customer, foreign_key: true, on_delete: :cascade, type: :uuid

      t.string :type, null: false
      t.decimal :amount
      t.string :stripe_ref
      t.string :status
      t.jsonb :outcome, default: {}
      t.timestamps
    end
  end
end
