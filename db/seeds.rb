ActiveRecord::Base.transaction do
  # Create products and SKUs
  @product = Product.where(name: "sleep").first_or_create!
  @product.skus.where(stripe_id: "868137000122").find_or_create_by!(price: 65, currency: 'usd')

  @product = Product.where(name: "+charcoal").first_or_create!
  @product.skus.where(stripe_id: "868137000108").first_or_create!(price: 65, currency: 'usd')

  @product = Product.where(name: "+collagen").first_or_create!
  @product.skus.where(stripe_id: "868137000115").first_or_create!(price: 65, currency: 'usd')

  @product = Product.where(name: "+ginseng").first_or_create!
  @product.skus.where(stripe_id: "868137000139").first_or_create!(price: 65, currency: 'usd')

  @product = Product.where(name: "+matcha").first_or_create!
  @product.skus.where(stripe_id: "868137000146").first_or_create!(price: 65, currency: 'usd')

  FulfillmentAddress.find_or_create_by!({
    email: "fulfillment.east@dirtylemon.com",
    carrier_facility: 'njfs',
    street1: "145 Talmadge Rd",
    city: "Edison",
    state: "New Jersey",
    country: "US",
    zip: "08817",
    time_zone: "Eastern Time (US & Canada)"
  })

  FulfillmentAddress.find_or_create_by!({
    email: "fulfillment.west@dirtylemon.com",
    carrier_facility: 'spwg',
    street1: "1320 Weber Ave",
    city: "Stockton",
    state: "California",
    country: "US",
    zip: "95203",
    time_zone: "Pacific Time (US & Canada)"
  })

  # Create coupons
  Coupon.find_or_create_by!(code: '100OFF', percent_off: 100)
  Coupon.find_or_create_by!(code: '75OFF', percent_off: 75)
  Coupon.find_or_create_by!(code: '50OFF', percent_off: 50)
  Coupon.find_or_create_by!(code: '33OFF', percent_off: 33)
  Coupon.find_or_create_by!(code: '25OFF', percent_off: 25)
  Coupon.find_or_create_by!(code: '20', amount_off: 20)
  Coupon.find_or_create_by!(code: '65', amount_off: 65)
  Coupon.find_or_create_by!(code: '130', amount_off: 130)
  Coupon.find_or_create_by!(code: '195', amount_off: 195)
  Coupon.find_or_create_by!(code: '260', amount_off: 260)
  Coupon.find_or_create_by!(code: '325', amount_off: 325)

  # Users
  @user = User.find_or_create_by!(first_name: 'TODO', email: 'TODO')

  @bot = User.find_or_create_by!(first_name: 'bot', email: 'tech+bot@dirtylemon.com')

  @admin = User.find_or_create_by!(first_name: 'admin', email: 'tech+admin@dirtylemon.com')

  @support = User.find_or_create_by!(first_name: 'support', email: 'tech+support@dirtylemon.com')

  @website = User.find_or_create_by!(first_name: 'website', email: 'tech+website@dirtylemon.com')

  @hitl = User.find_or_create_by!(first_name: "HITL", email: "tech+hitl@dirtylemon.com")
end
