# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20171212173643) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "pg_stat_statements"
  enable_extension "pg_trgm"
  enable_extension "uuid-ossp"

  create_table "addresses", id: :uuid, default: -> { "uuid_generate_v4()" }, force: :cascade do |t|
    t.string "street1"
    t.string "street2"
    t.string "city"
    t.string "state"
    t.string "country"
    t.string "zip"
    t.string "type"
    t.decimal "lat"
    t.decimal "lng"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.uuid "customer_id"
    t.string "bitly_id"
    t.index ["customer_id"], name: "index_addresses_on_customer_id"
    t.index ["type", "customer_id"], name: "index_addresses_on_type_and_customer_id"
    t.index ["type"], name: "index_addresses_on_type"
  end

  create_table "batches", id: :uuid, default: -> { "uuid_generate_v4()" }, force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "enqueued_at", null: false
    t.uuid "fulfillment_address_id", null: false
    t.string "name", null: false
    t.index ["fulfillment_address_id", "name"], name: "index_batches_on_fulfillment_address_id_and_name", unique: true
    t.index ["fulfillment_address_id"], name: "index_batches_on_fulfillment_address_id"
  end

  create_table "cards", id: :uuid, default: -> { "uuid_generate_v4()" }, force: :cascade do |t|
    t.uuid "customer_id", null: false
    t.string "stripe_id"
    t.string "exp_month"
    t.string "exp_year"
    t.string "last4"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "bitly_id"
    t.index ["customer_id"], name: "index_cards_on_customer_id"
  end

  create_table "coupons", id: :uuid, default: -> { "uuid_generate_v4()" }, force: :cascade do |t|
    t.string "code"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "description"
    t.datetime "expires"
    t.boolean "active", default: true
    t.decimal "amount_off", precision: 12, scale: 4
    t.integer "percent_off"
    t.index ["code"], name: "index_coupons_on_code", unique: true
  end

  create_table "customers", id: :uuid, default: -> { "uuid_generate_v4()" }, force: :cascade do |t|
    t.string "phone_number"
    t.string "stripe_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "email"
    t.string "first_name"
    t.string "last_name"
    t.boolean "wants_prerelease_updates", default: false
    t.bigint "crm_id"
    t.string "init_id"
    t.datetime "confirmed_at"
    t.datetime "confirmation_generated_at"
    t.string "confirmation_token"
    t.string "country_code"
    t.string "reference", null: false
    t.string "time_zone", default: "UTC"
    t.datetime "unsubscribed_at"
    t.string "unconfirmed_phone_number"
    t.string "tags", default: [], array: true
    t.boolean "needs_attention", default: false
    t.datetime "renew_recur_order_on"
    t.index ["needs_attention"], name: "index_customers_on_needs_attention", where: "(needs_attention = true)"
    t.index ["phone_number", "stripe_id"], name: "index_customers_on_phone_number_and_stripe_id"
    t.index ["phone_number"], name: "index_customers_on_phone_number", unique: true
    t.index ["reference"], name: "index_customers_on_reference", unique: true
    t.index ["renew_recur_order_on"], name: "index_customers_on_renew_recur_order_on"
    t.index ["unconfirmed_phone_number"], name: "index_customers_on_unconfirmed_phone_number", unique: true
  end

  create_table "events", id: :uuid, default: -> { "uuid_generate_v4()" }, force: :cascade do |t|
    t.string "name", null: false
    t.uuid "eventable_id", null: false
    t.string "eventable_type", null: false
    t.jsonb "data", default: {}
    t.datetime "created_at"
    t.index ["eventable_id", "eventable_type"], name: "index_events_on_eventable_id_and_eventable_type"
  end

  create_table "fulfillment_addresses", id: :uuid, default: -> { "uuid_generate_v4()" }, force: :cascade do |t|
    t.string "carrier_facility"
    t.string "street1"
    t.string "street2"
    t.string "city"
    t.string "state"
    t.string "zip"
    t.string "country"
    t.string "email"
    t.string "time_zone"
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_fulfillment_addresses_on_deleted_at"
  end

  create_table "line_items", id: :uuid, default: -> { "uuid_generate_v4()" }, force: :cascade do |t|
    t.string "description"
    t.decimal "amount"
    t.integer "quantity"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.uuid "sku_id", null: false
    t.uuid "order_id", null: false
    t.index ["order_id"], name: "index_line_items_on_order_id"
    t.index ["sku_id"], name: "index_line_items_on_sku_id"
  end

  create_table "messages", id: :uuid, default: -> { "uuid_generate_v4()" }, force: :cascade do |t|
    t.text "content"
    t.string "content_type"
    t.string "sender_role"
    t.datetime "sent_at", null: false
    t.datetime "processed_at"
    t.datetime "created_at", null: false
    t.uuid "session_id", null: false
    t.index ["session_id"], name: "index_messages_on_session_id"
  end

  create_table "order_coupons", force: :cascade do |t|
    t.uuid "order_id"
    t.uuid "coupon_id"
  end

  create_table "order_transitions", id: :uuid, default: -> { "uuid_generate_v4()" }, force: :cascade do |t|
    t.string "to_state", null: false
    t.json "metadata", default: {}
    t.integer "sort_key", null: false
    t.boolean "most_recent", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.uuid "order_id"
    t.index ["order_id"], name: "index_order_transitions_on_order_id"
  end

  create_table "orders", id: :uuid, default: -> { "uuid_generate_v4()" }, force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.decimal "tax"
    t.decimal "total"
    t.decimal "shipping_fee"
    t.uuid "coupon_id"
    t.jsonb "shipping", default: {}
    t.string "currency"
    t.string "reference", null: false
    t.string "status"
    t.string "stripe_id"
    t.uuid "customer_id", null: false
    t.uuid "batch_id"
    t.boolean "recur", default: false
    t.boolean "vip", default: false, null: false
    t.decimal "subtotal"
    t.decimal "tax_rate"
    t.decimal "item_total"
    t.decimal "item_discount"
    t.datetime "scheduled_confirm_on"
    t.index ["coupon_id"], name: "index_orders_on_coupon_id"
    t.index ["customer_id"], name: "index_orders_on_customer_id"
    t.index ["reference"], name: "index_orders_on_reference", unique: true
    t.index ["scheduled_confirm_on"], name: "index_orders_on_scheduled_confirm_on", where: "(scheduled_confirm_on IS NOT NULL)"
  end

  create_table "outreaches", id: :uuid, default: -> { "uuid_generate_v4()" }, force: :cascade do |t|
    t.uuid "customer_id"
    t.string "campaign_name"
    t.datetime "sent_at"
    t.string "phone_number", null: false
    t.index ["customer_id", "campaign_name"], name: "index_outreaches_on_customer_id_and_campaign_name", unique: true
    t.index ["customer_id"], name: "index_outreaches_on_customer_id"
    t.index ["phone_number", "campaign_name"], name: "index_outreaches_on_phone_number_and_campaign_name", unique: true
    t.index ["sent_at"], name: "index_outreaches_on_sent_at"
  end

  create_table "products", id: :uuid, default: -> { "uuid_generate_v4()" }, force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "description"
    t.index ["description"], name: "index_products_on_description"
  end

  create_table "sessions", id: :uuid, default: -> { "uuid_generate_v4()" }, force: :cascade do |t|
    t.string "tags", default: [], array: true
    t.string "topics", default: [], array: true
    t.uuid "customer_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "sender_roles", default: [], array: true
    t.boolean "interactive", default: true, null: false
    t.index ["tags"], name: "index_sessions_on_tags"
    t.index ["topics"], name: "index_sessions_on_topics"
  end

  create_table "shipments", id: :uuid, default: -> { "uuid_generate_v4()" }, force: :cascade do |t|
    t.string "tracking_number"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.uuid "sku_id"
    t.string "carrier"
    t.string "easypost_shipment_id"
    t.string "easypost_tracker_id"
    t.datetime "eta"
    t.decimal "rate"
    t.string "service"
    t.string "tracking_url"
    t.string "tracking_status"
    t.datetime "tracking_status_at"
    t.uuid "order_id", null: false
    t.uuid "fulfillment_address_id"
    t.datetime "fulfillable_at"
    t.datetime "purchased_at"
    t.datetime "refunded_at"
    t.uuid "batch_id"
    t.string "label_png_s3_key"
    t.string "label_zpl_s3_key"
    t.index ["order_id"], name: "index_shipments_on_order_id"
  end

  create_table "skus", id: :uuid, default: -> { "uuid_generate_v4()" }, force: :cascade do |t|
    t.decimal "price"
    t.string "currency"
    t.uuid "product_id", null: false
    t.string "stripe_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["stripe_id"], name: "index_skus_on_stripe_id", unique: true
  end

  create_table "suggested_messages", id: :uuid, default: -> { "uuid_generate_v4()" }, force: :cascade do |t|
    t.uuid "session_id", null: false
    t.string "remote_id"
    t.text "content"
    t.string "content_type"
    t.integer "selected_count", default: 0
    t.index ["session_id"], name: "index_suggested_messages_on_session_id"
  end

  create_table "transactions", id: :uuid, default: -> { "uuid_generate_v4()" }, force: :cascade do |t|
    t.uuid "order_id"
    t.uuid "customer_id"
    t.string "type", null: false
    t.decimal "amount"
    t.string "stripe_id"
    t.string "status"
    t.jsonb "outcome", default: {}
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["customer_id"], name: "index_transactions_on_customer_id"
    t.index ["order_id"], name: "index_transactions_on_order_id"
    t.index ["stripe_id"], name: "index_transactions_on_stripe_id", unique: true
  end

  create_table "users", id: :uuid, default: -> { "uuid_generate_v4()" }, force: :cascade do |t|
    t.string "email"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "encrypted_password", limit: 128
    t.string "first_name"
    t.boolean "approved", default: false
    t.string "token"
    t.integer "sign_in_count", default: 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.index ["email"], name: "index_users_on_email"
  end

  add_foreign_key "batches", "fulfillment_addresses"
  add_foreign_key "order_transitions", "orders", on_delete: :cascade
  add_foreign_key "outreaches", "customers"
  add_foreign_key "transactions", "customers"
  add_foreign_key "transactions", "orders"
end
