require 'sidekiq'

redis_url = ENV.fetch('REDIS_URL') { 'redis://localhost:6379' }

Sidekiq.configure_client do |config|
  config.redis = {
    url: redis_url,
    driver: :hiredis
  }
end

Sidekiq.configure_server do |config|
  config.redis = {
    url: redis_url,
    driver: :hiredis
  }

  config.server_middleware do |chain|
    chain.add Sidekiq::RetryMonitoring::Middleware
  end
end
