ActionController::Renderers.add :resource do |resource, options|
  self.content_type ||= Mime[:json]
  SerializableResource.serialize(resource).to_json
end
