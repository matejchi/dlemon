# Set Service timeout
# https://github.com/heroku/rack-timeout#service-timeout
Rack::Timeout.service_timeout = 15 # seconds

# Disable logger
Rack::Timeout::Logger.disable
