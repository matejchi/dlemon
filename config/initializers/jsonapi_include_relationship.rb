module JSONAPI::Serializable::IncludeRelationship
  def include_relation(name)
    attribute(name) do
      relation =  @object.send(name)

      if relation.respond_to?(:to_ary)
        Array(relation).map(&:serialize)
      elsif relation
        relation.serialize
      else
        nil
      end
    end
  end
end
JSONAPI::Serializable::Resource.extend(JSONAPI::Serializable::IncludeRelationship)
