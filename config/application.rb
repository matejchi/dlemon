require_relative 'boot'
require_relative '../app/middleware/cloudfront_denier'
require_relative '../app/middleware/rescue_json_parse_errors'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module DirtylemonApi
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 5.1

    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

    config.active_job.queue_adapter = :sidekiq

    # Use UUIDs
    config.active_record.primary_key = :uuid

    config.middleware.use Rack::Attack

    config.middleware.use RescueJsonParseErrors

    # Disallow Cloudfront from fetching anything but /assets
    config.middleware.use CloudfrontDenier,
      target: 'https://api.dirtylemon.com/'

    config.middleware.insert_before 0, Rack::Cors do
      allow do
        origins '*'

        resource '/assets/*', headers: :any, methods: [:get]
        resource '*', headers: :any, methods: [:get, :post, :put, :patch]
      end
    end
  end
end
