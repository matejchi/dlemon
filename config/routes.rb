Rails.application.routes.draw do

  mount ActionCable.server => '/cable'

  namespace :api, path: '', defaults: { format: :json } do
    namespace :v1 do
      resources :addresses, only: [:show, :update] do
        collection do
          post :validate
        end
      end

      resources :customers, only: [:index, :show, :create, :update, :destroy], shallow: true do
        member do
          post :confirm
          post :generate_confirmation
          post :send_confirmation
          post :subscribe
          post :unsubscribe
          get :stats
        end

        resources :addresses, only: [:create]
        resource :card, only: [:create]

        resources :orders, only: [:index, :show, :create, :update] do
          post :schedule
          post :pay
          post :fulfill
          post :queue
          post :unqueue
          post :cancel
          post :return
          post :confirm
        end

        collection do
          get :need_attention
        end
      end

      resources :customers, only: [] do
        resources :sessions, only: [:index, :show, :update] do
          resources :messages, only: [:index, :create]
        end
      end

      resources :sessions, only: [:index] do
        collection do
          get :recent
        end
      end

      resources :shipments, only: [] do
        collection do
          post :webhook
        end
      end

      controller :bot do
        post 'bot/trigger'
      end

      resources :coupons, only: [:index]
      resources :skus, only: [:index]

      get :analytics, controller: :analytics, action: :index
    end

    root to: 'home#index', via: :all
  end

  namespace :stripe do
    resource :events, only: :create
  end

  resources :customers, only: [] do
    resource :card, only: [:edit]
    resources :addresses, only: [:show, :edit, :update]
  end
end
