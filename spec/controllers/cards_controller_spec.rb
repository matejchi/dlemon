require 'rails_helper'

RSpec.describe CardsController, type: :controller do
  context "authorized" do
    describe "GET edit" do
      let(:customer) { create(:customer, :confirmed, :card) }
      before { get :edit, params: { customer_id: customer.to_param } }

      it "assigns the requested card as @card" do
        expect(assigns(:card).last4).to eq(customer.card.last4)
      end

      it "assigns the customer as @customer" do
        expect(assigns(:customer)).to eq(customer)
      end

      it "responds with 200 and render template" do
        expect(response).to have_http_status(200)
        expect(response).to render_template :edit
      end
    end
  end
end
