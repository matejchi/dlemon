require 'rails_helper'

RSpec.describe Api::V1::AddressesController, type: :controller do

  it { should use_before_action(:authenticate) }

  context 'authorized' do
    let(:user) { create(:user) }

    before do
      request.headers['HTTP_AUTHORIZATION'] = "Bearer #{user.token}"
    end

    describe 'GET show' do
      let(:address) { create(:address) }
      before { get :show, params: { id: address.id }, format: :json }

      it "assigns the requested address as @address" do
        expect(assigns(:address)).to eq(address)
      end

      it "responds with 200 and render template" do
        expect(response).to have_http_status(200)
        expect(json(response)[:id]).to eq address.id
      end
    end

    describe 'POST create', :pending do
    end

    describe 'PATCH update', :pending do
    end

    describe 'POST validate', :pending do
    end
  end
end
