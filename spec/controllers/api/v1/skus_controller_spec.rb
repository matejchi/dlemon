require 'rails_helper'

RSpec.describe Api::V1::SkusController, type: :controller do

  before { create(:coupon) }

  it { should use_before_action(:authenticate) }

  context "authorized" do
    let(:user) { create(:user) }

    before do
      request.headers['HTTP_AUTHORIZATION'] = "Bearer #{user.token}"
    end

    describe "GET index" do
      before { get :index, format: :json }

      let(:skus) { Sku.all }

      it "assigns all skus to @skus" do
        expect(assigns(:skus)).to eq(skus)
      end

      it "responds with 200 and render template" do
        expect(response).to have_http_status(200)
      end
    end
  end
end
