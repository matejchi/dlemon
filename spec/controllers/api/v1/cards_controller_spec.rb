require 'rails_helper'

RSpec.describe Api::V1::CardsController, type: :controller do

  describe "POST create" do
    let(:card) { create(:card) }
    let(:customer) { create(:customer, :confirmed, stripe_id: Stripe::Customer.create.id, card: card) }

    context "with valid params" do
      let(:params) do
        { customer_id: customer.id, stripeToken: StripeMock.generate_card_token }
      end

      before { post :create, params: params, format: :json }

      context "assigns" do
        it 'updates and return the card' do
          expect(response).to have_http_status(201)
          expect(json(response)[:last4]).to eq customer.reload.card.last4
          expect(assigns(:card)).to be_a(Card)
          expect(assigns(:card)).to be_valid
        end
      end
    end

    context "with invalid params" do
      let(:params) { { customer_id: 'inexisting' } }

      it "doesn't create a new Event record" do
        expect {
          post :create, params: params, format: :json
        }.not_to change(Event, :count)
      end

      context "with non existing customer" do
        it "responds with 404" do
          post :create, params: params, format: :json
          expect(response).to have_http_status(404)
        end
      end
    end
  end
end
