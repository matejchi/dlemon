require 'rails_helper'

RSpec.describe Api::V1::MessagesController, type: :controller do

  it { should use_before_action(:authenticate) }

  context 'authorized' do
    let(:user) { create(:user) }

    before do
      request.headers['HTTP_AUTHORIZATION'] = "Bearer #{user.token}"
    end

    describe 'GET index' do
      let(:customer) { create(:customer) }
      let!(:session) { setup_session(customer, 1.minute.ago) }

      before { get :index, params: params, format: :json }

      context 'with valid params' do
        context 'with given session' do
          let(:params) do
            {
              customer_id: customer.id,
              session_id: session.id
            }
          end

          it 'assigns the requested customer as @customer' do
            expect(assigns(:customer)).to eq(customer)
          end

          it 'assigns the requested session as @session' do
            expect(assigns(:session)).to eq(session)
          end

          it 'responds with 200 and render template' do
            expect(response).to have_http_status(200)
          end
        end

        context 'with current session' do
          let(:params) do
            {
              customer_id: customer.id,
              session_id: 'current'
            }
          end

          it 'assigns the requested customer as @customer' do
            expect(assigns(:customer)).to eq(customer)
          end

          it 'assigns the requested session as @session' do
            expect(assigns(:session)).to eq(session)
          end

          it 'responds with 200 and render template' do
            expect(response).to have_http_status(200)
          end
        end
      end

      context "with invalid params" do
        let(:params) do
          {
            customer_id: 'inexisting',
            session_id: 'inexisting'
          }
        end

        it "responds with 404 and renders a json error" do
          get :index, params: params
          expect(response).to have_http_status(404)
          expect(json(response)[:code]).to eq('RecordNotFound')
        end
      end
    end

    describe 'POST create' do
      let(:customer) { create(:customer, :confirmed) }

      context "with valid params" do
        shared_examples "successful message creation" do
          let(:params) do
            {
              customer_id: customer.id ,
              session_id: 'current',
              message: {
                sender_role: 'agent',
                content: 'test content',
                content_type: 'text',
                sent_at: Time.current.iso8601
              }
            }
          end

          before { post :create, params: params, format: :json }

          it "assigns the requested customer as @customer" do
            expect(assigns(:customer)).to be_a(Customer)
            expect(assigns(:customer)).to eq(customer)
          end

          it "assigns the requested session as @session" do
            expect(assigns(:session)).to be_a(Session)
          end

          it "assigns the new message as @message" do
            expect(assigns(:message)).to be_a(Message)
          end

          it "responds with 201 and renders json" do
            expect(response).to have_http_status(201)
            expect(json(response)[:content]).to eq 'test content'
          end
        end


        context "with current session" do
          let!(:session) { setup_session(customer, 1.minute.ago) }
          include_examples "successful message creation"
        end

        context "without session" do
          include_examples "successful message creation"
        end
      end

      context "with invalid params" do
        context "with current session" do
          let(:params) do
            {
              customer_id: customer.id,
              session_id: 'current'
            }
          end

          it "responds with 422 and renders a json error" do
            post :create, params: params, format: :json
            expect(response).to have_http_status(422)
            expect(json(response)[:code]).to eq('ParameterMissing')
          end
        end

        context "with given session" do
          let(:params) do
            {
              customer_id: customer.id,
              session_id: session.id
            }
          end

          it "responds with 404 and renders a json error" do
            post :create, params: params, format: :json
            expect(response).to have_http_status(404)
            expect(json(response)[:code]).to eq('RecordNotFound')
          end
        end
      end
    end
  end
end
