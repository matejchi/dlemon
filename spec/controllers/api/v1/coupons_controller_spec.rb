require 'rails_helper'

RSpec.describe Api::V1::CouponsController, type: :controller do
  before { create(:coupon) }

  it { should use_before_action(:authenticate) }

  context "authorized" do
    let(:user) { create(:user) }

    before do
      request.headers['HTTP_AUTHORIZATION'] = "Bearer #{user.token}"
    end

    describe "GET index" do
      before { get :index, format: :json }

      let(:coupons) { Coupon.all }

      it "assigns all coupons to @coupons" do
        expect(assigns(:coupons)).to eq(coupons)
      end

      it "responds with 200 and render template" do
        expect(response).to have_http_status(200)
      end
    end
  end
end
