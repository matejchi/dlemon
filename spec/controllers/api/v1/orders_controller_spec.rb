require 'rails_helper'

RSpec.describe Api::V1::OrdersController, type: :controller do
  it { should use_before_action(:authenticate) }

  context "authorized" do
    let(:user) { create(:user) }
    let(:line_items_params) { [{ quantity: 1, sku: Sku.last.stripe_id }] }

    before do
      request.headers['HTTP_AUTHORIZATION'] = "Bearer #{user.token}"
    end

    describe "GET show" do
      let(:order) { create(:order) }
      before { get :show, params: { id: order.to_param }, format: :json }

      it "assigns the requested order as @order" do
        expect(assigns(:order)).to eq(order)
      end

      it "responds with 200 and render template" do
        expect(response).to have_http_status(200)
        expect(json(response)[:id]).to eq order.id
      end
    end

    describe "POST create" do
      context "with a confirmed customer" do
        let(:customer) { create(:customer, :confirmed) }

        context "with a shipping payload" do
          let(:shipping) { attributes_for(:shipping) }
          let(:params) do
            { shipping: shipping, line_items: line_items_params }
          end

          it "uses the shipping payload" do
            post :create, params: { customer_id: customer.to_param, order: params }, format: :json
            expect(assigns(:order_action).order.shipping.street1).to eq(shipping[:street1])
          end
        end

        context "without a shipping payload" do
          let(:params) do
            {
              line_items: [{ quantity: 1, sku: Sku.last.stripe_id }]
            }
          end

          it "renders an error" do
            post :create, params: { customer_id: customer.to_param, order: params }, format: :json
            expect(response).to have_http_status(422)
            expect(json(response)[:code]).to eq('RecordInvalid')
          end
        end

        context "with a shipping address" do
          let(:customer) { create(:customer, :shipping_address, :confirmed) }
          let(:params) do
            { line_items: [{ quantity: 1, sku: Sku.first.stripe_id }] }
          end

          it "uses the customer's shipping address" do
            post :create, params: { customer_id: customer.to_param, order: params }, format: :json
            expect(assigns(:order_action).order.shipping.street1).to eq(customer.shipping_address.street1)
          end
        end

        context "without a shipping address" do
          let(:params) do
            {
              line_items: [{ quantity: 1, sku: Sku.first.stripe_id }]
            }
          end

          it "renders an error" do
            post :create, params: { customer_id: customer.to_param, order: params }, format: :json
            expect(response).to have_http_status(422)
            expect(json(response)[:code]).to eq('RecordInvalid')
          end
        end

        context "with a shipping address and a shipping payload" do
          let!(:shipping_address) { create(:shipping_address, customer: customer) }
          let(:shipping) { attributes_for(:shipping) }
          let(:params) do
            {
              line_items: [{ quantity: 1, sku: Sku.first.stripe_id }],
              shipping: shipping
            }
          end

          it "uses the shipping payload" do
            post :create, params: { customer_id: customer.to_param, order: params }, format: :json
            expect(assigns(:order_action).order.shipping.street1).to eq(shipping[:street1])
          end
        end

        context "with customer from GB" do
          let!(:shipping_address) { create(:shipping_address, customer: customer, country: "GB") }
          let(:params) { { line_items: line_items_params } }

          it "adds shipping_fee" do
            post :create, params: { customer_id: customer.to_param, order: params }, format: :json
            expect(assigns(:order_action).order.shipping_fee).to eql(12.00)
          end
        end

        context "with valid params" do
          let(:sku) { Sku.first }
          let(:params) do
            {
              line_items: [{ quantity: 2, sku: sku.stripe_id }],
              shipping: attributes_for(:shipping)
            }
          end

          it "creates a new record with line items and pricing populated" do
            expect {
              post :create, params: { customer_id: customer.to_param, order: params }, format: :json
            }.to change(Order, :count).by(1)

            order = assigns(:order_action).order
            expect(order.line_items[0].sku).to be_a(Sku)
            expect(order.line_items[0].sku.stripe_id).to eq(sku.stripe_id)
            expect(order.line_items[0].quantity).to eql(2)

            expect(order.item_total).to eq(2 * 65.0) # 2 cases 65$
            expect(order.item_discount).to eq(0)     # No coupons
            expect(order.subtotal).to eq(order.item_total)

            expect(order.shipping_fee).to eq(0.0)

            expect(order.tax).to eq(0.0)
            expect(order.tax_rate).to eq(0.0)
            expect(order.total).to eql(order.subtotal)
          end

          context do
            before { post :create, params: { customer_id: customer.to_param, order: params }, format: :json }

            it "assigns a newly created and valid record" do
              expect(assigns(:order_action).order).to be_a(Order)
              expect(assigns(:order_action).order).to be_valid
            end

            it "accepts line item" do
              expect(assigns(:order_action).order.line_items).to all(be_valid)
            end

            it "responds with 201 and renders json" do
              expect(response).to have_http_status(201)
            end
          end

          context "with a valid coupon" do
            before { Coupon.create(code: '25OFF', percent_off: 25) }

            it "accepts percentage coupon (singular)" do
              post :create, params: { customer_id: customer.to_param, order: params.deep_merge({ coupon: '25OFF' }) }, format: :json
              expect(response).to have_http_status(201)

              order = assigns(:order_action).order
              expect(order.coupons.size).to eql(1)
              expect(order.coupons.first).to be_a(Coupon)
              expect(order.coupons.first.code).to eq('25OFF')

              expect(order.item_total).to eq(2 * 65.0) # 2 cases 65$
              expect(order.item_discount).to eq(2 * 65.0 * 0.25)  # 2 cases 65$ 25% off
              expect(order.subtotal).to eq(2 * 65.0 * (1 - 0.25)) # 2 cases 65$ 25% off

              expect(order.shipping_fee).to eq(0.0)

              expect(order.tax).to eq(0.0)
              expect(order.tax_rate).to eq(0.0)
              expect(order.total).to eql(order.subtotal) # Same as subtotal – no tax, no shipping
            end
          end

          context 'when customer is vip' do
            let(:customer) { create(:customer, :vip) }

            it "computes vip pricing for orders" do
              post :create, params: { customer_id: customer.to_param, order: params }, format: :json
              expect(response).to have_http_status(201)

              order = assigns(:order_action).order
              expect(order.vip).to be true
              li = order.line_items.first
              expect(li.amount / li.quantity).to eql(sku.vip_price)
            end
          end
        end

        context "with invalid params" do
          context do
            let(:params) do
              {}
            end

            it "responds with 422 and renders a json error" do
              post :create, params: { customer_id: customer.to_param, order: params }, format: :json
              expect(response).to have_http_status(422)
              expect(json(response)[:code]).to eq('ParameterMissing')
            end
          end

          context "with an invalid SKU" do
            let(:params) do
              {
                line_items: [{ quantity: 1, sku: 'invalid' }],
                shipping: attributes_for(:shipping)
              }
            end

            it "renders an error" do
              post :create, params: { customer_id: customer.to_param, order: params }, format: :json
              expect(response).to have_http_status(404)
              expect(json(response)[:code]).to eq('RecordNotFound')
            end
          end

          context "with an invalid coupon" do
            let(:params) do
              {
                line_items: [{ quantity: 1, sku: Sku.first.stripe_id }],
                shipping: attributes_for(:shipping),
                coupon: 'inexisting'
              }
            end

            it "renders an error" do
              post :create, params: { customer_id: customer.to_param, order: params }, format: :json
              expect(response).to have_http_status(404)
              expect(json(response)[:code]).to eq('RecordNotFound')
            end
          end
        end
      end

      context "with a unconfirmed customer" do
        let(:customer) { create(:customer) }
        # TODO
      end
    end

    describe "PATCH update" do
      let(:order) { create(:order, shipping: build(:shipping, name: 'Test')) }

      context "with valid params" do
        let(:params) do
          {
            shipping: {
              name: 'Tested'
            }
          }
        end

        it "assigns the requested order as @order" do
          patch :update, params: { id: order.to_param, order: params }, format: :json
          expect(assigns(:order)).to be_a(Order)
          expect(assigns(:order)).to eq(order)
        end

        it "updates the order's shipping" do
          expect(order.shipping.name).to eq('Test')

          patch :update, params: { id: order.to_param, order: params }, format: :json
          order.reload

          expect(order.shipping.name).to eq('Tested')
        end

        it "responds with 200 and renders json" do
          patch :update, params: { id: order.to_param, order: params }, format: :json
          expect(response).to have_http_status(200)
          expect(json(response)[:id]).to eq order.id
        end

        context 'with order in queued state' do
          let(:order) { create(:order, :queued) }

          it "responds with 403 and renders a json error" do
            patch :update, params: { id: order.to_param, order: params }, format: :json
            expect(response).to have_http_status(403)
            expect(json(response)[:code]).to eq('Forbidden')
          end
        end

        context 'with order in canceled state' do
          let(:order) { create(:order, :canceled) }

          it "responds with 403 and renders a json error" do
            patch :update, params: { id: order.to_param, order: params }, format: :json
            expect(response).to have_http_status(403)
            expect(json(response)[:code]).to eq('Forbidden')
          end
        end

        context 'with order in fulfilled state' do
          let(:order) { create(:order, :fulfilled) }

          it "responds with 403 and renders a json error" do
            patch :update, params: { id: order.to_param, order: params }, format: :json
            expect(response).to have_http_status(403)
            expect(json(response)[:code]).to eq('Forbidden')
          end
        end
      end

      context "with invalid params" do
        let(:params) do
          {
            shipping: {
              name: nil
            }
          }
        end

        it "doesn't update the order's shipping" do
          expect {
            patch :update, params: { id: order.to_param, order: params }, format: :json
          }.not_to change(order, :shipping)
        end

        it "responds with 422 and renders a json error" do
          patch :update, params: { id: order.to_param, order: params }, format: :json
          expect(response).to have_http_status(422)
          expect(json(response)[:code]).to eq('RecordInvalid')
        end
      end
    end

    describe "POST schedule" do
      let(:scheduled_confirm_on) { (Time.current + 2.days).round }
      let(:order) { create(:order, :line_items, customer: customer) }
      let(:params) { { order_id: order.to_param } }

      context "with a confirmed customer" do
        let(:customer) { create(:customer, :shipping_address, :confirmed, :card) }

        context  "and missing scheduled_confirm_on" do
          it "doesn't transition the order and responds with 422" do
            expect {
              post :schedule, format: :json, params: params
              order.reload
            }.not_to change(order, :status)
            expect(response).to have_http_status(422)
            expect(json(response)[:code]).to eq('ParameterMissing')
          end
        end

        context "and valid scheduled_confirm_on" do
          let(:params) { super().merge(order: { scheduled_confirm_on: scheduled_confirm_on.as_json }) }

          it "transitions the order and responds with 200" do
            expect {
              post :schedule, format: :json, params: params
              order.reload
            }.to change(order, :status).to('scheduled')
            order.reload
            expect(order.status).to eql('scheduled')
            expect(response).to have_http_status(200)
            expect(order.scheduled_confirm_on).to eql(scheduled_confirm_on)
          end

          context "and order that is already scheduled" do
            before do
              order.scheduled_confirm_on = Time.current + 10.days
              order.state_machine.transition_to!(:scheduled)
            end

            it "updates the scheduled_confirm_on" do
              expect {
                post :schedule, format: :json, params: params
                order.reload
              }.to change(order, :scheduled_confirm_on).to(scheduled_confirm_on)
            end
          end
        end
      end

      context "with an unconfirmed customer" do
        let(:customer) { create(:customer) }

        it "doesn't transition the order and responds with 422" do
          expect {
            post :schedule, format: :json, params: params.merge(order: { scheduled_confirm_on: scheduled_confirm_on.as_json })
            order.reload
          }.not_to change(order, :status)

          expect(response).to have_http_status(422)
          expect(json(response)[:code]).to eq('RecordInvalid')
        end
      end
    end

    describe "POST pay" do
      context "with a confirmed customer" do
        let(:customer) { create(:customer, :shipping_address, :confirmed, :card) }

        context "with a payable order" do
          let(:order) { create(:order, :line_items, customer: customer) }

          it "assigns the requested order as @order" do
            post :pay, params: { order_id: order.to_param }, format: :json
            expect(assigns(:order)).to eq(order)
          end

          it "transitions the order" do
            expect {
              post :pay, params: { order_id: order.to_param }, format: :json
              order.reload
            }.to change(order, :status).to('paid')
          end

          it "doesn't mark the order as recur" do
            expect(order.recur).to be false
            expect {
              post :pay, params: { order_id: order.to_param }, format: :json
            }.not_to change(order, :status)
          end

          it "responds with 200 and renders json" do
            post :pay, params: { order_id: order.to_param }, format: :json
            expect(response).to have_http_status(200)
            expect(json(response)[:id]).to eq order.id
          end
        end

        context "with a unpayable order" do
          let(:order) { create(:order, :paid) }

          it "assigns the requested order as @order" do
            post :pay, params: { order_id: order.to_param }, format: :json
            expect(assigns(:order)).to eq(order)
          end

          it "doesn't transition the order" do
            expect {
              post :pay, params: { order_id: order.to_param }, format: :json
            }.not_to change(order, :status)
          end

          it "responds with 422 and renders a json error" do
            post :pay, params: { order_id: order.to_param }, format: :json
            expect(response).to have_http_status(422)
            expect(json(response)[:code]).to eq('RecordInvalid')
          end
        end
      end

      context "with an unconfirmed customer" do
        let(:customer) { create(:customer) }
        let(:order) { create(:order, :line_items, customer: customer) }

        it "doesn't transition the order" do
          expect {
            post :pay, params: { order_id: order.to_param }, format: :json
          }.not_to change(order, :status)
        end

        it "responds with 422 and renders a json error" do
          post :pay, params: { order_id: order.to_param }, format: :json
          expect(response).to have_http_status(422)
          expect(json(response)[:code]).to eq('RecordInvalid')
        end
      end

      context "with a confirmed customer, first vip order" do
        let(:customer) { create(:customer, :shipping_address, :confirmed, :card, :vip) }
        let(:order) { create(:order, :line_items, customer: customer) }

        it "starts the customer's recurring order cycle" do
          expect(customer.renew_recur_order_on).to be nil
          expect(order.vip).to be true

          post :pay, params: { order_id: order.to_param }, format: :json

          expect(customer.reload.renew_recur_order_on).to be
        end
      end

      context "with a confirmed customer, non-initial vip order" do
        let(:customer) { create(:customer, :shipping_address, :confirmed, :card, :vip, renew_recur_order_on: 2.weeks.ago.round) }
        let(:order) { create(:order, :line_items, customer: customer) }

        it "bumps next recurring order date (renew_recur_order_on)" do
          expect {
            post :pay, params: { order_id: order.to_param }, format: :json
            customer.reload && order.reload
          }.to change(customer, :renew_recur_order_on)
        end
      end
    end

    describe "POST fulfill" do
      context "with a confirmed customer" do
        let(:customer) { create(:customer, :shipping_address, :confirmed, :card) }

        context "with a fulfilable order" do
          let(:order) { create(:order, :queued, customer: customer) }

          it "assigns the requested order as @order" do
            post :fulfill, params: { order_id: order.to_param }, format: :json
            expect(assigns(:order)).to eq(order)
          end

          it "transitions the order" do
            expect {
              post :fulfill, params: { order_id: order.to_param }, format: :json
              order.reload
            }.to change(order, :status).to('fulfilled')
          end

          it "responds with 200 and renders json" do
            post :fulfill, params: { order_id: order.to_param }, format: :json
            expect(response).to have_http_status(200)
            expect(json(response)[:id]).to eq order.id
          end
        end

        context "with a unfulfilable order" do
          let(:order) { create(:order, :fulfilled) }

          it "assigns the requested order as @order" do
            post :fulfill, params: { order_id: order.to_param }, format: :json
            expect(assigns(:order)).to eq(order)
          end

          it "doesn't transition the order" do
            expect {
              post :fulfill, params: { order_id: order.to_param }, format: :json
            }.not_to change(order, :status)
          end

          it "responds with 422 and renders a json error" do
            post :fulfill, params: { order_id: order.to_param }, format: :json
            expect(response).to have_http_status(422)
            expect(json(response)[:code]).to eq('RecordInvalid')
          end
        end
      end
    end

    describe "POST queue" do
      context "with a confirmed customer" do
        let(:customer) { create(:customer, :shipping_address, :confirmed, :card) }

        context "with a queueable order" do
          let(:order) { create(:order, :paid, customer: customer) }

          it "assigns the requested order as @order" do
            post :queue, params: { order_id: order.to_param }, format: :json
            expect(assigns(:order)).to eq(order)
          end

          it "transitions the order" do
            expect {
              post :queue, params: { order_id: order.to_param }, format: :json
              order.reload
            }.to change(order, :status).to('queued')
          end

          it "responds with 200 and renders json" do
            post :queue, params: { order_id: order.to_param }, format: :json
            expect(response).to have_http_status(200)
            expect(json(response)[:id]).to eq order.id
          end
        end

        context "with a unqueuable order" do
          let(:order) { create(:order, :queued) }

          it "assigns the requested order as @order" do
            post :queue, params: { order_id: order.to_param }, format: :json
            expect(assigns(:order)).to eq(order)
          end

          it "doesn't transition the order" do
            expect {
              post :queue, params: { order_id: order.to_param }, format: :json
            }.not_to change(order, :status)
          end

          it "responds with 422 and renders a json error" do
            post :queue, params: { order_id: order.to_param }, format: :json
            expect(response).to have_http_status(422)
            expect(json(response)[:code]).to eq('RecordInvalid')
          end
        end
      end
    end

    describe "POST unqueue" do
      context "with a confirmed customer" do
        let(:customer) { create(:customer, :shipping_address, :confirmed, :card) }

        context "with a unqueueable order" do
          let(:order) { create(:order, :queued, :line_items, customer: customer) }

          it "assigns the requested order as @order" do
            post :unqueue, params: { order_id: order.to_param }, format: :json
            expect(assigns(:order)).to eq(order)
          end

          it "transitions the order" do
            expect {
              post :unqueue, params: { order_id: order.to_param }, format: :json
              order.reload
            }.to change(order, :status).to('unqueued')
          end

          it "responds with 200 and renders json" do
            post :unqueue, params: { order_id: order.to_param }, format: :json
            expect(response).to have_http_status(200)
            expect(json(response)[:id]).to eq order.id
          end
        end

        context "with an un-unqueueable order" do
          let(:order) { create(:order, :unqueued) }

          it "assigns the requested order as @order" do
            post :unqueue, params: { order_id: order.to_param }, format: :json
            expect(assigns(:order)).to eq(order)
          end

          it "doesn't transition the order" do
            expect {
              post :unqueue, params: { order_id: order.to_param }, format: :json
            }.not_to change(order, :status)
          end

          it "responds with 422 and renders a json error" do
            post :unqueue, params: { order_id: order.to_param }, format: :json
            expect(response).to have_http_status(422)
            expect(json(response)[:code]).to eq('RecordInvalid')
          end
        end
      end
    end

    describe "POST cancel" do
      context "with a confirmed customer" do
        let(:customer) { create(:customer, :shipping_address, :confirmed, :card) }

        context "with a cancelable order" do
          let(:order) { create(:order, :paid, :line_items, customer: customer) }

          it "assigns the requested order as @order" do
            post :cancel, params: { order_id: order.to_param }, format: :json
            expect(assigns(:order)).to eq(order)
          end

          it "transitions the order" do
            expect {
              post :cancel, params: { order_id: order.to_param }, format: :json
              order.reload
            }.to change(order, :status).to('canceled')
          end

          it "responds with 200 and renders json" do
            post :cancel, params: { order_id: order.to_param }, format: :json
            expect(response).to have_http_status(200)
            expect(json(response)[:id]).to eq order.id
          end
        end

        context "with a uncancelable order" do
          let(:order) { create(:order, :canceled) }

          it "assigns the requested order as @order" do
            post :cancel, params: { order_id: order.to_param }, format: :json
            expect(assigns(:order)).to eq(order)
          end

          it "doesn't transition the order" do
            expect {
              post :cancel, params: { order_id: order.to_param }, format: :json
            }.not_to change(order, :status)
          end

          it "responds with 422 and renders a json error" do
            post :cancel, params: { order_id: order.to_param }, format: :json
            expect(response).to have_http_status(422)
            expect(json(response)[:code]).to eq('RecordInvalid')
          end
        end
      end
    end
  end
end
