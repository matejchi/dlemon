require 'rails_helper'

RSpec.describe Api::V1::SessionsController, type: :controller do

  it { should use_before_action(:authenticate) }

  context 'authorized' do
    let(:user) { create(:user) }

    before do
      request.headers['HTTP_AUTHORIZATION'] = "Bearer #{user.token}"
    end

    describe 'GET index' do
      let(:customer) { create(:customer) }
      let!(:session) { setup_session(customer, 1.minute.ago) }

      context 'with valid params' do
        before { get :index, params: params, format: :json }

        context 'with customer_id' do
          let(:params) { { customer_id: customer.id } }

          it 'assigns the requested customer as @customer' do
            expect(assigns(:customer)).to be_a(Customer)
            expect(assigns(:customer)).to eq(customer)
          end

          it 'assigns the requested customer as @customer' do
            expect(assigns(:sessions)).to eq([customer.sessions.last])
          end

          it 'responds with 200 and render template' do
            expect(response).to have_http_status(200)
          end
        end

        context 'without customer_id' do
          let(:params) { {} }

          it 'responds with 200 and render template' do
            expect(response).to have_http_status(200)
          end
        end
      end

      context "with invalid params" do
        it "responds with 404 and renders a json error" do
          get :index, params: { customer_id: 'inexisting' }, format: :json
          expect(response).to have_http_status(404)
          expect(json(response)[:code]).to eq('RecordNotFound')
        end
      end
    end

    describe 'GET show' do
      let(:customer) { create(:customer) }
      let!(:session) { setup_session(customer, 1.minute.ago) }

      context 'with valid params' do
        before { get :show, params: params, format: :json }

        context 'with current session' do
          let(:params) do
            {
              customer_id: customer.id,
              id: 'current'
            }
          end

          it 'assigns the requested customer as @customer' do
            expect(assigns(:customer)).to be_a(Customer)
            expect(assigns(:customer)).to eq(customer)
          end

          it 'assigns the requested session as @session' do
            expect(assigns(:session)).to eq(customer.sessions.order(:created_at).last!)
          end

          it 'responds with 200 and render template' do
            expect(response).to have_http_status(200)
            expect(json(response)[:id]).to eq(customer.sessions.last.id)
          end
        end

        context "with given session" do
          let(:params) do
            {
              customer_id: customer.id,
              id: customer.sessions.last.id
            }
          end

          it 'assigns the requested customer as @customer' do
            expect(assigns(:customer)).to be_a(Customer)
            expect(assigns(:customer)).to eq(customer)
          end

          it 'assigns the requested session as @session' do
            expect(assigns(:session)).to eq(customer.sessions.last)
          end

          it 'responds with 200 and render template' do
            expect(response).to have_http_status(200)
            expect(json(response)[:id]).to eq(customer.sessions.last.id)
          end
        end
      end

      context "with invalid params" do
        let(:params) do
          {
            customer_id: customer.id,
            id: 'inexisting'
          }
        end

        it "responds with 404 and renders a json error" do
          get :show, params: params, format: :json
          expect(response).to have_http_status(404)
          expect(json(response)[:code]).to eq('RecordNotFound')
        end
      end
    end

    describe 'POST update', :pending do
      # TODO
    end
  end
end
