require 'rails_helper'
require 'stripe_mock'

RSpec.describe Api::V1::CustomersController, type: :controller do
  it { should use_before_action(:authenticate) }

  context "authorized" do
    let(:user) { create(:user) }

    before do
      request.headers['HTTP_AUTHORIZATION'] = "Bearer #{user.token}"
    end

    describe "GET show" do
      let(:customer) { create(:customer, :shipping_address, :billing_address, :card) }
      before { get :show, params: { id: customer.to_param }, format: :json }

      it "assigns the requested customer as @customer" do
        expect(assigns(:customer)).to eq(customer)
      end

      it "responds with 200 and render json" do
        expect(response).to have_http_status(200)
        expect(json(response)[:id]).to eq customer.id
      end
    end

    describe "POST create" do
      context "with valid params" do

        it "creates a new record" do
          expect {
            post :create, params: { customer: attributes_for(:customer) }, format: :json
          }.to change(Customer, :count).by(1)
        end

        context do
          let(:params) { { customer: attributes_for(:customer) } }

          it "assigns a newly created and valid record" do
            post :create, params: params, format: :json
            expect(assigns(:customer)).to be_a(Customer)
            expect(assigns(:customer)).to be_valid
          end

          it "creates a Stripe customer" do
            post :create, params: params, format: :json
            expect(CreateStripeCustomerJob).to have_enqueued_sidekiq_job(json(response)[:id])
          end

          it "responds with 201 and renders json" do
            post :create, params: params, format: :json
            expect(response).to have_http_status(201)
            expect(json(response)[:first_name]).to eq params[:customer][:first_name]
            expect(json(response)[:last_name]).to eq params[:customer][:last_name]
          end
        end

        context "with invalid params" do
          it "responds with 422 and renders a json error" do
            post :create, params: {}
            expect(response).to have_http_status(422)
            expect(json(response)[:code]).to eq('ParameterMissing')
          end
        end
      end
    end

    describe "POST confirm" do
      let(:customer) { create(:customer, :shipping_address, :billing_address, :card) }

      context "with valid params" do
        before { customer.generate_confirmation_token! }

        let(:params) do
          {
            id: customer.to_param,
            customer: {
              confirmation_token: customer.confirmation_token
            }
          }
        end

        it "assigns the requested customer as @customer" do
          post :confirm, params: params, format: :json
          expect(assigns(:customer)).to be_a(Customer)
          expect(assigns(:customer)).to eq(customer)
        end

        it "confirms the customer" do
          expect(customer).not_to be_confirmed
          expect(customer.phone_number).to be_blank

          post :confirm, params: params, format: :json
          customer.reload

          expect(customer).to be_confirmed
          expect(customer.phone_number).to be_present
        end

        it "responds with 200 and renders json" do
          post :confirm, params: params, format: :json
          expect(response).to have_http_status(200)
          expect(json(response)[:id]).to eq customer.id
        end
      end

      context "with invalid params" do
        before { customer.generate_confirmation_token! }

        let(:params) do
          {
            id: customer.to_param,
            customer: {
              confirmation_token: 'invalid'
            }
          }
        end

        it "doesn't confirm the customer" do
          post :confirm, params: params, format: :json
          expect(assigns(:customer)).not_to be_confirmed
          expect(assigns(:customer).phone_number).to be_blank
        end

        it "assigns the requested customer as @customer" do
          post :confirm, params: params, format: :json
          expect(assigns(:customer)).to be_a(Customer)
          expect(assigns(:customer)).to eq(customer)
        end

        it "responds with 422 and renders a json error" do
          post :confirm, params: params, format: :json
          expect(response).to have_http_status(422)
          expect(json(response)[:code]).to eq('RecordInvalid')
        end
      end
    end

    describe "POST generate_confirmation" do
      let(:customer) { create(:customer, :shipping_address, :billing_address, :card) }

      context "with valid params" do
        it "assigns the requested customer as @customer" do
          post :generate_confirmation, params: { id: customer.to_param }
          expect(assigns(:customer)).to be_a(Customer)
          expect(assigns(:customer)).to eq(customer)
        end

        it "generates a new confirmation token" do
          expect {
            post :generate_confirmation, params: { id: customer.to_param }
            customer.reload
          }.to change(customer, :confirmation_token)
        end

        it "responds with 201 and renders nothing" do
          post :generate_confirmation, params: { id: customer.to_param }
          expect(response).to have_http_status(201)
          expect(response.body).to be_blank
        end
      end
    end

    describe "GET stats" do

      let!(:customer) { create(:customer, :shipping_address, :billing_address, :confirmed, :card) }

      context "with multiple orders status" do

        let!(:order_created) { create(:order, :line_items, customer: customer) }
        let!(:order_paid) { create(:order, :line_items, :paid, customer: customer) }
        let!(:order_unqueued) { create(:order, :line_items, :unqueued, customer: customer) }
        let!(:order_queued) { create(:order, :line_items, :queued, customer: customer) }
        let!(:order_fulfilled) { create(:order, :line_items, :shipments, :fulfilled, customer: customer) }

        before { order_fulfilled.shipments.update_all(rate: 12.00) }

        it "calculates stats for processed orders" do
          total_lifetime = (order_paid.total + order_unqueued.total + order_queued.total + order_fulfilled.total)

          get :stats, params: { id: customer.id }, format: :json

          expect(json(response)[:total_lifetime].to_d).to eq(total_lifetime)
          expect(json(response)[:avg_shipping_cost].to_d).to eq(12.00)
        end
      end
    end
  end

  context "unauthorized" do
    describe "POST create" do
      it "responds with 401" do
        post :create, params: {}, format: :json
        expect(response).to have_http_status(401)
      end
    end
  end
end
