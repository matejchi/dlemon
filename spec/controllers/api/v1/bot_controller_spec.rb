require 'rails_helper'

RSpec.describe Api::V1::BotController, type: :controller do
  describe 'POST trigger' do
    context 'with valid params' do
      let(:customer) { create(:customer) }
      let(:params) { { trigger: { resource_type: 'Customer', name: 'order.confirmed', resource_id: customer.id } } }

      before do
        allow_any_instance_of(BotpressService).to receive(:trigger)
      end

      it 'responds with 200' do
        post :trigger, params: params, format: :json
        expect(response).to have_http_status(200)
      end
    end

    context 'with invalid params' do
      context 'with unsubscribed customer' do
        let(:customer) { create(:customer, :unsubscribed) }
        let(:params) { { trigger: { resource_type: 'Customer', name: 'order.confirmed', resource_id: customer.id } } }

        it 'responds with 403' do
          post :trigger, params: params, format: :json
          expect(response).to have_http_status(403)
        end
      end

      context 'with invalid resource_type' do
        let(:params) { { trigger: { resource_type: 'bad', name: '1', resource_id: '1' } } }

        it 'responds with 403' do
          post :trigger, params: params, format: :json
          expect(response).to have_http_status(403)
        end
      end

      context 'with not allowed resource_type' do
        let(:params) { { trigger: { resource_type: 'User', name: '1', resource_id: '1' } } }

        it 'responds with 403' do
          post :trigger, params: params, format: :json
          expect(response).to have_http_status(403)
        end
      end

      context 'with invalid name param' do
        let(:params) { { trigger: { resource_type: 'Order', name: 'order.confirmation.bad', resource_id: '1' } } }

        it 'responds with 403' do
          post :trigger, params: params, format: :json
          expect(response).to have_http_status(403)
        end
      end
    end
  end
end
