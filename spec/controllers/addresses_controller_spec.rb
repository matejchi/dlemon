require 'rails_helper'

RSpec.describe AddressesController, type: :controller do
  context 'authorized' do
    describe 'GET edit' do
      let(:address) { create(:shipping_address) }
      before { allow(EasypostService).to receive(:validate_address).and_return(address) }
      before { get :edit, params: { customer_id: address.customer.to_param, id: address.to_param } }

      it 'assigns the requested address as @address' do
        expect(assigns(:address)).to eq(address)
      end

      it 'responds with 200 and render template' do
        expect(response).to have_http_status(200)
        expect(response).to render_template :edit
      end
    end

    describe 'PATCH update' do
      let(:address) { create(:shipping_address) }

      context 'with valid params' do
        let(:address_params) { attributes_for(:address).merge({ street1: '321 Valid Street' }) }
        let(:params) do
          {
            customer_id: address.customer.to_param,
            id: address.to_param,
            address: address_params
          }
        end

        before do
          response_address = address
          response_address.assign_attributes(address_params)
          allow(EasypostService).to receive(:validate_address).and_return(response_address)
        end

        it 'updates the requested address' do
          put :update, params: params
          expect(address.street1).to eq '321 Valid Street'
        end

        it 'assigns the requested address as @address' do
          put :update, params: params
          expect(assigns(:address)).to eq(address)
        end

        it 'responds with 200 and render template' do
          put :update, params: params
          expect(response).to have_http_status(302)
        end
      end

      context 'with invalid params' do
        let(:address) { create(:shipping_address) }
        let(:params) do
          {
            customer_id: address.customer.to_param,
            id: address.to_param,
            address: attributes_for(:invalid_address)
          }
        end

        before do
          address.assign_attributes(attributes_for(:invalid_address))
          allow(EasypostService).to receive(:validate_address).and_return(address)
        end
        before { put :update, params: params }

        it 'assigns the address as @address' do
          expect(assigns(:address)).to eq(address)
        end

        it "re-renders the 'edit' template" do
          expect(response).to render_template(:edit)
        end
      end
    end
  end
end
