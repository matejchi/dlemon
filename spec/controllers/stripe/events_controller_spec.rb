require 'rails_helper'

RSpec.describe Stripe::EventsController, type: :controller do
  describe "POST create" do
    context "charge.refunded event" do
      let(:event) { StripeMock.mock_webhook_event('charge.refunded') }

      it "calls SyncStripeRefundsAction" do
        expect(SyncStripeRefundsAction).to receive(:call)
        post :create, params: event.as_json, format: :json
        expect(response).to have_http_status(200)
      end
    end

    context "charge.refund.updated event" do
      # StripeMock doesn't have this event. Let's fake it:
      #  - Patch the event `type` property
      #  - Patch Stripe::Event.retrieve
      let(:event) do
        StripeMock.mock_webhook_event('charge.refunded').tap do |event|
          event.type = "charge.refund.updated"
        end
      end

      before do
        allow(Stripe::Event).to receive(:retrieve).and_return(event)
      end

      it "captures a message w/ Sentry" do
        expect(Raven).to receive(:capture_message)
        post :create, params: event.as_json, format: :json
        expect(response).to have_http_status(200)
      end
    end

    context "unsupported event" do
      let(:event) { StripeMock.mock_webhook_event('customer.created') }
      it "no-ops with unsupported events type" do
        post :create, params: event.as_json, format: :json
        expect(response).to have_http_status(200)
      end
    end
  end
end
