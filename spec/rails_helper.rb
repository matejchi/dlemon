ENV['RAILS_ENV'] ||= 'test'

require 'spec_helper'
require File.expand_path('../../config/environment', __FILE__)
require 'rspec/rails'

# consider useing
# require 'timecop'

# consider migrating
require 'shoulda/matchers'

# Requires supporting ruby files with custom matchers and macros, etc,
# in spec/support/ and its subdirectories.
Dir["./spec/support/**/*.rb"].sort.each { |f| require f }
#
# Add additional requires below this line. Rails is not loaded until this point!
#
# Checks for pending migrations before tests are run.
ActiveRecord::Migration.maintain_test_schema!

RSpec.configure do |config|
  config.use_transactional_fixtures = true
  config.fixture_path = "#{::Rails.root}/spec/fixtures"

  config.infer_spec_type_from_file_location!

  # Filter lines from Rails gems in backtraces.
  config.filter_rails_from_backtrace!
  # arbitrary gems may also be filtered via:
  # config.filter_gems_from_backtrace("gem name")

  # Usage of visit and root_path in specs (original, cleanup req)
  config.include Rails.application.routes.url_helpers
  config.include Devise::Test::ControllerHelpers, type: :controller
  config.include ControllerMacros, type: :controller
  config.include Helpers # FIXME, merge into others

  # Configuration for Shoulda Gem. Used to complement tests.
  Shoulda::Matchers.configure do |sconfig|
    sconfig.integrate do |with|
      with.test_framework :rspec
      with.library :rails
    end
  end

  config.before(:suite) do
    # prep factory_run tracking
    FactoryBot::Analytics.track_factories

    # Rails seed?
    Rails.application.load_seed
  end

  config.after(:suite) do
    FactoryBot::Analytics.print_statistics
  end

  # should be able to fix via vcr..
  config.before(:each) do
    # Always allow Twilio phone number validation to pass
    allow_any_instance_of(TwilioValidator).to receive(:validate)
  end

end

