require 'rails_helper'

RSpec.describe Batch, type: :model do
  describe 'Factories' do
    context 'Valid factory' do
      subject { create(:batch) }
      specify { should be_valid }
    end

    context 'Invalid factory' do
      subject { build(:invalid_batch) }
      specify { is_expected.not_to be_valid }
    end
  end

  describe 'Associations' do
    it { should have_many(:shipments) }
    it { should have_many(:orders) }
    it { should belong_to(:fulfillment_address) }
  end

  describe 'Validations' do
    it { should validate_presence_of(:name) }
    it { should validate_presence_of(:enqueued_at) }
  end

  describe 'Callbacks' do
  end

  describe 'ClassMethods' do
  end

  describe 'InstanceMethods' do
  end
end
