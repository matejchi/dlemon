require 'rails_helper'

RSpec.describe Api::Address do
  describe 'Validations' do
    it { should validate_presence_of(:street1) }
    it { should validate_presence_of(:city) }
    it { should validate_presence_of(:state) }
    it { should validate_presence_of(:country) }
    it { should validate_presence_of(:zip) }
    it { should allow_value('95 Grand St').for(:street1) }
    it { should allow_value('95 Grand St').for(:street2) }
    it { should_not allow_value('PO Box 123').for(:street1) }
    it { should_not allow_value('PO Box 123').for(:street2) }
  end

  describe 'ClassMethods' do
  end

  describe 'InstanceMethods' do
  end
end
