require 'rails_helper'

RSpec.describe Session, type: :model do
  describe 'Factories' do
    context 'Valid factory' do
      subject { build(:session) }
      specify { should be_valid }
    end

    context 'Invalid factory' do
      subject { build(:invalid_session) }
      specify { is_expected.not_to be_valid }
    end
  end

  describe 'Associations' do
    it { should have_many(:events) }
  end

  describe 'Validations' do
  end

  describe 'Callbacks' do
    describe '#set_interactive_flag' do
      let(:customer) { create(:customer) }

      def create_session(sender_roles)
        create(:session, customer: customer, sender_roles: sender_roles)
      end

      it 'sets interactive to false when automated messages' do
        expect(create_session(%w(notification)).interactive).to be false
        expect(create_session(%w(outreach)).interactive).to be false
        expect(create_session(%w(outreach notification)).interactive).to be false
      end

      it 'set interactive to true when has interaction' do
        expect(create_session(%w(agent)).interactive).to be true
        expect(create_session(%w(notification user)).interactive).to be true
        expect(create_session(%w(outreach notification bot)).interactive).to be true
      end
    end
  end

  describe 'ClassMethods' do
    let(:customer) { create(:customer) }

    describe ".current" do
      it "returns session when active in the last 2 hours" do
        setup_session(customer, 10.hours.ago)
        new_session = setup_session(customer)
        expect(Session.current(customer)).to eql(new_session)
      end

      it "returns nil when inactive for more than 2 hours" do
        setup_session(customer, 125.minutes.ago)
        expect(Session.current(customer)).to be nil
      end
    end

    describe ".latest!" do
      it "gets the current session if it exists" do
        session = setup_session(customer)
        expect(Session.latest!(customer)).to eql(session)
      end

      it "creates a new session gets the current session if it exists" do
        old_session = setup_session(customer, 3.hours.ago)
        new_session = Session.latest!(customer)
        expect(new_session).not_to eql(old_session)
        expect(new_session).to be_a(Session)
        expect(new_session.customer).to eql(customer)
      end
    end
  end

  describe 'InstanceMethods' do
  end
end
