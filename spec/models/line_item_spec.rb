require 'rails_helper'

RSpec.describe LineItem, type: :model do
  describe 'Factories' do
    context 'Valid factory' do
      subject { create(:line_item) }
      specify { should be_valid }
    end

    context 'Invalid factory' do
      subject { build(:invalid_line_item) }
      specify { is_expected.not_to be_valid }
    end
  end

  describe 'Associations' do
    it { should belong_to(:order) }
    it { should belong_to(:sku) }
  end

  describe 'Validations' do
    it { should validate_presence_of(:description) }
    it { should validate_presence_of(:quantity) }
    it { should validate_presence_of(:amount) }
    it { should validate_numericality_of(:quantity).is_greater_than(0).only_integer }
    it { should validate_numericality_of(:amount).is_greater_than(0) }
  end

  describe 'Callbacks' do
  end

  describe 'ClassMethods' do
  end

  describe 'InstanceMethods' do
  end
end
