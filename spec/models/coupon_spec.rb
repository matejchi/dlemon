require 'rails_helper'

RSpec.describe Coupon, type: :model do
  describe 'Factories' do
    context 'Valid factory' do
      subject { create(:coupon) }
      specify { should be_valid }
    end

    context 'Invalid factory' do
      subject { build(:invalid_coupon) }
      specify { is_expected.not_to be_valid }
    end
  end

  describe 'Associations' do
    it { should have_many(:orders).through(:order_coupons) }
  end

  describe 'Validations' do
    it { should validate_presence_of(:code) }

    context 'the same code' do
      let!(:coupon1) { create(:coupon, code: '324rgbER') }
      let(:coupon2) { build(:coupon, code: '324rgbER') }

      it "raises unique validation error" do
        expect(coupon2).not_to be_valid
      end
    end
  end

  describe 'Callbacks' do
  end

  describe 'ClassMethods' do
  end

  describe 'InstanceMethods' do
    describe "#compute" do
      context "with amount_off coupon" do
        subject { Coupon.new(amount_off: 20.00) }
        it "subtracts amount from the total" do
          expect(subject.compute(100.00)).to eql(20.00)
        end

        it "can't be greater than the passed in amount" do
          expect(subject.compute(10.00)).to eql(10.00)
        end
      end

      context "with percent_off coupon" do
        context "of 100%" do
          subject { Coupon.new(percent_off: 100) }
          it "returns the input amount" do
            expect(subject.compute(10.00)).to eql(10.00)
          end
        end

        context "of 33%" do
          subject { Coupon.new(percent_off: 33) }
          it "rounds the discount" do
            expect(subject.compute(5.50)).to eql(1.82) # 1.815 rounded
          end
        end
      end
    end
  end
end
