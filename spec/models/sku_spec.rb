require 'rails_helper'

RSpec.describe Sku, type: :model do
  describe 'Factories' do
    context 'Valid factory' do
      subject { create(:sku) }
      specify { should be_valid }
    end

    context 'Invalid factory' do
      subject { build(:invalid_sku) }
      specify { is_expected.not_to be_valid }
    end
  end

  describe 'Associations' do
    it { should belong_to(:product) }
  end

  describe 'Validations' do
    it { should validate_presence_of(:stripe_id) }
    it { should validate_presence_of(:currency) }
    it { should validate_presence_of(:price) }
    it { should validate_uniqueness_of(:stripe_id).case_insensitive }
  end

  describe 'Callbacks' do
  end

  describe 'ClassMethods' do
  end

  describe 'InstanceMethods' do
  end
end
