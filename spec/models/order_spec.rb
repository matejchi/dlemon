require 'rails_helper'

RSpec.describe Order, type: :model do

  describe 'Factories' do
    context 'Valid factory' do
      subject { create(:order, :paid) }
      specify { should be_valid }
    end

    context 'Invalid factory' do
      subject { build(:invalid_order) }
      specify { is_expected.not_to be_valid }
    end
  end

  describe 'Associations' do
    it { should belong_to(:customer) }
    it { should have_many(:coupons).through(:order_coupons) }
    it { should belong_to(:batch) }
    it { should have_many(:line_items).dependent(:destroy) }
    it { should have_many(:shipments).dependent(:destroy) }
    it { should have_many(:transitions) }
    it { should have_many(:events) }
  end

  describe 'Validations' do
    it { should validate_presence_of(:currency) }
    # it { should validate_presence_of(:reference) } # this is created before_validation
    # it { should validate_uniqueness_of(:reference) }
  end

  describe 'Callbacks' do
  end

  describe 'ClassMethods' do
  end

  describe 'InstanceMethods' do
  end

  describe 'Transitions' do
    it 'is in state created by default' do
      expect(build(:order).status).to eq 'created'
    end
  end
end
