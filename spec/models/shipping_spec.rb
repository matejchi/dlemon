require 'rails_helper'

RSpec.describe Shipping, type: :model do
  describe 'Factories' do
    context 'Valid factory' do
      subject { build(:shipping) }
      specify { should be_valid }
    end

    context 'Invalid factory' do
      subject { build(:invalid_shipping) }
      specify { is_expected.not_to be_valid }
    end
  end

  describe 'Associations' do
  end

  describe 'Validations' do
    it { should validate_presence_of(:name) }
    it { should validate_presence_of(:street1) }
    it { should validate_presence_of(:city) }
    it { should validate_presence_of(:state) }
    it { should validate_presence_of(:country) }
    it { should validate_presence_of(:zip) }
  end

  describe 'Callbacks' do
  end

  describe 'ClassMethods' do
    let(:shipping_address_attrs) do
      {
        street1: "1919 8th Street",
        street2: "#810",
        city: "Seattle",
        zip: "12345",
        state: "WA",
        country: "US"
      }
    end
    
    let(:customer) do
      build(:customer, :phone_number,
            shipping_address: build(:shipping_address, shipping_address_attrs))
    end

    describe ".from_customer" do
      it "uses all fields from customer to build an adress" do
        shipping = Shipping.from_customer(customer)
        expect(shipping.name).to eql(customer.full_name)
        expect(shipping.phone_number).to eql(customer.phone_number)
        expect(shipping.email).to eql(customer.email)
        shipping_address_attrs.each do |attr, expected|
          actual = shipping.send(attr)
          expect(actual).to eql(expected), "#{attr} not copied, #{actual.inspect} != #{expected.inspect}"
        end
      end

      it "allows overriding all fields" do
        overrides = {
          name: "1",
          phone_number: "2",
          email: "3",
          street1: "A",
          street2: "B",
          city: "C",
          zip: "D",
          state: "E",
          country: "F"
        }
        shipping = Shipping.from_customer(customer, overrides)

        overrides.each do |attr, expected|
          actual = shipping.send(attr)
          expect(actual).to eql(expected), "#{attr} not overrided, #{actual.inspect} != #{expected.inspect}"
        end
      end
    end
  end

  describe 'InstanceMethods' do
  end
end
