require 'rails_helper'

RSpec.describe Message, type: :model do
  describe 'Factories' do
    context 'Valid factory' do
      subject { create(:message) }
      specify { should be_valid }
    end

    context 'Invalid factory' do
      subject { build(:invalid_message) }
      specify { is_expected.not_to be_valid }
    end
  end

  describe 'Associations' do
  end

  describe 'Validations' do
    it { should validate_presence_of(:content) }
    it { should validate_presence_of(:content_type) }
    it { should validate_presence_of(:sender_role) }
    it { should validate_presence_of(:sent_at) }
    it { should validate_inclusion_of(:content_type).in_array(['text']) }
    it { should validate_inclusion_of(:sender_role).in_array([
      'end-user', 'agent', 'bot', 'notification', 'outreach']) }
    it { should have_many(:events) }
  end

  describe 'Callbacks' do
    describe "sync_session_sender_roles" do
      let(:session) { create(:message, sender_role: "end-user").session }
      it "adds new sender role to the end" do
        expect(session.sender_roles).to eql(["end-user"])
        session.messages << build(:message, sender_role: "bot")
        expect(session.sender_roles).to eql(["end-user", "bot"])
      end

      it "does not duplicate sender roles" do
        session.messages << build(:message, sender_role: "end-user")
        expect(session.sender_roles).to eql(["end-user"])
      end

      it "doesn't save twice if nothing changed" do
        expect(session).not_to receive(:save)
        session.messages << build(:message, sender_role: "end-user")
      end
    end
  end

  describe 'ClassMethods' do
  end

  describe 'InstanceMethods' do
  end
end
