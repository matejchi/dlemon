require 'rails_helper'

RSpec.describe Address, type: :model do
  describe 'Factories' do
    context 'Valid factory' do
      subject { create(:address) }
      specify { should be_valid }
    end

    context 'Invalid factory' do
      subject { build(:invalid_address) }
      specify { is_expected.not_to be_valid }
    end
  end

  describe 'Associations' do
    it { should belong_to(:customer) }
  end

  describe 'Validations' do
    it { should validate_presence_of(:street1) }
    it { should validate_presence_of(:city) }
    it { should validate_presence_of(:state) }
    it { should validate_presence_of(:country) }
    it { should validate_presence_of(:zip) }
  end

  describe 'Callbacks' do
  end

  describe 'ClassMethods' do
  end

  describe 'InstanceMethods' do
  end
end
