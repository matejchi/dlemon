require 'rails_helper'

RSpec.describe Shipment, type: :model do
  describe 'Factories' do
    context 'Valid factory' do
      subject { create(:shipment) }
      specify { should be_valid }
    end

    context 'Invalid factory' do
      subject { build(:invalid_shipment) }
      specify { is_expected.not_to be_valid }
    end
  end

  describe 'Associations' do
    it { should belong_to(:order) }
    it { should belong_to(:sku) }
    it { should belong_to(:batch) }
    it { should belong_to(:fulfillment_address) }
    it { should have_one(:customer).through(:order) }
    it { should have_many(:events) }
  end

  describe 'Validations' do
  end

  describe 'Callbacks' do
  end

  describe 'ClassMethods' do
  end

  describe 'InstanceMethods' do
  end
end
