require 'rails_helper'

RSpec.describe FulfillmentAddress, type: :model do
  describe 'Factories' do
    context 'Valid factory' do
      subject { create(:fulfillment_address) }
      specify { should be_valid }
    end

    context 'Invalid factory' do
      subject { build(:invalid_fulfillment_address) }
      specify { is_expected.not_to be_valid }
    end
  end

  describe 'Associations' do
    it { should have_many(:shipments) }
  end

  describe 'Validations' do
    it { should validate_presence_of(:street1) }
    it { should validate_presence_of(:city) }
    it { should validate_presence_of(:state) }
    it { should validate_presence_of(:country) }
    it { should validate_presence_of(:zip) }
    it { should validate_inclusion_of(:carrier_facility).in_array(['njfs', 'spwg']) }
  end

  describe 'Callbacks' do
  end

  describe 'ClassMethods' do
  end

  describe 'InstanceMethods' do
  end
end
