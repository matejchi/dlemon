require 'rails_helper'

RSpec.describe Customer, type: :model do
  describe 'Factories' do
    context 'Valid factory' do
      subject { create(:customer) }
      specify { should be_valid }
    end

    context 'Invalid factory' do
      subject { build(:invalid_customer) }
      specify { is_expected.not_to be_valid }
    end
  end

  describe 'Associations' do
    subject { create(:customer, :shipping_address, :billing_address, :with_addresses) }

    it { should have_many(:orders).dependent(:destroy) }
    it { should have_many(:addresses).dependent(:destroy) }
    it { should have_many(:messages) }
    it { should have_one(:shipping_address) }
    it { should have_one(:billing_address) }
  end

  describe 'Validations' do
    it { should validate_inclusion_of(:country_code).in_array(ISO3166::Country.codes) }

    describe 'with a confirmed customer' do
      subject { create(:customer, :confirmed) }

      it { should validate_presence_of(:phone_number) }
    end
  end

  describe 'Callbacks' do
  end

  describe 'ClassMethods' do
  end

  describe 'InstanceMethods' do
    describe "tags=" do
      it "downcases all tags" do
        customer = Customer.new(tags: ["VIP", "VVIP"])
        expect(customer.tags).to eql(%w(vip vvip))
      end

      it "strips duplicate tags" do
        customer = Customer.new(tags: ["VIP", "vip"])
        expect(customer.tags).to eql(%w(vip))
      end

      it "strips empty tags" do
        customer = Customer.new(tags: ["vip", " ", ""])
        expect(customer.tags).to eql(%w(vip))
      end

      it "strips whitespace" do
        customer = Customer.new(tags: ["vip "])
        expect(customer.tags).to eql(%w(vip))
      end

      it "doesn't crash with non-enumerable values" do
        customer = Customer.new(tags: 121)
        expect(customer.tags.to_s).to eql("121")
      end
    end
  end
end
