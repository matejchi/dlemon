require 'rails_helper'

RSpec.describe Outreach, type: :model do
  describe 'Associations' do
    it { should belong_to(:customer) }
  end

  let(:campaign) { "NEW_MATCHA" }

  describe 'ClassMethods' do
    def create_customer(zip: nil, confirmed: true, attributes: {}, address_attributes: nil)
      traits = []
      traits << :confirmed if confirmed
      create(:customer, *traits, **attributes).tap do |customer|
        attrs = { country: "US", customer: customer }.merge(address_attributes || {})
        attrs[:zip] = zip if zip
        customer.shipping_address = create(:shipping_address, attrs)
      end
    end

    def create_campaign(customer: nil, phone_number: nil)
      customer ||= create(:customer, :confirmed)
      create(:outreach,
             campaign_name: campaign,
             customer: customer,
             phone_number: phone_number || customer.phone_number)
    end

    describe "spawn_new_campaign" do
      it "creates Outreach for passed in zip, once" do
        ['12000', '12100', '13000', '00000'].each do |zip|
          create_customer(zip: zip)
        end

        Outreach.spawn_new_campaign(campaign: campaign, zip: ['12', '13000'])

        expect(Outreach.all.count).to eql(3)
        all_outreaches = Outreach.all.includes(customer: :shipping_address).to_a
        outreach_zips = all_outreaches.map { |outreach| outreach.customer.shipping_address.zip }
        expect(outreach_zips.to_set).to eql(['12000', '12100', '13000'].to_set)

        # Calling a second time doesn't change the result
        Outreach.spawn_new_campaign(campaign: campaign, zip: ['12', '13000'])
        expect(Outreach.all.count).to eql(3)
      end

      it "accepts a block to filter query" do
        create_customer(zip: '12345')
        block_called = false
        Outreach.spawn_new_campaign(campaign: campaign) do |query|
          block_called = true
          query.none
        end

        expect(block_called).to eql(true)
        expect(Outreach.all.count).to eql(0)
      end

      it "processes unconfirmed phone numbers if specified" do
        customer = create_customer(confirmed: false)

        Outreach.spawn_new_campaign(campaign: campaign)

        all_outreach = Outreach.all.to_a
        expect(all_outreach.size).to eql(1)
        expect(all_outreach.first.phone_number).to eql(customer.unconfirmed_phone_number)
      end

      it "never sends to the same phone number twice" do
        phone = "+15555555555"
        create_customer(confirmed: false, attributes: { unconfirmed_phone_number: phone })
        confirmed_customer = create_customer(attributes: { phone_number: phone })
        Outreach.spawn_new_campaign(campaign: campaign)

        all_outreaches = Outreach.all.to_a
        expect(all_outreaches.size).to eql(1)
        expect(all_outreaches.first.customer_id).to eql(confirmed_customer.id)
      end
    end

    describe "generate_schedule" do
      before do
        # Jan 1st of next year in the middle of the day (not DND time anywhere in America)
        jan_1st = Time.new(Time.current.year + 1, 1, 1).change(hour: 12, min: 0, sec: 0)
        jan_1st_est = ActiveSupport::TimeZone.new('America/New_York').local_to_utc(jan_1st)
        Timecop.freeze(jan_1st_est)
      end

      after do
        Timecop.return
      end


      it "schedules messages to go out outside of their dnd period" do
        ny_customer = create_customer(attributes: { time_zone: "America/New_York" })
        la_customer = create_customer(attributes: { time_zone: "America/Los_Angeles" })
        [ny_customer, la_customer].each do |customer|
          create(:outreach, customer: customer, campaign_name: campaign)
        end

        at_night = Time.current.in_time_zone("America/New_York").change(hour: 4) + 1.day

        events = Outreach.generate_schedule(campaign: campaign, count: 2, rate: 10, starting_at: at_night)
        events.sort_by!(&:first)

        expect(events.size).to eql(2)

        ny_event, la_event = events

        expect(ny_event[0]).to eql(at_night.change(hour: 8))
        expect(ny_event[1].customer_id).to eql(ny_customer.id)

        expect(la_event[0]).to eql(at_night.in_time_zone("America/Los_Angeles").change(hour: 8))
        expect(la_event[1].customer_id).to eql(la_customer.id)
      end

      it "schedules messages over multiple days if need be" do
        3.times do
          create(:outreach,
                 campaign_name: campaign,
                 customer: create_customer(attributes: { time_zone: "America/New_York" }))
        end

        end_of_day = Time.current.in_time_zone("America/New_York").change(hour: 21)
        events = Outreach.generate_schedule(campaign: campaign, count: 3, rate: 1, starting_at: end_of_day)

        sorted_ny_times = events.map(&:first).sort.map do |time|
          time.in_time_zone("America/New_York").strftime("%m-%d %H:%M")
        end

        expect(sorted_ny_times).to eql(["01-01 21:00", "01-02 08:00", "01-02 09:00"])
      end

      it "schedules at most `limit` jobs" do
        2.times { create_campaign }

        events = Outreach.generate_schedule(campaign: campaign, count: 1, rate: 1)
        expect(events.size).to eql(1)
      end

      it "spreads jobs over the hour" do
        2.times { create_campaign }

        starting_at = Time.current.change(min: 0, sec: 0)
        events = Outreach.generate_schedule(campaign: campaign, count: 2, rate: 2, starting_at: starting_at)
        minutes = events.map { |e| e.first.min }
        expect(minutes).to eql([0, 30])
      end

      it "schedules job from `starting_at` even if in the middle of the hour" do
        2.times { create_campaign }

        starting_at = Time.current.change(min: 30, sec: 0)
        events = Outreach.generate_schedule(campaign: campaign, count: 2, rate: 2, starting_at: starting_at)
        minutes = events.map { |e| e.first.min }
        expect(minutes).to eql([30, 0])
      end
    end

    describe "schedule" do
      it "glues generate_schedule and SendOutreachTextJob together" do
        create(:outreach, campaign_name: campaign, customer: create(:customer, :confirmed))
        expect(Outreach).to receive(:generate_schedule).and_call_original
        expect(SendOutreachTextJob).to receive(:perform_at)

        Outreach.schedule(campaign: campaign, count: 2, rate: 2)
      end
    end
  end
end

