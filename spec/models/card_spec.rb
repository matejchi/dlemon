require 'rails_helper'

RSpec.describe Card, type: :model do
  describe 'Factories' do
    context 'Valid factory' do
      subject { build(:card) }
      specify { should be_valid }
    end

    context 'Invalid factory' do
      subject { build(:invalid_card) }
      specify { is_expected.not_to be_valid }
    end
  end

  describe 'Associations' do
    it { should have_many(:events) }
  end

  describe 'Validations' do
    it { should validate_presence_of(:stripe_id) }
    it { should validate_presence_of(:exp_month) }
    it { should validate_presence_of(:exp_year) }
    it { should validate_presence_of(:last4) }
    it { should validate_length_of(:exp_month).is_at_least(1).is_at_most(2) }
    it { should validate_length_of(:exp_year).is_equal_to(4) }
    it { should validate_length_of(:last4).is_equal_to(4) }
  end
end
