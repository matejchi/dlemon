require 'rails_helper'

RSpec.describe OrderTransition, type: :model do
  describe 'Factories' do
    context 'Valid factory' do
      subject { build(:order_transition) }
      specify { should be_valid }
    end

    context 'Invalid factory' do
      # subject { build(:invalid_order_transition) }
      # specify { is_expected.not_to be_valid }
    end
  end

  describe 'Associations' do
  end

  describe 'Validations' do
  end

  describe 'Callbacks' do
  end

  describe 'ClassMethods' do
  end

  describe 'InstanceMethods' do
  end
end
