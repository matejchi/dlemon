require 'rails_helper'

RSpec.describe S3Service  do
  let(:service) { S3Service.new }
  let(:options) { { example: 'example' } }

  before do
    stub_request(:get, 'http://169.254.169.254/latest/meta-data/iam/security-credentials/').
      to_return(status: 200, body: '')

    allow_any_instance_of(Aws::S3::Client).to receive(:delete_object).and_return(true)
  end

  describe '#delete_object' do
    it 'calls request' do
      expect_any_instance_of(Aws::S3::Client).to receive(:delete_object).with(options)
      service.delete_object(options)
    end

    it 'returns a boolean' do
      service.delete_object(options)
      expect(service.delete_object({})).to be_truthy
    end
  end
end
