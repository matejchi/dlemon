require 'rails_helper'

RSpec.describe BotpressService  do
  let(:service) { BotpressService.new }
  let(:data) { { example: 'example' } }

  describe '#create_event' do
    it 'calls request' do
      expect_any_instance_of(BotpressService).to receive(:request).with(:post, 'test', 'botpress-dirtylemon/webhook', body: data)
      service.create_event(data)
    end
  end

  describe '#trigger' do
    let(:customer) { create(:customer, :confirmed) }
    let(:payload) do
      {
        name: 'name',
        customer_id: customer.id,
        data: data
      }
    end

    it 'calls request' do
      expect_any_instance_of(BotpressService).to receive(:request).with(:post, 'test', 'botpress-dirtylemon/trigger', body: payload)
      service.trigger('name', customer, data)
    end
  end
end
