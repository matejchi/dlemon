require 'rails_helper'

RSpec.describe EasypostService do
  let(:service) { EasypostService.new }

  describe "purchase_shipping_label" do
    let(:shipment) { create(:shipment) }
    let(:ep_shipment) { double('ep_shipment') }

    before do
      allow(ep_shipment).to receive(:buy).and_return(ep_shipment)
    end

    it "calls Easypost API" do
      expect(ep_shipment).to receive(:buy)
      service.purchase_shipping_label(shipment, ep_shipment, 3)
    end

    it "returns the Easypost shipment" do
      expect(service.purchase_shipping_label(shipment, ep_shipment, 3)).to be(ep_shipment)
    end
  end

  describe "refund_shipping_label" do
    let(:shipment) { create(:shipment) }
    let(:ep_shipment) { double('ep_shipment', refund: true) }

    before do
      allow(EasyPost::Shipment).to receive(:retrieve) { ep_shipment }
    end

    it "calls Easypost API" do
      expect(ep_shipment).to receive(:refund)
      service.refund_shipping_label(shipment)
    end

    it "returns the Easypost shipment" do
      expect(service.refund_shipping_label(shipment)).to be(ep_shipment)
    end
  end

  describe "create_shipment" do
    context "with a valid shipment" do
      let(:order) { create(:order, shipping: shipping)}
      let(:shipment) { create(:shipment, order: order) }
      let(:fulfillment_address) { create(:fulfillment_address) }
      let(:response) { double(:shipment, messages: []) }

      before { allow(EasyPost::Shipment).to receive(:create).and_return(response) }

      context "with a US shipping label" do
        let(:shipping) do
          build(:shipping, {
            name: "David Chang",
            phone_number: "+1-123-456-7890",
            street1: "128 Lafayette St",
            city: "New York",
            state: "New York",
            country: "US",
            zip: "10013"
          })
        end

        it "creates an Easypost shipment" do
          expect(EasyPost::Shipment).to receive(:create).and_return(response)
          service.create_shipment(shipping, fulfillment_address, [], { fulfill_at: Time.now })
        end
      end

      context "with an international shipping address" do
        let(:shipping) do
          build(:shipping, {
            name: "Philippe Dionne",
            phone_number: "+1-123-456-7890",
            street1: "60 Cremazie Ouest",
            city: "Quebec",
            state: "Quebec",
            country: "CA",
            zip: "G1R 1X3"
          })
        end

        before do
          allow(EasyPost::CustomsItem).to receive(:create).and_return(nil)
          allow(EasyPost::CustomsInfo).to receive(:create).and_return(nil)
        end

        it "creates an Easypost customs item" do
          expect(EasyPost::CustomsItem).to receive(:create).and_return(nil)
          service.create_shipment(shipping, fulfillment_address, [], { fulfill_at: Time.now })
        end

        it "creates an Easypost customs info" do
          expect(EasyPost::CustomsInfo).to receive(:create).and_return(nil)
          service.create_shipment(shipping, fulfillment_address, [], { fulfill_at: Time.now })
        end

        it "creates an Easypost shipment" do
          expect(EasyPost::Shipment).to receive(:create).and_return(response)
          service.create_shipment(shipping, fulfillment_address, [], { fulfill_at: Time.now })
        end
      end
    end
  end

  describe "validate_address", pending: true do
  end
end
