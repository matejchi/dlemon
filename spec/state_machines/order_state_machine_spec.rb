require 'rails_helper'

RSpec.describe 'OrderStateMachine' do
  let(:state_machine) { order.state_machine }

  describe 'Transitions'do
    context 'from created state' do
      context 'with confirmed customer' do
        let(:customer) { create(:customer, :confirmed, :card) }
        let(:order) { create(:order, customer: customer) }

        it "can transition to scheduled with scheduled_confirm_on" do
          order.update_attributes!(scheduled_confirm_on: Time.current + 2.days)
          expect { state_machine.transition_to!(:scheduled) }.not_to raise_error
        end

        it "can transition to state paid" do
          expect { state_machine.transition_to!(:paid) }.not_to raise_error
          expect(order.transactions.size).to eql(1)

          charge_transaction = order.transactions.first
          expect(charge_transaction).to be_a(ChargeTransaction)
          expect(charge_transaction.amount).to eql(order.total)
          expect(charge_transaction.stripe_id).to be
        end

        it "can transition to state paid and calls Stripe services" do
          expect(Stripe::Charge).to receive(:create).and_call_original
          state_machine.transition_to!(:paid)
        end

        it "can transition to state paid and calls OrderMailer" do
          expect(OrderMailer).to receive(:confirmation).and_return(OrderMailer.confirmation(order))
          state_machine.transition_to!(:paid)
        end

        context "with vip customer" do
          before { customer.update_attributes(tags: ["vip", Customer::VIP_FAILED_CARD_TAG]) }
          it "can transition to paid, updates the next recur date, clears card failed tag" do
            state_machine.transition_to!(:paid)
            customer.reload

            expect(customer.renew_recur_order_on).to be
            expect(customer.renew_recur_order_on).to be > (Time.current + 2.week)
            expect(customer.tags).not_to include(Customer::VIP_FAILED_CARD_TAG)
          end
        end

        context "with failing card" do
          before { StripeMock.prepare_card_error(:card_declined) }
          it "raises ActiveRecord::RecordInvalid and triggers order.charge-failed on the bot" do
            expect(BotpressTriggerJob).to receive(:perform_async).with('order.charge-failed', customer.id, anything)
            expect(-> { state_machine.transition_to!(:paid) }).to raise_error(ActiveRecord::RecordInvalid)
          end
        end
      end

      context 'with unconfirmed customer' do
        let(:order) { create(:order) }

        it "cannot transition to state paid" do
          expect { state_machine.transition_to!(:paid) }.to raise_error(Statesman::GuardFailedError)
        end

        context "with scheduled_confirm_on" do
          let(:order) { create(:order, scheduled_confirm_on: Time.current + 2.days) }

          it "cannot transition to state scheduled" do
            expect { state_machine.transition_to!(:scheduled) }.to raise_error(Statesman::GuardFailedError)
          end
        end
      end

      context do
        let(:order) { create(:order) }

        it "can transition to state canceled" do
          expect { state_machine.transition_to!(:canceled) }.not_to raise_error
        end

        it "cannot transition to state queued" do
          expect { state_machine.transition_to!(:queued) }.to raise_error(Statesman::TransitionFailedError)
        end

        it "cannot transition to state fulfilled" do
          expect { state_machine.transition_to!(:fulfilled) }.to raise_error(Statesman::TransitionFailedError)
        end
      end
    end

    context 'from scheduled state' do
      let(:order) { create(:order, :scheduled) }

      it "can transition to state paid" do
        expect { state_machine.transition_to!(:paid) }.not_to raise_error
        expect(order.scheduled_confirm_on).to be nil
      end

      it "can transition to state canceled" do
        expect { state_machine.transition_to!(:canceled) }.not_to raise_error
        expect(order.scheduled_confirm_on).to be nil
      end

      it "can transition to state created" do
        expect { state_machine.transition_to!(:created) }.not_to raise_error
        expect(order.scheduled_confirm_on).to be nil
      end
    end


    context 'from paid state' do
      let(:order) { create(:order, :paid) }

      it "can transition to state queued" do
        expect { state_machine.transition_to!(:queued) }.not_to raise_error
      end

      it "can transition to state canceled" do
        expect { state_machine.transition_to!(:canceled) }.not_to raise_error
      end

      it "can transition to state canceled and triggers a RefundOrderJob" do
        expect { state_machine.transition_to!(:canceled) }.to change(RefundOrderJob.jobs, :size).by(1)
      end

      it "cannot transition to state created" do
        expect { state_machine.transition_to!(:created) }.to raise_error(Statesman::TransitionFailedError)
      end

      it "cannot transition to state fullfield" do
        expect { state_machine.transition_to!(:fulfilled) }.to raise_error(Statesman::TransitionFailedError)
      end

      it "cannot transition to state returned" do
        expect { state_machine.transition_to!(:returned) }.to raise_error(Statesman::TransitionFailedError)
      end
    end

    context 'from queued state' do
      let(:order) { create(:order, :queued) }

      it "can transition to state fulfilled" do
        expect { state_machine.transition_to!(:fulfilled) }.not_to raise_error
      end

      it "can transition to state unqueued" do
        expect { state_machine.transition_to!(:unqueued) }.not_to raise_error
      end

      it "cannot transition to state created" do
        expect { build(:order, :queued).state_machine.transition_to!(:created) }.to raise_error(Statesman::TransitionFailedError)
      end

      it "cannot transition to state paid" do
        expect { state_machine.transition_to!(:paid) }.to raise_error(Statesman::TransitionFailedError)
      end

      it "cannot transition to state canceled" do
        expect { state_machine.transition_to!(:canceled) }.to raise_error(Statesman::TransitionFailedError)
      end

      it "cannot transition to state returned" do
        expect { state_machine.transition_to!(:returned) }.to raise_error(Statesman::TransitionFailedError)
      end
    end

    context 'from unqueued state' do
      let(:order) { create(:order, :unqueued) }

      it "can transition to state queued" do
        expect { state_machine.transition_to!(:queued) }.not_to raise_error
      end

      it "can transition to state canceled" do
        expect { state_machine.transition_to!(:canceled) }.not_to raise_error
      end

      it "cannot transition to state created" do
        expect { state_machine.transition_to!(:created) }.to raise_error(Statesman::TransitionFailedError)
      end

      it "cannot transition to state paid" do
        expect { state_machine.transition_to!(:paid) }.to raise_error(Statesman::TransitionFailedError)
      end

      it "cannot transition to state fulfilled" do
        expect { state_machine.transition_to!(:fulfilled) }.to raise_error(Statesman::TransitionFailedError)
      end

      it "cannot transition to state returned" do
        expect { state_machine.transition_to!(:returned) }.to raise_error(Statesman::TransitionFailedError)
      end
    end

    context 'from fulfilled state' do
      let(:order) { create(:order, :fulfilled) }

      it "can transition to state returned" do
        expect { state_machine.transition_to!(:returned) }.not_to raise_error
      end

      it "can transition to state returned and triggers a RefundOrderJob" do
        expect { state_machine.transition_to!(:returned) }.to change(RefundOrderJob.jobs, :size).by(1)
      end

      it "cannot transition to state created" do
        expect { state_machine.transition_to!(:created) }.to raise_error(Statesman::TransitionFailedError)
      end

      it "cannot transition to state paid" do
        expect { state_machine.transition_to!(:paid) }.to raise_error(Statesman::TransitionFailedError)
      end

      it "cannot transition to state queued" do
        expect { state_machine.transition_to!(:queued) }.to raise_error(Statesman::TransitionFailedError)
      end

      it "cannot transition to state canceled" do
        expect { state_machine.transition_to!(:canceled) }.to raise_error(Statesman::TransitionFailedError)
      end
    end
  end

  describe 'Guards' do
    context 'from created state' do
      context 'with confirmed customer' do
        let(:customer) { create(:customer, :confirmed, :card) }
        let(:order) { create(:order, customer: customer) }

        it "can transition to state paid" do
          expect { state_machine.transition_to!(:paid) }.not_to raise_error
        end
      end

      context 'with unconfirmed customer' do
        let(:order) { create(:order) }

        it "cannot transition to state paid" do
          expect { state_machine.transition_to!(:paid) }.to raise_error(Statesman::GuardFailedError)
        end

        it "cannot transition to state scheduled" do
          expect { state_machine.transition_to!(:scheduled) }.to raise_error(Statesman::GuardFailedError)
        end
      end

      context 'with invalid shipping email' do
        let(:customer) { create(:customer, :confirmed, :shipping_address, :card) }
        let(:order) { create(:order, customer: customer, shipping: { email: nil }) }

        it "cannot transition to state paid" do
          expect { state_machine.transition_to!(:paid) }.to raise_error(Statesman::GuardFailedError)
        end
      end

      context 'with incomplete customer card' do
        let(:customer) { create(:customer, :confirmed, :shipping_address, :card) }
        let(:order) { create(:order, customer: customer) }

        it "cannot transition to state paid" do
          allow(customer.card).to receive(:valid?).and_return(false)
          expect { state_machine.transition_to!(:paid) }.to raise_error(Statesman::GuardFailedError)
        end
      end

      context 'with invalid customers shipping_address' do
        let(:customer) { create(:customer, :shipping_address) }
        let(:order) { create(:order, customer: customer) }

        it "cannot transition to state paid" do
          allow(customer.reload.shipping_address).to receive(:valid?).and_return(false)
          expect { state_machine.transition_to!(:paid) }.to raise_error(Statesman::GuardFailedError)
        end
      end
    end
  end
end
