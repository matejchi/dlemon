require 'rails_helper'

RSpec.describe SyncStripeRefundsAction do
  let(:customer) { create(:customer, :confirmed, :shipping_address, :card, :stripe) }
  let(:order) { create(:order, :fulfilled, customer: customer) }
  let(:charge_transaction) { order.transactions.charge }
  let(:stripe_charge) { Stripe::Charge.retrieve(charge_transaction.stripe_id) }

  let(:action_result) { SyncStripeRefundsAction.call(stripe_charge) }

  def create_refund(stripe_charge)
    Stripe::Refund.create(charge: stripe_charge.id).tap do |refund|
      stripe_charge.refresh
    end
  end

  context "charge with new refunds" do
    before { create_refund(stripe_charge) }

    it "creates RefundTransaction records and refund taxes" do
      expect(action_result.size).to eql(1)
      transaction = action_result.first
      expect(transaction).to be_a(RefundTransaction)
      expect(transaction.amount).to eql(-order.total)
      expect(RefundTaxJob.jobs.size).to eql(1)
    end
  end

  context "charge with handled refunds" do
    before do
      refund = create_refund(stripe_charge)
      order.transactions.create!(type: RefundTransaction,
                                 stripe_id: refund.id,
                                 amount: refund.amount.fdiv(100))
    end

    it "no-ops" do
      expect(action_result).to eql([])
      expect(RefundTaxJob.jobs.size).to eql(0)
    end
  end

  context "charge without refunds" do
    it "no-ops" do
      expect(action_result).to eql([])
      expect(RefundTaxJob.jobs.size).to eql(0)
    end
  end
end
