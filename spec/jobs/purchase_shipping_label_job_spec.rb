require 'rails_helper'

RSpec.describe PurchaseShippingLabelJob do
  let(:job) { PurchaseShippingLabelJob.new }

  let(:shipment) { create(:shipment) }
  let(:fulfillment_address) { create(:fulfillment_address) }
  let(:ep_shipment) { double('ep_shipment') }

  before do
    allow(ep_shipment).to receive_message_chain(:options, :label_date).and_return(Time.current)
    allow(ep_shipment).to receive_message_chain(:postage_label, :label_url).and_return('label_url')
    allow(Shipment).to receive(:from_easypost).and_return({})
    allow_any_instance_of(DirtyLemon::ShipmentRouter).to receive(:route).and_return([ep_shipment, 2, fulfillment_address])
    allow_any_instance_of(EasypostService).to receive(:purchase_shipping_label).and_return(ep_shipment)
  end

  it "routes the shipment" do
    expect_any_instance_of(DirtyLemon::ShipmentRouter).to receive(:route)
    job.perform(shipment.id)
  end

  it "purchases the shipping label" do
    expect_any_instance_of(EasypostService).to receive(:purchase_shipping_label)
    job.perform(shipment.id)
  end

  it "persists route informations" do
    job.perform(shipment.id)
    shipment.reload
    expect(shipment.purchased_at).to be_present
    expect(shipment.fulfillable_at).to be_present
    expect(shipment.fulfillment_address).to eq(fulfillment_address)
  end

  it "enqueues a job", :skip do
    # TODO
  end
end
