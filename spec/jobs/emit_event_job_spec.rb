require 'rails_helper'

RSpec.describe EmitEventJob do
  let(:job) { EmitEventJob.new }

  let(:event) { create(:event) }

  before do
    allow_any_instance_of(BotpressService).to receive(:create_event).and_return(true)
  end

  it 'calls BotpressDispatcher dispatcher' do
    expect_any_instance_of(DirtyLemon::BotpressDispatcher).to receive(:emit).with(event).and_return(true)
    job.perform(event.id, 'DirtyLemon::BotpressDispatcher')
  end

  it 'calls ActionCableDispatcher dispatcher' do
    expect_any_instance_of(DirtyLemon::ActionCableDispatcher).to receive(:emit).with(event).and_return(true)
    job.perform(event.id, 'DirtyLemon::ActionCableDispatcher')
  end
end
