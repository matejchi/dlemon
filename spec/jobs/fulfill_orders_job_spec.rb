require 'rails_helper'

RSpec.describe FulfillOrdersJob do
  let(:job) { FulfillOrdersJob.new }
  let(:fulfillment_address) { create(:fulfillment_address) }
  let(:batch) { Batch.create(enqueued_at: Time.current, fulfillment_address: fulfillment_address) }
  let!(:shipments) do
    2.times.map do
      create(:shipment, :purchased,
      fulfillment_address: fulfillment_address,
      order: create(:order, :queued),
      created_at: 2.days.ago)
    end
  end

  it 'associates shipments to batch and transition orders to fulfilled' do
    job.perform(batch.id)

    Shipment.all.each { |shipment| expect(shipment.batch_id).to eql(batch.id) }
    Order.all.each { |order| expect(order.status).to eql('fulfilled') }
    expect(Batch.count).to eql(1)
  end

  it 'sends an email to the fulfillment address' do
    # TODO
  end
end
