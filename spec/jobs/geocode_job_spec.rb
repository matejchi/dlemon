require 'rails_helper'

RSpec.describe GeocodeJob do
  let(:job) { GeocodeJob.new }

  let(:address) { create(:address) }

  it 'calls correct service' do
    expect_any_instance_of(GoogleMapService).to receive(:get_coordinates).with(address)
    expect_any_instance_of(GoogleMapService).to receive(:get_time_zone).with(address)
    job.perform(address.id)
  end
end
