require 'rails_helper'

RSpec.describe RenewRecurOrderJob do
  let(:job) { RenewRecurOrderJob.new }
  let(:now) { Time.current.round }

  def create_customer(*traits, **attrs)
    create(:customer, :confirmed, :card, :shipping_address, *traits, **attrs)
  end

  context "non-vip customer" do
    let(:customer) { create_customer(renew_recur_order_on: now - 1.day) }

    it "clears customer renew_recur_order_on and exits" do
      expect(-> { job.perform(customer.id) }).not_to change(Order, :count)
      expect(customer.reload.renew_recur_order_on).to be nil
    end
  end

  context "customer without any processed order" do
    let(:customer) { create_customer(:vip, renew_recur_order_on: now - 1.day) }
    it "clears vip info from the customer" do
      expect(-> { job.perform(customer.id) }).not_to change(Order, :count)

      customer.reload
      expect(customer.vip?).to be false
      expect(customer.renew_recur_order_on).to be nil
    end
  end

  context "renew_recur_order_on in the future" do
    let(:customer) { create(:customer, :confirmed, :vip, renew_recur_order_on: now + 1.day) }

    it "no-ops" do
      expect(-> { job.perform(customer.id) }).not_to change(Order, :count)
      expect(customer.reload.renew_recur_order_on).to be
    end
  end

  context "order to renew" do
    let(:renew_recur_order_on) { now - 1.day }
    let(:customer) { create_customer(:vip) }
    let!(:order) { create(:order, :fulfilled, customer: customer, created_at: 30.days.ago) }

    before { customer.update_attributes!(renew_recur_order_on: renew_recur_order_on) }

    it "creates a scheduled order and sends a trigger to the bot" do
      expect(BotpressTriggerJob).to receive(:perform_async).with('order.renew-notice', customer.id, anything)
      expect(-> { job.perform(customer.id) }).to change(Order, :count)

      expect(customer.orders.recur.count).to eql(1)
      expect(customer.orders.where(status: "scheduled").count).to eql(1)

      scheduled_orders = customer.orders.where(status: "scheduled").first
      expect(scheduled_orders.scheduled_confirm_on).to be > Time.current
    end

    context "and more recent processed order" do
      before { create(:order, :fulfilled, customer: customer) }

      it "skips the renewal" do
        expect(-> { job.perform(customer.id) }).not_to change(Order, :count)
      end
    end
  end
end
