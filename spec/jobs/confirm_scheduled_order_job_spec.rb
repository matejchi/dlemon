require 'rails_helper'

RSpec.describe ConfirmScheduledOrderJob, type: :job do
  let(:job) { ConfirmScheduledOrderJob.new }

  context 'scheduled recurring order with VIP customer' do
    let(:order) { create(:order, :recur, :scheduled, :customer_vip) }

    it "pays and queues the order" do
      job.perform(order.id)
      expect(Order.find(order.id).status).to eq('queued')
    end

    context 'with failed charge' do
      before { StripeMock.prepare_card_error(:card_declined) }

      it "completes, transitions to created and triggers the payment failed flow" do
        expect(BotpressTriggerJob).to receive(:perform_async).with('order.charge-failed', anything, anything).once
        expect(-> { job.perform(order.id) }).not_to raise_error
        expect(order.reload.status).to eql('created')
      end

      it "marks the customer with vip failed card tag" do
        job.perform(order.id)
        expect(order.customer.reload.tags).to include(Customer::VIP_FAILED_CARD_TAG)
      end
    end

    context 'with a more recent paid order on file' do
      before do
        order.update_attributes!(created_at: Time.current - 4.days)
        create(:order, :queued, customer: order.customer)
      end

      it "marks the order as created and no-ops" do
        job.perform(order.id)
        expect(order.reload.status).to eql('created')
      end
    end
  end

  context 'scheduled recurring order with non-VIP customer' do
    let(:order) { create(:order, :recur, :scheduled) }

    it "clears VIP tag and transitions the order the created" do
      job.perform(order.id)
      expect(Order.find(order.id).status).to eq('created')
      expect(order.customer.reload.vip?).to be false
    end
  end

  context 'scheduled order with non-VIP customer' do
    let(:order) { create(:order, :scheduled) }

    it "pays and queues the order" do
      job.perform(order.id)
      expect(Order.find(order.id).status).to eq('queued')
    end
  end

  context 'created order' do
    let(:order) { create(:order) }

    it "does not queue the order" do
      job.perform(order.id)
      expect(Order.find(order.id).status).to eq('created')
    end
  end
end
