require 'rails_helper'

RSpec.describe RefundOrderJob do
  let(:job) { RefundOrderJob.new }

  context "with ChargeTransaction" do
    let(:customer) { create(:customer, :stripe) }

    let(:charge) do
      Stripe::Charge.create(amount: 100_00, currency: 'usd', customer: customer.stripe_id)
    end

    let(:order) do
      create(:order, customer: customer).tap do |order|
        order.transactions.create!(
          customer: customer,
          type: ChargeTransaction,
          amount: charge.amount.fdiv(100),
          stripe_id: charge.id,
        )
      end
    end

    it "triggers a refund" do
      expect(Stripe::Refund).to receive(:create).with(charge: charge.id)
      job.perform(order.id)
    end
  end

  context "without ChargeTransaction" do
    let(:order) { create(:order) }
    it "does nothing" do
      expect(Stripe::Refund).not_to receive(:create)
      job.perform(order.id)
    end
  end
end
