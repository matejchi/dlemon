require 'rails_helper'

RSpec.describe RefundShippingLabelJob do
  let(:job) { RefundShippingLabelJob.new }

  let(:shipment) { create(:shipment) }

  it 'refunds the shipping label' do
    expect_any_instance_of(EasypostService).to receive(:refund_shipping_label).with(shipment)
    job.perform(shipment.id)
  end
end
