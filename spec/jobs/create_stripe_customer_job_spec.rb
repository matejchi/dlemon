require 'rails_helper'

RSpec.describe CreateStripeCustomerJob, type: :job do
  let(:customer) { create(:customer, :shipping_address, :billing_address, :card) }

  let(:job) { CreateStripeCustomerJob.new }

  it "should find a customer and create a record in stripe" do
    job.perform(customer.id)
    expect(customer.reload.stripe_id).to eq('test_cus_1')
  end
end
