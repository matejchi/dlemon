require 'rails_helper'

RSpec.describe UpdateStripeCustomerJob, type: :job do

  let!(:customer) { create(:customer, :shipping_address, :billing_address, :card) }

  let(:job) { UpdateStripeCustomerJob.new }

  before do
    customer.update_columns(stripe_id: 'cus_Blp1yYbdaSakuN')
    StripeMock.stop # FIXME: convert this to use StripeMock?
  end

  it "should find a customer and update a record in stripe" do
    allow(Stripe::Customer).to receive(:retrieve).and_call_original

    VCR.use_cassette('stripe', record: :new_episodes, match_requests_on: [:method, :host]) do
      job.perform(customer.id)
      expect(Stripe::Customer).to have_received(:retrieve).with(customer.stripe_id)
    end
  end
end
