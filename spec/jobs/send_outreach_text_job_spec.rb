require 'rails_helper'

RSpec.describe SendOutreachTextJob do
  let(:campaign_name) { "TEST_CAMPAIGN" }
  let(:customer) { create(:customer, :confirmed) }
  let(:outreach) { create(:outreach, campaign_name: campaign_name, customer: customer) }
  let(:twilio_message_class) { Twilio::REST::Api::V2010::AccountContext::MessageList }

  context "invalid campaign name" do
    it "does nothing" do
      expect_any_instance_of(Twilio::REST::Client).not_to receive(:messages)
      SendOutreachTextJob.new.perform(outreach.id)
    end
  end

  context "valid campaign name" do
    before { allow(Outreach).to receive(:campaign_details).and_return({ content: "Test text" }) }
    before do
      allow_any_instance_of(twilio_message_class)
        .to receive(:create)
    end

    it "calls twilio" do
      expect_any_instance_of(twilio_message_class)
        .to receive(:create)
      SendOutreachTextJob.new.perform(outreach.id)
    end

    it "persists message and marks as sent" do
      SendOutreachTextJob.new.perform(outreach.id)
      messages = outreach.customer.messages.to_a
      expect(messages.size).to eql(1)
      expect(messages.first.content).to eql("Test text")
      expect(messages.first.sender_role).to eql("outreach")

      outreach.reload
      expect(outreach.sent_at).to be
    end

    context "failing to send sms" do
      before do
        allow_any_instance_of(twilio_message_class)
          .to receive(:create)
          .and_raise(Twilio::REST::TwilioError.new("Faked error"))
      end

      it "rollbacks all changes" do
        expect(-> { SendOutreachTextJob.new.perform(outreach.id) })
          .to raise_error(Twilio::REST::TwilioError)

        outreach.reload

        expect(outreach.customer.messages.count).to eql(0)
        expect(outreach.sent_at).to be(nil)
      end
    end

    context "recipient that unsubscribed" do
      before do
        blacklisted_message = "Unable to create record: The message From/To pair violates a blacklist rule."
        allow_any_instance_of(twilio_message_class)
          .to receive(:create)
          .and_raise(Twilio::REST::RestError.new(blacklisted_message, 21610, 400))
      end

      it "catches the error, marks customer as unsubscribed, delete the Outreach" do
        SendOutreachTextJob.new.perform(outreach.id)
        outreach.customer.reload

        expect(Outreach.where(id: outreach.id).first).to be nil
        expect(outreach.customer.unsubscribed_at).to be
      end
    end
  end
end
