require 'vcr'

VCR.configure do |c|
  c.default_cassette_options = { record: :new_episodes, allow_playback_repeats: true }
  c.cassette_library_dir = 'spec/cassettes'
  c.allow_http_connections_when_no_cassette = false #true
  c.hook_into :webmock
  c.configure_rspec_metadata!
  c.debug_logger = File.open(Rails.root.join('log/vcr.log'), 'w')
end
