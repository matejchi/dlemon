module Helpers

  # Parses a JSON string into a hash
  #
  # @param response [ActionDispatch::Response]
  # @return [Hash]
  def json(response)
    JSON.parse(response.body, symbolize_names: true)
  end

  # Creates a session with 1 message at a given time
  #
  # @param customer [Customer]
  # @param sent_at [Time]
  # @return [Session]
  def setup_session(customer, sent_at = nil)
    sent_at ||= Time.current
    message = create(:message, session: customer.sessions.create!(created_at: sent_at), sent_at: sent_at)

    message.session
  end
end
