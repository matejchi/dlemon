require 'rails_helper'

RSpec.describe SerializableOrder, type: :serializer do
  describe 'Data Validation' do

    context 'General orden' do
      let(:customer) { create(:customer) }
      let(:order) { create(:order, customer: customer) }
      let(:order_serializable) { order.serialize }

      it 'has an id that matches' do
        expect(order_serializable[:id]).to eql(order.id)
      end

      it 'has customer that matches' do
        expect(order_serializable[:customer_id]).to eql(customer.id)
      end

      it 'has attributes that matches' do
        expect(order_serializable[:currency]).to eql(order.currency)
        expect(order_serializable[:reference]).to eql(order.reference)
        expect(order_serializable[:status]).to eql(order.status)
        expect(order_serializable[:stripe_id]).to eql(order.stripe_id)
        expect(order_serializable[:recur]).to eql(order.recur)
      end

      it 'valid format to shipping_fee' do
        expect(order_serializable[:shipping_fee]).to eql((order.shipping_fee * 100).to_i)
      end

      it 'valid format to total' do
        expect(order_serializable[:total]).to eql((order.total * 100).to_i)
      end

      it 'valid format to tax' do
        expect(order_serializable[:tax]).to eql((order.tax * 100).to_i)
      end

      it 'valid shipping relations' do
        expect(order_serializable[:shipping][:name]).to eql(order.shipping.name)
        expect(order_serializable[:shipping][:company]).to eql(order.shipping.company)
        expect(order_serializable[:shipping][:phone_number]).to eql(order.shipping.phone_number)
        expect(order_serializable[:shipping][:email]).to eql(order.shipping.email)
        expect(order_serializable[:shipping][:street1]).to eql(order.shipping.street1)
        expect(order_serializable[:shipping][:street2]).to eql(order.shipping.street2)
        expect(order_serializable[:shipping][:city]).to eql(order.shipping.city)
        expect(order_serializable[:shipping][:zip]).to eql(order.shipping.zip)
      end
    end
  end
end