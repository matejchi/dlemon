require 'rails_helper'

RSpec.describe SerializableSku, type: :serializer do
  describe 'Data Validation' do
    context 'General sku' do
      let(:sku) { create(:sku) }
      let(:sku_serializable) { sku.serialize }

      it 'has an id that matches' do
        expect(sku_serializable[:id]).to eql(sku.stripe_id)
      end

      it 'has attributes that matches' do
        expect(sku_serializable[:currency]).to eql(sku.currency)
      end

      it 'includes product' do
        expect(sku_serializable[:product]).to be
        expect(sku_serializable[:product][:name]).to eql(sku.product.name)
        expect(sku_serializable[:product][:id]).to eql(sku.product.id)
      end

      it 'Validate price' do
        expect(sku_serializable[:price]).to eql((sku.price * 100).to_i)
      end
    end
  end
end
