require 'rails_helper'

RSpec.describe SerializableAddress, type: :serializer do
  describe 'Data Validation' do
    context 'General Address' do
      let(:address) { create(:address) }
      let(:address_serializable) { address.serialize }

      it 'has an id that matches' do
        expect(address_serializable[:id]).to eql(address.id)
      end

      it 'has attributes that matches' do
        expect(address_serializable[:street1]).to eql(address.street1)
        expect(address_serializable[:street2]).to eql(address.street2)
        expect(address_serializable[:city]).to eql(address.city)
        expect(address_serializable[:state]).to eql(address.state)
        expect(address_serializable[:country]).to eql(address.country)
        expect(address_serializable[:zip]).to eql(address.zip)
        expect(address_serializable[:created_at]).to eql(address.created_at)
        expect(address_serializable[:updated_at]).to eql(address.updated_at)
      end
    end
  end
end
