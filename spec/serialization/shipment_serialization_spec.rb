require 'rails_helper'

RSpec.describe SerializableShipment, type: :serializer do
  describe 'Data Validation' do
    context 'General shipment' do
      let(:shipment) { create(:shipment) }
      let(:shipment_serializable) { shipment.serialize }

      it 'has an id that matches' do
        expect(shipment_serializable[:id]).to eql(shipment.id)
      end

      it 'has attributes that matches' do
        expect(shipment_serializable[:tracking_number]).to eql(shipment.tracking_number)
        expect(shipment_serializable[:carrier]).to eql(shipment.carrier)
        expect(shipment_serializable[:easypost_shipment_id]).to eql(shipment.easypost_shipment_id)
        expect(shipment_serializable[:easypost_tracker_id]).to eql(shipment.easypost_tracker_id)
        expect(shipment_serializable[:eta]).to eql(shipment.eta)
        expect(shipment_serializable[:rate]).to eql(shipment.rate)
        expect(shipment_serializable[:service]).to eql(shipment.service)
        expect(shipment_serializable[:tracking_url]).to eql(shipment.tracking_url)
        expect(shipment_serializable[:tracking_status]).to eql(shipment.tracking_status)
        expect(shipment_serializable[:tracking_status_at]).to eql(shipment.tracking_status_at)
        expect(shipment_serializable[:fulfillable_at]).to eql(shipment.fulfillable_at)
        expect(shipment_serializable[:purchased_at]).to eql(shipment.purchased_at)
        expect(shipment_serializable[:refunded_at]).to eql(shipment.refunded_at)
        expect(shipment_serializable[:label_zpl_s3_key]).to eql(shipment.label_zpl_s3_key)
        expect(shipment_serializable[:label_png_s3_key]).to eql(shipment.label_png_s3_key)
      end
    end
  end
end
