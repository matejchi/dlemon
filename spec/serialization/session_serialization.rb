require 'rails_helper'

RSpec.describe SerializableSession, type: :serializer do
  describe 'Data Validation' do
    context 'General session' do
      let(:session) { create(:session) }
      let(:session_serializable) { session.serialize }

      it 'has an id that matches' do
        expect(session_serializable[:id]).to eql(session.id)
      end

      it 'has attributes that matches' do
        expect(session_serializable[:customer_id]).to eql(session.customer_id)
        expect(session_serializable[:tags]).to eql(session.tags)
        expect(session_serializable[:topics]).to eql(session.topics)
        expect(session_serializable[:created_at]).to eql(session.created_at)
        expect(session_serializable[:updated_at]).to eql(session.updated_at)
        expect(session_serializable[:sender_roles]).to eql(session.sender_roles)
      end
    end
  end
end