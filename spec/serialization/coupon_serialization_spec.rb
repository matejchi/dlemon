require 'rails_helper'

RSpec.describe SerializableCoupon, type: :serializer do
  describe 'Data Validation' do
    context 'General Coupon' do
      let(:coupon) { create(:coupon) }
      let(:coupon_serializable) { coupon.serialize }

      it 'has an id that matches' do
        expect(coupon_serializable[:id]).to eql(coupon.code)
      end
    end
  end
end
