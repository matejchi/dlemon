require 'rails_helper'

RSpec.describe SerializableCard, type: :serializer do
  describe 'Data Validation' do
    context 'General CArd' do
      let(:card) { create(:card) }
      let(:card_serializable) { card.serialize }

      it 'has attributes that matches' do
        expect(card_serializable[:customer_id]).to eql(card.customer.id)
        expect(card_serializable[:exp_month]).to eql(card.exp_month)
        expect(card_serializable[:exp_year]).to eql(card.exp_year)
        expect(card_serializable[:last4]).to eql(card.last4)
      end
    end
  end
end
