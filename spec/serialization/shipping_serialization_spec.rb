require 'rails_helper'

RSpec.describe SerializableShipping, type: :serializer do
  describe 'Data Validation' do
    context 'General shipping' do
      let(:shipping) { build(:shipping) }
      let(:shipping_serializable) { shipping.serialize }

      it 'has attributes that matches' do
        expect(shipping_serializable[:name]).to eql(shipping.name)
        expect(shipping_serializable[:company]).to eql(shipping.company)
        expect(shipping_serializable[:phone_number]).to eql(shipping.phone_number)
        expect(shipping_serializable[:email]).to eql(shipping.email)
        expect(shipping_serializable[:street1]).to eql(shipping.street1)
        expect(shipping_serializable[:street2]).to eql(shipping.street2)
        expect(shipping_serializable[:city]).to eql(shipping.city)
        expect(shipping_serializable[:state]).to eql(shipping.state)
        expect(shipping_serializable[:country]).to eql(shipping.country)
        expect(shipping_serializable[:zip]).to eql(shipping.zip)
      end
    end
  end
end