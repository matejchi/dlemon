require 'rails_helper'

RSpec.describe SerializableBillingAddress, type: :serializer do
  describe 'Data Validation' do
    context 'General BillingAddress' do
      let(:billing_address) { create(:billing_address) }
      let(:billing_address_serializable) { billing_address.serialize }

      it 'has an id that matches' do
        expect(billing_address_serializable[:id]).to eql(billing_address.id)
      end

      it 'has attributes that matches' do
        expect(billing_address_serializable[:street1]).to eql(billing_address.street1)
        expect(billing_address_serializable[:street2]).to eql(billing_address.street2)
        expect(billing_address_serializable[:city]).to eql(billing_address.city)
        expect(billing_address_serializable[:state]).to eql(billing_address.state)
        expect(billing_address_serializable[:country]).to eql(billing_address.country)
        expect(billing_address_serializable[:zip]).to eql(billing_address.zip)
        expect(billing_address_serializable[:type]).to eql(billing_address.type)
        expect(billing_address_serializable[:created_at]).to eql(billing_address.created_at)
        expect(billing_address_serializable[:updated_at]).to eql(billing_address.updated_at)
      end
    end
  end
end
