require 'rails_helper'

RSpec.describe SerializableEvent, type: :serializer do
  describe 'Data Validation' do
    context 'General event' do
      let(:event) { create(:event) }
      let(:event_serializable) { event.serialize }

      it 'has an id that matches' do
        expect(event_serializable[:id]).to eql(event.id)
      end

      it 'has attributes that matches' do
        expect(event_serializable[:name]).to eql(event.name)
        expect(event_serializable[:data]).to eql(event.data)
        expect(event_serializable[:created_at]).to eql(event.created_at)
      end
    end
  end
end