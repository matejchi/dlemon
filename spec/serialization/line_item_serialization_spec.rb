require 'rails_helper'

RSpec.describe SerializableLineItem, type: :serializer do
  describe 'Data Validation' do
    context 'General line item' do
      let(:line_item) { create(:line_item) }
      let(:line_item_serializable) { line_item.serialize }

      it 'has an id that matches' do
        expect(line_item_serializable[:id]).to eql(line_item.id)
      end

      it 'has attributes that matches' do
        expect(line_item_serializable[:description]).to eql(line_item.description)
        expect(line_item_serializable[:quantity]).to eql(line_item.quantity)
        expect(line_item_serializable[:created_at]).to eql(line_item.created_at)
        expect(line_item_serializable[:updated_at]).to eql(line_item.updated_at)
      end

      it 'validate amount' do
        expect(line_item_serializable[:amount]).to eql((line_item.amount * 100).to_i)
      end
    end
  end
end