require 'rails_helper'

RSpec.describe SerializableCustomer, type: :serializer do
  describe 'Data Validation' do
    context 'General customer' do
      let(:customer) { create(:customer) }
      let(:customer_serializable) { customer.serialize }

      it 'has an id that matches' do
        expect(customer_serializable[:id]).to eql(customer.id)
      end

      it 'has attributes that matches' do
        expect(customer_serializable[:first_name]).to eql(customer.first_name)
        expect(customer_serializable[:last_name]).to eql(customer.last_name)
        expect(customer_serializable[:reference]).to eql(customer.reference)
        expect(customer_serializable[:country_code]).to eql(customer.country_code)
        expect(customer_serializable[:email]).to eql(customer.email)
        expect(customer_serializable[:time_zone]).to eql(customer.time_zone)
        expect(customer_serializable[:tags]).to eql(customer.tags)
        expect(customer_serializable[:created_at]).to eql(customer.created_at)
        expect(customer_serializable[:updated_at]).to eql(customer.updated_at)
        expect(customer_serializable[:billing_address]).to be_nil
        expect(customer_serializable[:shipping_address]).to be_nil
        expect(customer_serializable[:card]).to be_nil
        expect(customer_serializable[:renew_recur_order_on]).to be_nil
        expect(customer_serializable.has_key?(:renew_recur_order_on)).to be true
      end
    end

    context 'Confirmed customer' do
      let(:customer) { create(:customer, :confirmed) }
      let(:customer_serializable) { customer.serialize }

      it 'has an id that matches' do
        expect(customer_serializable[:id]).to eql(customer.id)
      end

      it 'has attributes that matches' do
        expect(customer_serializable[:confirmed_at]).to eql(customer.confirmed_at)
        expect(customer_serializable[:phone_number]).to eql(customer.phone_number)
        expect(customer_serializable[:unconfirmed_phone_number]).to eql(nil)
      end
    end

    context 'Customer with card, shipping_address and billing_address' do
      let(:customer) { create(:customer, :billing_address, :with_addresses, :shipping_address, :confirmed, :card) }
      let(:billing_address) { create(:billing_address, customer: customer) }
      let(:customer_serializable) { customer.serialize }

      it 'has card that matches' do
        expect(customer_serializable[:card][:exp_month]).to eql(customer.card.exp_month)
        expect(customer_serializable[:card][:exp_year]).to eql(customer.card.exp_year)
        expect(customer_serializable[:card][:last4]).to eql(customer.card.last4)
        expect(customer_serializable[:card][:url]).to eql(customer.card.short_url)
      end

      it 'has shipping_address that matches' do
        expect(customer_serializable[:shipping_address][:id]).to eql(customer.shipping_address.id)
        expect(customer_serializable[:shipping_address][:street1]).to eql(customer.shipping_address.street1)
        expect(customer_serializable[:shipping_address][:street2]).to eql(customer.shipping_address.street2)
        expect(customer_serializable[:shipping_address][:city]).to eql(customer.shipping_address.city)
        expect(customer_serializable[:shipping_address][:state]).to eql(customer.shipping_address.state)
        expect(customer_serializable[:shipping_address][:country]).to eql(customer.shipping_address.country)
        expect(customer_serializable[:shipping_address][:zip]).to eql(customer.shipping_address.zip)
        expect(customer_serializable[:shipping_address][:url]).to eql(customer.shipping_address.short_url)
      end
    end
  end
end
