require 'rails_helper'

RSpec.describe SerializableMessage, type: :serializer do
  describe 'Data Validation' do
    context 'General message' do
      let(:message) { create(:message) }
      let(:message_serializable) { message.serialize }

      it 'has an id that matches' do
        expect(message_serializable[:id]).to eql(message.id)
      end

      it 'has attributes that matches' do
        expect(message_serializable[:content]).to eql(message.content)
        expect(message_serializable[:content_type]).to eql(message.content_type)
        expect(message_serializable[:sender_role]).to eql(message.sender_role)
        expect(message_serializable[:sent_at]).to eql(message.sent_at)
        expect(message_serializable[:processed_at]).to eql(message.processed_at)
        expect(message_serializable[:created_at]).to eql(message.created_at)
      end
    end
  end
end