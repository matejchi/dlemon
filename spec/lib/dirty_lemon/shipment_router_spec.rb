require 'rails_helper'

RSpec.describe DirtyLemon::ShipmentRouter do
  let(:fulfillment_addresses) { create_list(:fulfillment_address, 1) }
  let(:router) do
    DirtyLemon::ShipmentRouter.new(
      EasypostService.new,
      DirtyLemon::Settings.carrier_accounts,
      fulfillment_addresses
    )
  end

  it 'initializes params' do
    expect(router.service).to be_a(EasypostService)
    expect(router.carriers).to eq(DirtyLemon::Settings.carrier_accounts)
    expect(router.fulfillment_addresses).to eq(fulfillment_addresses)
  end

  describe '#route' do
    let(:shipment) { create(:shipment) }
    let(:rate) { double(:rate, rate: 1, carrier: 'UPS', shipment_id: 123, delivery_date_guaranteed: Time.now+1.day) }
    let(:ep_shipment) do
      double(
        :ep_shipment,
        id: 123,
        from_address: double(:from_address, street1: fulfillment_addresses.first.street1),
        get_rates: double(:get_rates, rates: [rate])
      )
    end

    before do
      allow_any_instance_of(EasypostService).to receive(:create_shipment).and_return(ep_shipment)
    end

    it 'returns shipment, selected rate and selected fulfillment address' do
      response = router.route(shipment, Time.now)
      expect(response).to eq ([ep_shipment, rate, fulfillment_addresses.first])
    end
  end

  describe '#fulfillable_at' do
    before { @result = router.fulfillable_at(time, 6) }

    context 'purchased on Monday' do
      context 'before fullfills_at time' do
        let(:time) { DateTime.parse('Monday 03:00 AM') }

        it 'returns same day date' do
          expect(@result).to eq time.at_beginning_of_day + 6.hours
        end
      end

      context 'after fullfills_at time' do
        let(:time) { DateTime.parse('Monday 03:00 PM') }

        it 'returns next day date' do
          expect(@result).to eq time.at_beginning_of_day.next_day + 6.hours
        end
      end
    end

    context 'purchased on Tuesday' do
      context 'before fullfills_at time' do
        let(:time) { DateTime.parse('Tuesday 03:00 AM') }

        it 'returns same day date' do
          expect(@result).to eq time.at_beginning_of_day + 6.hours
        end
      end

      context 'after fullfills_at time' do
        let(:time) { DateTime.parse('Tuesday 03:00 PM') }

        it 'returns next day date' do
          expect(@result).to eq time.at_beginning_of_day.next_day + 6.hours
        end
      end
    end

    context 'purchased on Wednesday' do
      context 'before fullfills_at time' do
        let(:time) { DateTime.parse('Wednesday 03:00 AM') }

        it 'returns same day date' do
          expect(@result).to eq time.at_beginning_of_day + 6.hours
        end
      end

      context 'after fullfills_at time' do
        let(:time) { DateTime.parse('Wednesday 03:00 PM') }

        it 'returns next day date' do
          expect(@result).to eq time.at_beginning_of_day.next_day + 6.hours
        end
      end
    end

    context 'purchased on Thursday' do
      context 'purchased on Thursday' do
        let(:time) { DateTime.parse('Thursday 03:00 PM') }

        it 'returns friday' do
          expect(@result).to eq time.at_beginning_of_day.next_day + 6.hours
        end
      end
    end

    context do
      let(:expected_fulfillment_date) { time.next_week.monday + 6.hours }

      context 'purchased on Friday' do
        let(:time) { DateTime.parse('Friday 03:00 PM') }

        it 'returns next weeks date' do
          expect(@result).to eq expected_fulfillment_date
        end
      end

      context 'purchased on Saturday' do
        let(:time) { DateTime.parse('Saturday 03:00 PM') }

        it 'returns next weeks date' do
          expect(@result).to eq expected_fulfillment_date
        end
      end

      context 'purchased on Sunday' do
        let(:time) { DateTime.parse('Sunday 03:00 PM') }

        it 'returns next weeks date' do
          expect(@result).to eq expected_fulfillment_date
        end
      end
    end
  end

end
