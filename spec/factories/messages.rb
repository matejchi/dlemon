FactoryBot.define do
  factory :message do
    content { Faker::Lorem.sentence(1) }
    content_type { ['text'].sample }
    sender_role { ['end-user', 'agent', 'bot'].sample }
    sent_at { Time.current }

    association :session, factory: [:session]
  end

  factory :invalid_message, parent: :message do
    content nil
    content_type nil
    sender_role nil
    sent_at nil
    session nil
  end
end
