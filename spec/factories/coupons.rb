FactoryBot.define do
  factory :coupon do
    code 'CODEVIP1'
    active true
  end

  factory :invalid_coupon, parent: :coupon do
    code nil
  end
end
