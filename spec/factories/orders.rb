FactoryBot.define do
  factory :order do
    customer
    currency { 'usd' }
    tax 0
    total { rand(45..260) }
    shipping_fee 0

    shipping do
      {
        name: Faker::Name.first_name,
        phone_number: "1-888-407-#{rand(1000..9999)}",
        email: Faker::Internet.email,
        street1: Faker::Address.street_address,
        street2: Faker::Address.secondary_address,
        city: Faker::Address.city,
        state: Faker::Address.state,
        country: Faker::Address.country,
        zip: Faker::Address.zip,
      }
    end

    trait :recur do
      recur true
    end

    trait :customer_vip do |order|
      association :customer, factory: [:customer, :confirmed, :shipping_address, :card, :vip]
    end

    trait :scheduled do |order|
      scheduled_confirm_on Time.current + 2.days
      association :customer, factory: [:customer, :confirmed, :shipping_address, :card]

      after :create do |order|
        order.reload.state_machine.transition_to!(:scheduled)
      end
    end

    trait :paid do |order|
      association :customer, factory: [:customer, :confirmed, :shipping_address, :card]

      after :create do |order|
        order.reload.state_machine.transition_to!(:paid)
      end
    end

    trait :queued do |order|
      association :customer, factory: [:customer, :confirmed, :shipping_address, :card]

      after :create do |order|
        order.reload.state_machine.transition_to!(:paid)
        order.reload.state_machine.transition_to!(:queued)
      end
    end

    trait :unqueued do |order|
      association :customer, factory: [:customer, :confirmed, :shipping_address, :card]

      after :create do |order|
        order.reload.state_machine.transition_to!(:paid)
        order.reload.state_machine.transition_to!(:queued)
        order.reload.state_machine.transition_to!(:unqueued)
      end
    end

    trait :fulfilled do |order|
      association :customer, factory: [:customer, :confirmed, :shipping_address, :card]

      after :create do |order|
        order.reload.state_machine.transition_to!(:paid)
        order.reload.state_machine.transition_to!(:queued)
        order.reload.state_machine.transition_to!(:fulfilled)
      end
    end

    trait :canceled do |order|
      association :customer, factory: [:customer, :confirmed, :shipping_address, :card]

      after :create do |order|
        order.reload.state_machine.transition_to!(:paid)
        order.reload.state_machine.transition_to!(:canceled)
      end
    end

    trait :returned do |order|
      association :customer, factory: [:customer, :shipping_address]

      after :create do |order|
        order.reload.state_machine.transition_to!(:paid)
        order.state_machine.transition_to!(:queued)
        order.state_machine.transition_to!(:fulfilled)
        order.state_machine.transition_to!(:returned)
      end
    end

    trait :line_items do
      after(:create) do |order, evaluator|
        create_list(:line_item, 1, sku: create(:sku), order: order)
      end
    end

    trait :shipments do
      after(:create) do |order|
        create_list(:shipment, 1, :purchased, order: order)
      end
    end
  end

  factory :invalid_order, parent: :order do
    customer nil
    currency nil
  end
end
