FactoryBot.define do
  factory :event do
    name 'address.updated'
    association :eventable, factory: [:address]
  end

  factory :invalid_event, parent: :event do
    name nil
    eventable nil
  end
end
