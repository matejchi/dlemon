FactoryBot.define do
  sequence(:stripe_sku_id) { |n| n.to_s.rjust(5, '0')  }

  factory :sku do
    product

    id { SecureRandom.uuid }
    currency { 'usd' }
    price { rand(100) }
    stripe_id { generate(:stripe_sku_id) }
  end

  factory :invalid_sku, parent: :sku do
    id nil
    currency nil
    price nil
    product nil
  end
end
