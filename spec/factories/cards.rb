FactoryBot.define do
  factory :card, class: Card do
    stripe_id 'stripe_id'
    exp_month 12
    exp_year 2020
    last4 1234
    association :customer, factory: [:customer]
  end

  factory :invalid_card, parent: :card do
    stripe_id nil
    exp_month nil
    exp_year nil
    last4 nil
  end
end
