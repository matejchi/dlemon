FactoryBot.define do
  sequence :stripe_charge_id { |n| "ch_#{n.to_s.rjust(10, "0")}" }
end
