FactoryBot.define do
  factory :outreach do
    campaign_name "MATCHA_OUTREACH"

    after(:build) do |outreach|
      outreach.phone_number = outreach.phone_number ||
                              outreach.customer&.phone_number ||
                              outreach.customer&.unconfirmed_phone_number
    end
  end
end
