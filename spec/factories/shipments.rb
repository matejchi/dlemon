FactoryBot.define do
  factory :shipment do
    association :order, factory: [:order]
    association :sku, factory: [:sku]

    trait :purchased do
      association :fulfillment_address, factory: [:fulfillment_address]

      easypost_shipment_id { SecureRandom.uuid }
      easypost_tracker_id { SecureRandom.uuid }
      fulfillable_at { Time.current + 7.hours }
      carrier { %w(carrier1 carrier2 carrier3).sample }
      service { %w(service1 service2 service3).sample }
      rate { rand(10..20) }
      tracking_number { SecureRandom.uuid }
      tracking_url { Faker::Internet.url }
      tracking_status { %w(pre_transit in_transit out_for_delivery delivered return_to_sender failure unknown).sample }
      tracking_status_at { Time.current }
      purchased_at { Time.current }
      eta { Time.current + 1.day }
    end

    trait :refunded do
      refunded_at { Time.current + 2.hours }
    end
  end

  factory :invalid_shipment, parent: :shipment do
    order nil
    sku nil
    easypost_shipment_id nil
    easypost_tracker_id nil
    carrier nil
    tracking_number nil
    tracking_url nil
    tracking_status nil
    tracking_status_at nil
    eta nil
  end
end
