FactoryBot.define do
  factory :order_transition do
    association :order, factory: [:order]
  end

  factory :invalid_order_transition, parent: :order_transition do
    order nil
  end
end
