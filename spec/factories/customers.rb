FactoryBot.define do
  sequence(:phone) { |n| "1-888-470-#{n + 1000}" }

  factory :customer do
    first_name { Faker::Name.first_name }
    last_name { Faker::Name.last_name }
    email { Faker::Internet.email }
    country_code 'US'
    unconfirmed_phone_number { generate(:phone) }
    tags { ['normal'] }
    stripe_id { 'cus_abc' }

    trait :phone_number do
      phone_number { generate(:phone) }
    end

    trait :confirmed do
      phone_number { generate(:phone) }
      confirmed_at { Time.current + 1.minute }
      confirmation_generated_at { Time.current }
    end

    trait :without_phone_number do
      phone_number nil
      confirmed_at nil
      confirmation_generated_at nil
    end

    trait :shipping_address do
      association :shipping_address, factory: [:shipping_address]
    end

    trait :billing_address do
      after :create do |customer|
        create(:billing_address, customer: customer)
      end
    end

    trait :with_addresses do
      after :create do |customer|
        create(:address, customer: customer)
      end
    end

    trait :card do
      after :create do |customer|
        customer.card = build(:card)
      end
    end

    trait :unsubscribed do
      unsubscribed_at { Time.current }
    end

    trait :vip do
      tags { ['vip'] }
    end

    trait :stripe do
      stripe_id do
        card_token =  StripeMock.create_test_helper.generate_card_token
        Stripe::Customer.create(source: card_token).id
      end
    end
  end

  factory :invalid_customer, parent: :customer do
    email nil
    first_name nil
    last_name nil
    country_code nil
    unconfirmed_phone_number nil
    phone_number nil
    confirmed_at { Time.current }
  end
end
