FactoryBot.define do
  sequence :stripe_token do |n|
    Stripe::Token.create(card: {
      number: "4012888888881881",
      exp_month: 5,
      exp_year: 2020,
      cvc: 1234
    }).id
  end
end
