FactoryBot.define do
  factory :product do
    name { %w(+charcoal +collagen sleep +ginseng).sample }
    description { Faker::Lorem.sentence(1) }

    trait :with_sku do
      after(:create) do |product, evaluator|
        create(:sku, product: product)
      end
    end
  end

  factory :invalid_product, parent: :product do
    name nil
    description nil
  end
end
