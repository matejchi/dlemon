FactoryBot.define do
  factory :shipping_address, class: ShippingAddress do
    customer

    street1 { Faker::Address.street_address }
    street2 { Faker::Address.secondary_address }
    city { Faker::Address.city }
    zip { Faker::Address.zip }
    state { Faker::Address.state }
    country { Faker::Address.country }
  end

  factory :invalid_shipping_address, parent: :shipping_address do
    customer nil
    street1 nil
    street2 nil
    city nil
    zip nil
    state nil
    country nil
  end
end
