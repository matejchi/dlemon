FactoryBot.define do
  factory :line_item do
    order
    sku { Sku.offset(rand(Sku.count)).first }

    description { Faker::Lorem.sentence(2) }
    quantity { rand(1..10) }
    amount { rand(50..100).floor }
  end

  factory :invalid_line_item, parent: :line_item do
    order nil
    sku nil
    description nil
    quantity nil
    amount nil
  end
end
