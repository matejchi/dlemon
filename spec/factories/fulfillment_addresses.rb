FactoryBot.define do
  factory :fulfillment_address, class: FulfillmentAddress do
    carrier_facility { ['njfs', 'spwg'].sample }
    street1 { Faker::Address.street_address }
    street2 { Faker::Address.secondary_address }
    city { Faker::Address.city }
    zip { Faker::Address.zip }
    state { Faker::Address.state }
    country { Faker::Address.country }
  end

  factory :invalid_fulfillment_address, parent: :fulfillment_address do
    carrier_facility nil
    street1 nil
    street2 nil
    city nil
    zip nil
    state nil
    country nil
  end
end
