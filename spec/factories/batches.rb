FactoryBot.define do
  factory :batch do
    fulfillment_address
    enqueued_at { Time.current }
  end

  factory :invalid_batch, parent: :batch do
    fulfillment_address nil
  end
end
