FactoryBot.define do
  factory :shipping do
    name { Faker::Name.name }
    company { Faker::Company.name }
    phone_number { "1-888-407-#{rand(1000..9999)}" }
    email { Faker::Internet.email }
    street1 { Faker::Address.street_address }
    street2 { Faker::Address.secondary_address }
    city { Faker::Address.city }
    zip { Faker::Address.zip }
    state { Faker::Address.state }
    country { Faker::Address.country }
  end

  factory :invalid_shipping, parent: :shipping do
    name nil
    street1 nil
    city nil
    zip nil
    state nil
    country nil
  end
end
