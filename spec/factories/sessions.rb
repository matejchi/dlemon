FactoryBot.define do
  factory :session do
    association :customer, factory: [:customer]
  end

  factory :invalid_session, parent: :session do
    customer nil
  end
end
