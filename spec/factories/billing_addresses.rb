FactoryBot.define do
  factory :billing_address, class: BillingAddress do
    customer

    street1 { Faker::Address.street_address }
    street2 { Faker::Address.secondary_address }
    city { Faker::Address.city }
    zip { Faker::Address.zip }
    state { Faker::Address.state }
    country { Faker::Address.country }
  end

  factory :invalid_billing_address, parent: :billing_address do
    customer nil
    street1 nil
    street2 nil
    city nil
    zip nil
    state nil
    country nil
  end
end
