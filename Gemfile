source 'https://rubygems.org'
ruby "2.4.1"

gem 'dotenv-rails' # note, may need to restrict from prod

# RUBY HACKING
gem 'concurrent-ruby'
gem 'virtus'
gem 'tzinfo-data'

# CORE
gem 'rails', '~> 5.1'
gem 'bootsnap', require: false

# DATA
gem 'pg'
gem 'countries'
gem "paranoia", "~> 2.2"

# USER
gem 'devise'
gem 'phony_rails'

# JSON, API SEC
gem 'oj'
gem 'jbuilder'
gem 'jsonapi-rails'
gem 'jwt'

# STATEMACHINE
gem 'statesman'

# UX
gem 'simple_form'
gem 'country_select'

# ASSET PIPELINE
gem 'webpacker', '~> 3.0.2'
gem 'sass-rails'
gem 'uglifier'

# API REQUESTS
gem 'http'

# EXTERNAL SERVICES
gem 'easypost', '~> 2.1'
gem 'mandrill-api', '~> 1.0'
gem 'stripe', '~> 3.3.0'
gem 'twilio-ruby', '~> 5.2'
gem 'bitly', '~> 1'
gem 'aws-sdk-s3', '~> 1' # FIXME: repl with fog-aws
gem 'taxjar-ruby', require: 'taxjar'
gem 'slack-ruby-client'

# PRODUCTION/STAGING
group :production, :staging do
  gem 'puma'
  gem 'rails_12factor'
  gem 'lograge'
end

# JOBSERVER
gem 'redis'
gem 'hiredis'
gem 'sidekiq'

# MIDDLEWARE
gem 'rack-heartbeat'  # simple uptime checker
gem 'rack-timeout'    # Abort requests that are taking too long
gem 'rack-attack'     # Handles blocking & throttling
gem 'rack-cors'       # Handles Cross-Origin Resource Sharing

# REPORTING
gem 'appsignal' # consider newrelic
gem 'sentry-raven'

group :development do
  gem 'bullet'
  gem 'better_errors'
  gem 'binding_of_caller', platforms: [:mri]

  gem 'spring-commands-rspec'
  gem 'guard-rspec', require: false
end

gem 'awesome_print'

# TESTING
group :test, :development do
  gem 'rspec-rails'
end

group :test do
  gem 'shoulda-matchers'
  # gem 'mailcatcher'
  gem 'rails-controller-testing'
  gem 'database_cleaner'
  gem 'factory_bot_rails'
  gem 'faker'
  gem 'fuubar'
  gem 'rspec-sidekiq'
  gem 'terminal-table'
  gem 'timecop'
  gem 'vcr'
  gem 'stripe-ruby-mock', require: false
  gem 'webmock'
end

# DEBUG/PRY
group :test, :development do
  gem 'byebug'
  gem 'pry'
  gem 'pry-byebug'
  gem 'pry-coolline'
  gem 'pry-doc'
  gem 'pry-rails'
  gem 'pry-remote'
  gem 'pry-rescue'
  gem 'pry-stack_explorer'
end

# GUARD / AUTO TEST
group :test, :development do
  gem 'guard'
  gem 'guard-rails'
  gem 'guard-rspec'
end
