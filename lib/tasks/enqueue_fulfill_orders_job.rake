# Creates a Batch and enqueues a FulfillOrdersJob
# This is meant to be called by the Heroku scheduler add-on
#
# Because we name our batch with Date+Hour on S3, there's a limit of one Batch per hour,
# and Batch.create! respects that through a unique index on [fulfillment_address_id, name],
# where name is the name of the batch.
namespace :dl do
  desc "Creates a Batch and enqueues a FulfillOrdersJob"
  task :enqueue_fulfill_orders_job, [:carrier_facility] => [:environment] do |t, args|
    @now = Time.current
    @fulfillment_address = FulfillmentAddress.find_by(carrier_facility: args[:carrier_facility])

    # Fulfillment happens only on Monday, Tuesday, Wednesday and Thursday but the Heroku Scheduler runs every day
    if (@now.monday? || @now.tuesday? || @now.wednesday? || @now.thursday?) && @fulfillment_address
      @batch = Batch.create!(fulfillment_address: @fulfillment_address, enqueued_at: @now)
      FulfillOrdersJob.perform_async(@batch.id)
    end
  end
end
