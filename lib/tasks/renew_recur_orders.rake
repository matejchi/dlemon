namespace :dl do
  desc "Renews recurring orders for customers that are due"
  task renew_recurring_orders: [:environment] do |t, args|
    Customer.recur_order_due.pluck(:id).each do |customer_id|
      RenewRecurOrderJob.perform_async(customer_id)
    end
  end
end

