namespace :dl do
  desc "Migrates ConfirmPendingRecurOrderJob to scheduled"
  task migrate_confirm_recur_tasks: [:environment] do |t, args|
    require 'sidekiq/api'

    ActiveRecord::Base.logger.level = 1

    jobs = Sidekiq::ScheduledSet.new.select { |job| job.klass == "ConfirmPendingRecurOrderJob" }

    jobs.each do |job|
      customer = Customer.find(job.args.first)
      order = customer.orders.created_recur.first

      if order.nil?
        Rails.logger.info("Customer #{customer.id} – deleting job, no pending order")
        job.delete
        next
      end

      if customer.orders.processed.most_recent.where('created_at > ?', order.created_at).exists?
        Rails.logger.info("Customer #{customer.id} – paid another order, deleting job")
        job.delete
        next
      end

      Rails.logger.info("Customer #{customer.id} – scheduling order #{order.reference} to #{job.score}")
      order.scheduled_confirm_on = Time.at(job.score)
      order.state_machine.transition_to!(:scheduled)
      job.delete
    end
  end
end

