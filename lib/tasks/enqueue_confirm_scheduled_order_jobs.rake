namespace :dl do
  desc "Enqueue jobs to confirm due scheduled orders"
  task enqueue_confirm_scheduled_order_jobs: [:environment] do |t, args|
    Order.where('scheduled_confirm_on < ?', Time.current).pluck(:id).each do |order_id|
      ConfirmScheduledOrderJob.perform_async(order_id)
    end
  end
end
